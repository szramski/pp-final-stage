/** load MongoDB Schema that will also be used to
 * make operations on the DB
 */
const ppMongoFlat = require('../models/Flat');

const mongoose = require('mongoose'); //For the typing in getLikedFlats!!

const cloudinary = require('cloudinary');

cloudinary.config({
  cloud_name: 'proxipiso',
  api_key: '227542732957667',
  api_secret: 'gBwmwHL0E2G7_lqef7953U3CHJA',
});

module.exports = {

async getAllFlats (req, res) {

    /* return only those flats that don't contain the id of the current user
    in the flatTrashes array. This means that we return just those flats that
    the user did not mark as "trash"
    */

   let objId = mongoose.Types.ObjectId(req.user.id);

    let flats = await ppMongoFlat.find({
        publishAnnouncement: true,
        flatTrashed: { 
            $not: { 
                $elemMatch: { _id: objId } 
            } //not
            }//flatTrashed
    }).sort( { publishedOnDate: 1 } );//find
    res.send(flats);
}, //getAllFlats

/********************************/

async getLikedFlats (req, res) {
  
    let userId = mongoose.Types.ObjectId(req.user.id);
    
    let flats = await ppMongoFlat.find({ 
        $and: [
            {
                publishAnnouncement: true,
            },
            { 
                flatLikes: {
                    _id: userId 
                },//flatLikes
            },
            {
                flatTrashed: { 
                    $not:   { 
                                $elemMatch: { _id: userId } 
                    } //not
                },//flatTrashed  
            },          
        ]
    }).sort( { publishedOnDate: 1 } );//find
    res.send(flats);

}, //getAllFlats

async getUsersFlats (req, res) {

    let userId = mongoose.Types.ObjectId(req.user.id);

    let flats = await ppMongoFlat.find({
        creator: {
            _id: userId
        }
    }).sort( { publishedOnDate: 1 } );//find
    res.send(flats);

},//getUsersFlats

/********************************/

async getTrashedFlats (req, res) {
  
    let userId = mongoose.Types.ObjectId(req.user.id);

    let flats = await ppMongoFlat.find({
        publishAnnouncement: true,
        flatTrashed: { 
                $elemMatch: { _id: userId } 
            }//flatTrashes
    }).sort( { publishedOnDate: 1 } );//find
    res.send(flats);

}, //getAllFlats

/********************************/

async getBookmarkedFlats (req, res) {
  
    let userId = mongoose.Types.ObjectId(req.user.id);
    
    let flats = await ppMongoFlat.find({ 
        publishAnnouncement: true,
        $and: [
            { 
                flatBookmarks: {
                    _id: userId 
                },//flatLikes
            },
            {
                flatTrashed: { 
                    $not:   { 
                                $elemMatch: { _id: userId } 
                    } //not
                },//flatTrashed  
            },          
        ]
    }).sort( { publishedOnDate: 1 } );//find
    res.send(flats);

}, //getBookmarkedFlats

/********************************/

async getFlatForEdit (req, res) {
  
    let strUserId = req.user.id || '';
    let strFlatId = req.query.flatId || '';
    let flatObjId = mongoose.Types.ObjectId(strFlatId); 

    /* The flat object ID in mongodb is a 24 character string
        If we get for example 123456 instead of 5b8b45a89770c195f8d8a26b
        then findById of mongoose is going to cause a crash.        
    */

   if(strFlatId.length === 24) {

    let flatFound = await ppMongoFlat.findById(flatObjId);

    if(flatFound.creator._id.toString() === strUserId){
        res.send(flatFound);
    } else {
        res.send({error: 'Provided user did not create this announcement!'});
       
    }

   } else {
       res.send({error: 'FlatID not provided or invalid!'});
   }



    //res.send(flats);

}, //getFlatForEdit

/********************************/

createFlat (req, res) {

    //console.log("Server has got: ", req.body);

    const flatObject = {...req.body, creator: req.user.id}

    const newFlat = new ppMongoFlat(flatObject);
    newFlat.save()
    .then( newlyCreatedFlat => {
        //console.log("Server has created Flat: ", newlyCreatedFlat);
        
        const flatId = newlyCreatedFlat._id;

        const flatImages = newlyCreatedFlat.flatImages;

        flatImages.forEach( image => {
            cloudinary.v2.uploader.remove_tag('without_announcement', image.public_id);
            cloudinary.v2.uploader.add_tag(`flatid_${flatId}`, image.public_id)
        });

        res.send(newlyCreatedFlat)
    })

}, //createFlat

async editFlat (req,res) {

    let flatId = mongoose.Types.ObjectId(req.body.flatId);

    const flatToEdit = await ppMongoFlat.findById(flatId);

    flatToEdit.typeOfProperty           = req.body.typeOfProperty;
    flatToEdit.furnishingType           = req.body.furnishingType;
    flatToEdit.epcRatingType            = req.body.epcRatingType;
    flatToEdit.solarOrientation         = req.body.solarOrientation;
    flatToEdit.availableFromDate        = req.body.availableFromDate;
    flatToEdit.createdOnDate            = req.body.createdOnDate;
    flatToEdit.publishedOnDate          = req.body.publishedOnDate;
    flatToEdit.country                  = req.body.country;
    flatToEdit.address_details          = req.body.address_details;
    flatToEdit.latlng                   = req.body.latlng;
    flatToEdit.constructionYearKnown    = req.body.constructionYearKnown;
    flatToEdit.hasAdditionalCosts       = req.body.hasAdditionalCosts;
    flatToEdit.hasAirCon                = req.body.hasAirCon;
    flatToEdit.hasBalcony               = req.body.hasBalcony;
    flatToEdit.hasBasement              = req.body.hasBasement;
    flatToEdit.hasBuiltInClosets        = req.body.hasBuiltInClosets;
    flatToEdit.hasDishwasher            = req.body.hasDishwasher;
    flatToEdit.hasElevator              = req.body.hasElevator;
    flatToEdit.hasFridge                = req.body.hasFridge;
    flatToEdit.hasGarage                = req.body.hasGarage;
    flatToEdit.hasGarden                = req.body.hasGarden;
    flatToEdit.hasHeating               = req.body.hasHeating;
    flatToEdit.hasMicrowave             = req.body.hasMicrowave;
    flatToEdit.hasParquetFlooring       = req.body.hasParquetFlooring;
    flatToEdit.hasPlayground            = req.body.hasPlayground;
    flatToEdit.hasPool                  = req.body.hasPool;
    flatToEdit.hasReinforcedDoor        = req.body.hasReinforcedDoor;
    flatToEdit.hasStove                 = req.body.hasStove;
    flatToEdit.hasTV                    = req.body.hasTV;
    flatToEdit.hasWashingMachine        = req.body.hasWashingMachine;
    flatToEdit.isForPets                = req.body.isForPets;
    flatToEdit.isForSmokers             = req.body.isForSmokers;
    flatToEdit.isForStudents            = req.body.isForStudents;
    flatToEdit.publishStreetNumber      = req.body.publishStreetNumber;
    flatToEdit.renovationYearKnown      = req.body.renovationYearKnown;
    flatToEdit.numberOfRooms            = req.body.numberOfRooms;
    flatToEdit.numberOfBathRooms        = req.body.numberOfBathRooms;
    flatToEdit.surfaceTotal             = req.body.surfaceTotal;
    flatToEdit.surfaceNet               = req.body.surfaceNet;
    flatToEdit.floor                    = req.body.floor;
    flatToEdit.floorTotal               = req.body.floorTotal;
    flatToEdit.rent                     = req.body.rent;
    flatToEdit.currency                 = req.body.currency;
    flatToEdit.paymentPeriod            = req.body.paymentPeriod;
    flatToEdit.deposit                  = req.body.deposit;
    flatToEdit.additionalCostsAmount    = req.body.additionalCostsAmount;
    flatToEdit.additionalCostsPaymentMethod = req.body.additionalCostsPaymentMethod;
    flatToEdit.constructionYear         = req.body.constructionYear;
    flatToEdit.renovationYear           = req.body.renovationYear;
    flatToEdit.flatImages               = req.body.flatImages;
    flatToEdit.headline                 = req.body.headline;
    flatToEdit.descr_short              = req.body.descr_short;
    flatToEdit.descr_long               = req.body.descr_long;

    flatToEdit.save( (error, result) => {
        if (result) res.send({status: 'OK'})
    });

   
    const flatImages = flatToEdit.flatImages;

    //remove the without_announcement tag from the images that have been
    //uploaded and place a tag with the flat id

    flatImages.forEach( image => {
        cloudinary.v2.uploader.remove_tag('without_announcement', image.public_id);
        cloudinary.v2.uploader.add_tag(`flatid_${flatId}`, image.public_id)
    });

}, //editFlat

/********************************/

async likeFlat (req, res) {
    let flatId = req.body.flatId;
    let userId = req.user.id;

    //Grabbing the flat by its ID
    let foundFlat = await ppMongoFlat.findById(flatId);

    /* If a user already has liked the flat, he shall not
    be able to like it again. We must keep in mind that:
    typeof foundFlat.flatLikes[0]._id  is  object
    typeof req.user.id  is string
    Therefore, we must convert the object to a string. */

    let userAlreadyLikedFlat = foundFlat.flatLikes
                               .filter( like => like._id.toString() === userId )
                               .length > 0;

    /* The above expr will return true if the userId already exists
    in the flatLikes array. Otherwise it will return false */                          

    if (!userAlreadyLikedFlat) {
        foundFlat.flatLikes.unshift(userId);
        foundFlat.save().then(
            res.send({status: 'ok'}) 
            //we must send sth so that .then on client side works
        );//foundFlat.save()
    };//if
},//likeFlat


async publishFlat (req, res) {

    let flatId = req.body.flatId;

    //Grabbing the flat by its ID
    let foundFlat = await ppMongoFlat.findById(flatId);

    foundFlat.publishAnnouncement = true;
        foundFlat.save().then(
            res.send({status: 'ok'}) 
            //we must send sth so that .then on client side works
        );//foundFlat.save()

},//publishFlat


async unpublishFlat (req, res) {
    
    let flatId = req.body.flatId;

    //Grabbing the flat by its ID
    let foundFlat = await ppMongoFlat.findById(flatId);

    foundFlat.publishAnnouncement = false;
        foundFlat.save().then(
            res.send({status: 'ok'}) 
            //we must send sth so that .then on client side works
        );//foundFlat.save()

},//publishFlat

/********************************/

async deleteFlat (req, res){

    let result =  await ppMongoFlat.findByIdAndRemove(req.query.flatId);

    if(result){
        cloudinary.v2.api
            .delete_resources_by_tag(`flatid_${req.query.flatId}`,
                (error, result) => {
                    //console.log(result);
                    res.send({status: 'ok'}) 
                });
    };//if
},//deleteFlat


/********************************/

async viewedFlat (req, res) {

    let flatId = req.body.flatId;
    let userId = req.user.id;

    //Grabbing the flat by its ID
    let foundFlat = await ppMongoFlat.findById(flatId);

    /* If a user already has viewed the flat, we will not
    add him to this array again. Only the first time that
    the user has viewed the flat counts for us.
    */

    let userAlreadyViewedFlat = foundFlat.flatViewed
                               .filter( user => user._id.toString() === userId )
                               .length > 0;

    /* The above expr will return true if the userId already exists
    in the flatViewed array. Otherwise it will return false */                          

    if (!userAlreadyViewedFlat) {
        foundFlat.flatViewed.unshift(userId);
        foundFlat.save().then(
            res.send({status: 'ok'}) 
            //we must send sth so that .then on client side works
        );//foundFlat.save()
    }//if
    else {
        res.send({error: 'User already viewed flat'}) 
    }
},//likeFlat

/********************************/

async unlikeFlat (req, res) {

    let flatId = req.body.flatId;
    let userId = req.user.id;

    //Grabbing the flat by its ID
    let foundFlat = await ppMongoFlat.findById(flatId);

    //If the user has not yet liked the flat, he won't be able to unlike it!
    let userHasntLikedFlatYet = foundFlat.flatLikes
                               .filter( like => like._id.toString() === userId )
                               .length === 0;   
                               
    if(!userHasntLikedFlatYet) {

        /* Because the objectIds are objects, we convert each one to a string which
        will give us an array of strings. We then check via indexOf the position of
        the userId provided in the request
        */
        let pos = foundFlat.flatLikes.map( like => like._id.toString() ).indexOf(userId);

        /* We splice out of the flatLikes Array the item (and just one item) at the position
        that we've found above. */

        foundFlat.flatLikes.splice(pos, 1);

        foundFlat.save().then(
            res.send({status: 'ok'}) 
            //we must send sth so that .then on client side works
        );//foundFlat.save()

    };//if                        
},//unlikeFlat

/********************************/

async bookmarkFlat (req, res) {
    let flatId = req.body.flatId;
    let userId = req.user.id;

    //Grabbing the flat by its ID
    let foundFlat = await ppMongoFlat.findById(flatId);

    /* If a user already has liked the flat, he shall not
    be able to like it again. We must keep in mind that:
    typeof foundFlat.flatLikes[0]._id  is  object
    typeof req.user.id  is string
    Therefore, we must convert the object to a string. */

    let userAlreadyBookmarkedFlat = foundFlat.flatBookmarks
                               .filter( bookmark => bookmark._id.toString() === userId )
                               .length > 0;

    /* The above expr will return true if the userId already exists
    in the flatLikes array. Otherwise it will return false */                          

    if (!userAlreadyBookmarkedFlat) {
        foundFlat.flatBookmarks.unshift(userId);
        foundFlat.save().then(
            res.send({status: 'ok'}) 
            //we must send sth so that .then on client side works
        );//foundFlat.save()
    };//if
},//bookmarkFlat

/********************************/

async unbookmarkFlat (req, res) {

    let flatId = req.body.flatId;
    let userId = req.user.id;

    //Grabbing the flat by its ID
    let foundFlat = await ppMongoFlat.findById(flatId);

    //If the user has not yet liked the flat, he won't be able to unlike it!
    let userHasntBookmarkedFlatYet = foundFlat.flatBookmarks
                               .filter( bookmark => bookmark._id.toString() === userId )
                               .length === 0;   
                               
    if(!userHasntBookmarkedFlatYet) {

        /* Because the objectIds are objects, we convert each one to a string which
        will give us an array of strings. We then check via indexOf the position of
        the userId provided in the request
        */
        let pos = foundFlat.flatBookmarks.map( bookmark => bookmark._id.toString() ).indexOf(userId);

        /* We splice out of the flatLikes Array the item (and just one item) at the position
        that we've found above. */

        foundFlat.flatBookmarks.splice(pos, 1);

        foundFlat.save().then(
            res.send({status: 'ok'}) 
            //we must send sth so that .then on client side works
        );//foundFlat.save()

    };//if                        
},//unbookmarkFlat


/********************************/

async trashFlat (req, res) {
    let flatId = req.body.flatId;
    let userId = req.user.id;

    //Grabbing the flat by its ID
    let foundFlat = await ppMongoFlat.findById(flatId);

    /* If a user already has liked the flat, he shall not
    be able to like it again. We must keep in mind that:
    typeof foundFlat.flatLikes[0]._id  is  object
    typeof req.user.id  is string
    Therefore, we must convert the object to a string. */

    let userAlreadyTrashedFlat = foundFlat.flatTrashed
                               .filter( trashed => trashed._id.toString() === userId )
                               .length > 0;

    /* The above expr will return true if the userId already exists
    in the flatLikes array. Otherwise it will return false */                          

    if (!userAlreadyTrashedFlat) {
        foundFlat.flatTrashed.unshift(userId);
        foundFlat.save().then(
            res.send({status: 'ok'}) 
            //we must send sth so that .then on client side works
        );//foundFlat.save()
    };//if
},//trashFlat

/********************************/

async untrashFlat (req, res) {

    let flatId = req.body.flatId;
    let userId = req.user.id;

    //Grabbing the flat by its ID
    let foundFlat = await ppMongoFlat.findById(flatId);

    //If the user has not yet liked the flat, he won't be able to unlike it!
    let userHasntTrashedFlatYet = foundFlat.flatTrashed
                               .filter( trashed => trashed._id.toString() === userId )
                               .length === 0;   
                               
    if(!userHasntTrashedFlatYet) {

        /* Because the objectIds are objects, we convert each one to a string which
        will give us an array of strings. We then check via indexOf the position of
        the userId provided in the request
        */
        let pos = foundFlat.flatTrashed.map( trashed => trashed._id.toString() ).indexOf(userId);

        /* We splice out of the flatLikes Array the item (and just one item) at the position
        that we've found above. */

        foundFlat.flatTrashed.splice(pos, 1);

        foundFlat.save().then(
            res.send({status: 'ok'}) 
            //we must send sth so that .then on client side works
        );//foundFlat.save()

    };//if                        
},//untrashFlat

/********************************/

async addComment (req, res) {

    const {flatId, userId, createdAt, comment} = req.body;
    
    let newComment = {
        user: userId, 
        createdAt, 
        comment
    };

    //Grabbing the flat by its ID
    let foundFlat = await ppMongoFlat.findById(flatId);

    foundFlat.flatComments.unshift(newComment);
    foundFlat.save().then(
        res.send({status: 'ok'}) 
        //we must send sth so that .then on client side works
    );//foundFlat.save()    


}, //addComment

/********************************/

async deleteComment (req, res) {

    let flatId = req.query.flatId;
    let commentId = req.query.commentId;

    //Grabbing the flat by its ID
    let foundFlat = await ppMongoFlat.findById(flatId);

    //filtering out of the array the id of the comment that has to be deleted
    //and saving the outcome in a new arrray
    let newArr = foundFlat.flatComments
                .filter( comment => comment._id.toString() !== commentId );

    //overwriting the value of the flatComments array with the new array
    foundFlat.flatComments = newArr;

    foundFlat.save().then(
        res.send({status: 'ok'}) 
    )

}, //deleteComment

}//module exports