/** Import gravatar, a services that provides user images */
const gravatar = require('gravatar');

/** Import bcrypt.js that allows us to encrypt user passwords */
const bcrypt = require('bcryptjs');

/** Import jwt so that we can send a token to the client after login */
const jwt = require('jsonwebtoken');
/** Import secretOrKey that we use to sign the JWT token */
const secretOrKey = require('../config/keys').secretOrKey;

/** Import passport that we'll use for protecting private routes */
const passport = require('passport');

/** Import the isEmpty fct from lodash */
const loDashIsEmpty = require('lodash.isempty');

const mongoose = require('mongoose');

/** Import our input validation */
const validateRegisterFormData = require('../form-validation/validate-register');
const validateLoginFormData = require('../form-validation/validate-login');

/** Defining Messages */
const SIGNUP_ERROR_EMAIL_ALREADY_EXISTS_IN_DB = 'This account already exists.';
const SIGNIN_ERROR_EMAIL_NOT_IN_DB = 'User email not found.';
const SIGNIN_ERROR_PASSWORD_INCORRECT = 'Password incorrect.';

/** Defining HTTP response stati 
 * See: https://gist.github.com/subfuzion/669dfae1d1a27de83e69
*/
const HTTP_ST_FOR_OK = 200;
const HTTP_ST_FOR_VALIDATION_ERROR = 400;
const HTTP_ST_FOR_NOT_FOUND_ERROR = 404;


/** load MongoDB Schema that will also be used to
 * make operations on the DB
 */
const ppMongoUser = require('../models/User');

/******************************************************* */

module.exports = {

  signUp(req, res) {

    /* We validate the fields provided in req.body 
    and if there are validation errors, we send them
    back to the client. Else, we proceed with the
    signup procedure */
  
    const validateResults = validateRegisterFormData(req.body);
  
    if(!loDashIsEmpty(validateResults) ){
      return res.status(HTTP_ST_FOR_VALIDATION_ERROR).json(validateResults);
      //if there are errors, this kicks us out from the signup process
    };
 
    /** Check if the email already exist in the DB*/
    ppMongoUser.findOne({email: req.body.email})
               .then( possiblyAlreadyExistingUser => {
  
                  /** Check if possiblyAlreadyExistingUser has a value */
                  if (possiblyAlreadyExistingUser){
                    /** If possiblyAlreadyExistingUser has a value:  */
  
                   
  
                    return res.status(HTTP_ST_FOR_VALIDATION_ERROR)
                              .json({email: SIGNUP_ERROR_EMAIL_ALREADY_EXISTS_IN_DB});
                  } else {
  
                    /** Here we handle the case that the user doesn't exist 
                     * yet in the DB. Let's create it. 
                    */
                  
  
                    const avatar = gravatar.url(req.body.email, {s:200, r: 'pg', d:'mm'});
                   
                    const newUser = new ppMongoUser({
                      firstname: req.body.firstname,
                      lastname: req.body.lastname, 
                      email:  req.body.email,
                      password:  req.body.password,
                      avatar                  
                    }); //newUser
  
                    /** Generating a Salt with 10 random chars */
                    bcrypt.genSalt(10, (possibleSaltError, generatedSalt) => {
  
                      /** Use the generatedSalt to hash the plain text user password */
                      bcrypt.hash(newUser.password, generatedSalt, (possibleHashError, generatedHash) => {
                        if(possibleHashError) throw possibleHashError;
  
                        //Here, we replace the value of newUser.password with the generatedHash
                        newUser.password = generatedHash;
  
                        //And now we're fine to write the newUser to the DB:
                        newUser.save()
                               .then( newCreatedUser => {
                                 res.json(newCreatedUser)
                                })
                               .catch(errorWhileWritingUser => console.log(errorWhileWritingUser) );                   
  
                      });//bcrypt.hash
                    });//bcrypt.genSalt
  
                  }//if possiblyAlreadyExistingUser
  
               }); //then of ppUserModel.findOne
  
  },//signUp


  async updateProfile (req, res) {

      let updatingPassword = false;
    
      if (req.body.password === '' && req.body.password2 === ''){
        req.body.password   = 'Pass the validation!';
        req.body.password2  = 'Pass the validation!';
      } else {
        updatingPassword = true;
      }
    
      const validateResults = validateRegisterFormData(req.body);

      if(!loDashIsEmpty(validateResults) ){
        return res.status(HTTP_ST_FOR_VALIDATION_ERROR).json(validateResults);
        //if there are errors, this kicks us out from the signup process
      };
    
      //Grab user by ID

      let userId = mongoose.Types.ObjectId(req.user.id);
      const userToUpdate = await ppMongoUser.findById(userId);

      //at this point we should not use gravatar because it will overwrite
      //the link to the user profile image on cloudinary
                     
      userToUpdate.firstname = req.body.firstname;
      userToUpdate.lastname = req.body.lastname;
      userToUpdate.email = req.body.email;     

      if (updatingPassword) {

        userToUpdate.password = req.body.password;

        /** Generating a Salt with 10 random chars */
        bcrypt.genSalt(10, (possibleSaltError, generatedSalt) => {

          /** Use the generatedSalt to hash the plain text user password */
          bcrypt.hash(userToUpdate.password, generatedSalt, (possibleHashError, generatedHash) => {
            if(possibleHashError) throw possibleHashError;

            //Here, we replace the value of newUser.password with the generatedHash
            userToUpdate.password = generatedHash;

            //And now we're fine to write the newUser to the DB:
            userToUpdate.save()
              .then( result => {
                res.send({
                  firstname: userToUpdate.firstname,
                  lastname:  userToUpdate.lastname,
                  email:  userToUpdate.email,
                  avatar: userToUpdate.avatar
                });//send
              })
              .catch(errorWhileWritingUser => console.log(errorWhileWritingUser) );   
                    
          });//bcrypt.hash
        });//bcrypt.genSalt

      } else {

        userToUpdate.save()
        .then( result => {
          res.send({
            firstname: userToUpdate.firstname,
            lastname:  userToUpdate.lastname,
            email:  userToUpdate.email,
            avatar: userToUpdate.avatar
          });//send
        })
        .catch(errorWhileWritingUser => console.log(errorWhileWritingUser) );   
          
      }; // if (updatingPassword)

  },//updateProfile


/*************************************************** */

  signIn(req, res) {

    /* We validate the fields provided in req.body 
    and if there are validation errors, we send them
    back to the client. Else, we proceed with the
    signin procedure */
    
    const validateResults = validateLoginFormData(req.body);
    
    if(!loDashIsEmpty(validateResults) ){
      return res.status(HTTP_ST_FOR_VALIDATION_ERROR).json(validateResults);
      //if there are errors, this kicks us out from the signup process
    };
    
      /** Checking if the provided email exists in the DB */
      ppMongoUser.findOne({email: req.body.email})
      .then( foundUserInDB => {
    
        if(!foundUserInDB){
          /**if foundUserInDB does not contain a value, it means that the
           * user doesn't exist in the DB
          */
          return res.status(HTTP_ST_FOR_NOT_FOUND_ERROR).json({email: SIGNIN_ERROR_EMAIL_NOT_IN_DB})
        } else {
          /** Here we handle the case that the user email exists in the DB.
           * We compare the passwords (in DB: hashed, in req.body: plain text) using bycrypt
           */
          bcrypt.compare(req.body.password, foundUserInDB.password)
                .then( passwordsDoMatch => {
                  if(passwordsDoMatch){
                    
                    /** We send a token to the client. Upon new requests, the
                     * client can send the token back to us which will confirm
                     * that the client had logged in correctly.
                     */
    
                    //Create the payload that we will put into the token:
                    const payload = {
                      id: foundUserInDB.id,
                      firstname: foundUserInDB.firstname,
                      lastname: foundUserInDB.lastname,
                      email:  foundUserInDB.email,
                      avatar: foundUserInDB.avatar,
                      joindate: foundUserInDB.joindate,
                    } //jwt payload 
    
                    /** charge the payload onto the token and sign it with 
                     * the secretOrKey and have token expire in 432000 seconds
                     * which is five days. Other values:
                     * One day: 86400
                     * One week: 604800
                     * One hour: 3600 
                     * One minute: 60 
                     * */
                    jwt.sign(payload,
                      secretOrKey,
                      { expiresIn: 604800 }, 
                      (error, generatedToken) => {
                        res.json({
                          success: true,
                          token: 'Bearer ' + generatedToken
                        });//res.json
                      }); //jwt.sign
    
                  }else{
                    return res.status(HTTP_ST_FOR_VALIDATION_ERROR).json({password: SIGNIN_ERROR_PASSWORD_INCORRECT});
                  }
                })//then of bcrypt.compare
        }//of if(!foundUserInDB)
    
      });//ppUserModel.findOne().then
    }, //signIn


/*************************************************** */

    getInfo (req,res) {

      let userId = mongoose.Types.ObjectId(req.user.id);

      ppMongoUser.findById(userId)
      .then( foundUserInDB => {
        res.send({
          firstname: foundUserInDB.firstname,
          lastname:  foundUserInDB.lastname,
          email:  foundUserInDB.email,
          avatar: foundUserInDB.avatar
        });//send
      });//then
    }//getinfo

}//module.exports