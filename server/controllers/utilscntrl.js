const cloudinary = require('cloudinary');

const ppMongoUser = require('../models/User');

cloudinary.config({
  cloud_name: 'proxipiso',
  api_key: '227542732957667',
  api_secret: 'gBwmwHL0E2G7_lqef7953U3CHJA',
});

module.exports = {

  deletePictureFromCloudinary (req, res) {

    //We want to delete the image with the public_id that came via req.query.public_id

    //First, we want to know if that image really exists in cloudery:
    cloudinary.v2.search
    .expression(`public_id=${req.query.public_id}`)
    .with_field('tags')
    .execute().then(result => {
      
      if (result.total_count === 1){

      //In this if branch we handle the case that the search has resulted in exactly
      //one image which means that the image that we want to delete does really exist.

      //Now, let's check the tags array of that image and see if that array includes
      //the user id. Remember: when a user uploads an image to cloudinary, we add to
      //the image a tag that contains the user id which comes via the request header.
      //This info in the header only comes in if the user was successfully logged in
      //and if a token exists on the user's machine. This way we make sure that any
      //other user can send a delete request for an image that he does not own!

        if (result.resources[0].tags.includes(`userid_${req.user.id}`)){

          cloudinary.v2.api.delete_resources(req.query.public_id, (error, result) => {

            if (error) return res.status(500).send(error);
        
             res.status(200).send({deleted: Object.keys(result.deleted)[0]})    
           });//cloudinary.v2.api.delete_resources

        } else {
          res.status(401).send({error: 'User not allowed to delete this image'})
        }//if (result.resources[0].tags.includes(req.user.id))

      } else {
        res.status(404).send({error: 'Image not found on Cloudery'})
      }//if (result.total_count === 1)

    })//.execute().then
  
  }, //deletePictureFromCloudinary

  uploadPictureToCloudinary (req, res) {

    /** Cloudinary allows to upload one single file per ajax call */

    let file2upload = req.files.newFlatImage;

    let clConfig = {
      folder: `flatimg`, 
      upload_preset: "proxipiso-preset1",
      tags: ['without_announcement',`userid_${req.user.id}`]
    };

    cloudinary.v2.uploader.upload(file2upload.path, clConfig , (error, result) =>{

      if (error) return res.status(500).send({error})

      let storedPicture = {
        public_id: result.public_id,
        sizeInKB: (result.bytes / 1024 ).toFixed(2),
        url: result.secure_url,
        thumb_url: `https://res.cloudinary.com/proxipiso/image/upload/c_thumb,w_300,g_center/${result.public_id}`,
        old_name: file2upload.name
      }
      
      res.status(200).send(storedPicture);

    });//cloudinary.v2.uploader.upload
    
},//uploadPictureToCloudinary


uploadProfilePictureToCloudinary (req, res){

//Find the previous profile image by ID and delete it
  cloudinary.v2.api
  .delete_resources_by_tag(`profimg_userid_${req.user.id}`,
      (error, result) => {

        //Upload the new profile image:

        let file2upload = req.files.newProfileImage;

        let clConfig = {
          folder: `profileimg`, 
          upload_preset: "proxipiso-preset1",
          tags: [`profimg_userid_${req.user.id}`]
        };
    
        cloudinary.v2.uploader.upload(file2upload.path, clConfig , 
          async (error, result) =>{
    
          if (error) return res.status(500).send({error})
    
          let storedPicture = {
            public_id: result.public_id,
            sizeInKB: (result.bytes / 1024 ).toFixed(2),
            url: result.secure_url,
            thumb_url: `https://res.cloudinary.com/proxipiso/image/upload/c_thumb,w_300,g_center/${result.public_id}`,
            old_name: file2upload.name
          }
          
          //update the user record with the new profile picture:
          const currentUser = await ppMongoUser.findById(req.user.id);
          currentUser.avatar = storedPicture.url;

          currentUser.save()
          .then(result => {

          //send storedPicture to the client
          res.status(200).send(storedPicture);

          });//currentUser.save()
    
        });//cloudinary.v2.uploader.upload

      });//delete_resources_by_tag

},//uploadProfilePictureToCloudinary

}