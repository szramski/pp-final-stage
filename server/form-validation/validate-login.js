/** Import the validator npm module */
const validator = require('validator');

/** Import the isEmpty fct from lodash */
const loDashIsEmpty = require('lodash.isempty');

/** The following fct will check the values in the Login Form */
const validateLoginFormData = ({email, password}) => {

  //We initialize the variable validationErrors as an empty object
  let validationErrors = {};

  /*Because we want to provide one error message per input, we check
    first if the provided strings match the appropriate criteria.
    Afterwards, we check if the provided strings are empty. 
    Example: If the password shall be between 6 and 20 chars but if
    it is "", then the first rule will be overwritten by the second
    one. Else, the first rule will be valid.
  */

  if( email != undefined &&  !validator.isEmail(email) ) {
    validationErrors.email = 'Please enter a valid email address.';
  }

  if( password != undefined &&  !validator.isLength(password, {min: 3, max: 30}) ) {
    validationErrors.password = 'Password must be between 3 and 30 characters.';
  }

  /** Lodash isEmpty function returns true if it checks an empty string, an empty object,
   * an empty array or values like undefined
   */

  if( loDashIsEmpty(email) ){
    validationErrors.email = 'Email is required.';
  }

  if( loDashIsEmpty(password) ){
    validationErrors.password = 'Password is required.';
  }  

  return validationErrors;

}//of validateLoginFormData

module.exports = validateLoginFormData;