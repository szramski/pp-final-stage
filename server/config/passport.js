const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;

/** Import the user model */
const ppUserModel = require('../models/User');

/** Import secretOrKey that we use to sign the JWT token */
const secretOrKey = require('./keys').secretOrKey;

const optionsForMyJwtStrategy = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey
};

const myJwtStrategy = new JwtStrategy(optionsForMyJwtStrategy, (jwt_payload_from_token, done) => {

  /** The myJwtStrategy has been configured with optionsForMyJwtStrategy which make it use
   * the secretOrKey that we used to sign the token. Also, we save in the property jwtFromRequest 
   * the result of ExtractJwt.fromAuthHeaderAsBearerToken()
   */

  ppUserModel.findById(jwt_payload_from_token.id)
  /* We next look if the id that came in the token exists in the database */
            .then( userFoundInDB => {
              if(userFoundInDB){
                //if the user id exists in the db, then we return the done object
                //which will complete the authentication in the corresponding private route
                return done(null, userFoundInDB);
              } else {
                console.log(error)
                return done(error, null);
              }
            })
            .catch( error => console.log(error) );
});//myJwtStrategy

module.exports = passport => {
  passport.use(myJwtStrategy);
}