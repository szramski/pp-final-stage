/** 
 * Proxipiso Express Server - main file 
*/

/** import express npm module */
const express = require('express');

/** import mongoose driver for mongoDB */
const mongoose = require('mongoose');

/** import mongoDB URI from config file */
const dbURI = require('./config/keys').mongoURI;

/** connect to the mongoDB which will return a promise 
 *  Then, in a callback fct, console.log that we're connected
 *  And if there is an error, console.log that error
*/
mongoose.connect(dbURI, { useNewUrlParser: true })
        .then( () => console.log('Connected to MongoDB ds235711.mlab.com:35711/proxipiso'))
        .catch( errDuringConnect => console.log('Error during connect to mongoDB: ', errDuringConnect) );  
  
        
/** import path module */
const path = require('path');

/** initialize the express server app */
const ppServerApp = express();

/** define on which port the ppServerApp will be listening */
const PORT = process.env.PORT || 5000;

/** ============= ADD MIDDLEWARE ============= */

/** bodyParser: makes it possible to read req.body from HTTP requests */
/** import bodyParser MW */
const bodyParser = require('body-parser');
ppServerApp.use( bodyParser.urlencoded( {extended: false} ) );
ppServerApp.use( bodyParser.json() );

/** import passport MW */
const passport = require('passport');
ppServerApp.use( passport.initialize() );
/** make passport use config file:*/
require('./config/passport')(passport);

/** import & use cors MW */
const cors = require('cors');
ppServerApp.use(cors())

/** ============= ROUTES ============= */
/** import routes that the ppServerApp will serve */
const usersRoute = require('./routes/api/users');
const flatsRoute = require('./routes/api/flats');
const utilsRoute = require('./routes/api/utils');

/** make the ppServerApp use the imported routes */
ppServerApp.use('/api/users',   usersRoute);
ppServerApp.use('/api/flats',   flatsRoute);
ppServerApp.use('/api/utils',   utilsRoute);

/** Serve static assets when in production environment */
if(process.env.NODE_ENV === 'production'){
  ppServerApp.use(express.static('client/build'));
  ppServerApp.get('*', (req,res) => {
    res.sendFile( path.resolve(__dirname, 'client', 'build', 'index.html') );
  })
}//if
/*********************************************** */
/** start the ppServerApp */
ppServerApp.listen(PORT, ()=>{
  console.clear();
  console.log('-------------------');
  console.log(`ppServerApp up & listening on port ${PORT}.`);
  console.log(`For dev: CTRL+click on http://localhost:${PORT}/`);
});//ppServerApp.listen

