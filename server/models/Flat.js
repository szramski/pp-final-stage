/** import mongoose DB driver that already hosts the connection */
const mongoose = require('mongoose');

const autopopulate = require('mongoose-autopopulate');

/** define the Schema variable out of the mongoose driver */
let Schema = mongoose.Schema;

/** define the Schema for the proxipiso User that will be put into the DB */
const ppFlatSchema = new Schema({

  publishAnnouncement:              {type: "Boolean", default: false},
  additionalCostsAmount:            {type: "Number"},
  additionalCostsPaymentMethod:     {type: "String"},
  address_details:                  { 
                                      city:             {type: "String"},
                                      country:          {type: "String"},
                                      country_short:    {type: "String"},
                                      formattedAddress: {type: "String"},
                                      postalcode:       {type: "String"},
                                      region:           {type: "String"},
                                      streetName:       {type: "String"},
                                      streetNumber:     {type: "String"}
                                    },
  availableFromDate:                {type: "Date"},
  publishedOnDate:                  {type: "Date"},
  createdOnDate:                    {type: "Date"},
  constructionYear:                 {type: "Number"},
  constructionYearKnown:            {type: "Boolean", default: false},
  country:                          {type: "String"},
  creator:                          {
                                      type: "ObjectId", 
                                      required: true, 
                                      ref: 'pp_collection_users', 
                                      autopopulate: { select: 'firstname lastname avatar'},
                                    },
  currency:                         {type: "String"},
  deposit:                          {type: "Number"},
  descr_long:                       {type: "String"},
  descr_short:                      {type: "String"},
  epcRatingType:                    {type: "String"},
  flatImages:                       [{
                                      public_id:        {type: "String"},
                                      sizeInKB :        {type: "String"},
                                      url:              {type: "String"},
                                      thumb_url:        {type: "String"},
                                      old_name:         {type: "String"},
                                    }],
  flatLikes:                         [{
                                      user:             {type: "ObjectId"},
                                    }],  
  flatBookmarks:                    [{
                                      user:             {type: "ObjectId"},
                                    }], 
  flatTrashed:                        [{
                                      user:             {type: "ObjectId"},
                                    }],                                                          
  flatViewed:                        [{
                                      user:             {type: "ObjectId"},
                                    }],                   
  flatComments:                     [{
                                      user: {
                                        type: "ObjectId", 
                                        required: true, 
                                        ref: 'pp_collection_users', 
                                        autopopulate: { select: 'firstname lastname avatar'},
                                      },
                                      createdAt:        {type: "Number",    required: true},
                                      comment:          {type: "String",    required: true},
                                    }],     
  floor:                            {type: "Number"},
  floorTotal:                       {type: "Number"},
  furnishingType:                   {type: "String"},
  hasAdditionalCosts:               {type: "Boolean", default: false},
  hasAirCon:                        {type: "Boolean", default: false},
  hasBalcony:                       {type: "Boolean", default: false},
  hasBasement:                      {type: "Boolean", default: false},
  hasBuiltInClosets:                {type: "Boolean", default: false},
  hasDishwasher:                    {type: "Boolean", default: false},
  hasElevator:                      {type: "Boolean", default: false},
  hasFridge:                        {type: "Boolean", default: false},
  hasGarage:                        {type: "Boolean", default: false},
  hasGarden:                        {type: "Boolean", default: false},
  hasHeating:                       {type: "Boolean", default: false},
  hasMicrowave:                     {type: "Boolean", default: false},
  hasParquetFlooring:               {type: "Boolean", default: false},
  hasPlayground:                    {type: "Boolean", default: false},
  hasPool:                          {type: "Boolean", default: false},
  hasReinforcedDoor:                {type: "Boolean", default: false},
  hasStove:                         {type: "Boolean", default: false},
  hasTV:                            {type: "Boolean", default: false},
  hasWashingMachine:                {type: "Boolean", default: false},
  headline:                         {type: "String"},
  isForPets:                        {type: "Boolean", default: false},
  isForSmokers:                     {type: "Boolean", default: false},
  isForStudents:                    {type: "Boolean", default: false},
  latlng:                           { 
                                      lat:             {type: "Number"},
                                      lng:             {type: "Number"},
                                    },
  numberOfBathRooms:                {type: "Number"},
  numberOfRooms:                    {type: "Number"},
  paymentPeriod:                    {type: "String"},
  publishStreetNumber:              {type: "Boolean", default: false},
  renovationYear:                   {type: "Number"},
  renovationYearKnown:              {type: "Boolean", default: false},
  rent:                             {type: "Number"},
  solarOrientation:                 {type: "String"},
  surfaceNet:                       {type: "Number"},
  surfaceTotal:                     {type: "Number"},
  typeOfProperty:                   {type: "String"},

});

ppFlatSchema.plugin(autopopulate);

/** export the ppUserSchema */
module.exports = ppMongoFlat = mongoose.model('pp_collection_flats', ppFlatSchema);