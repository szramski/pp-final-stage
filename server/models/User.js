/** import mongoose DB driver that already hosts the connection */
const mongoose = require('mongoose');

/** define the Schema variable out of the mongoose driver */
let Schema = mongoose.Schema;

/** define the Schema for the proxipiso User that will be put into the DB */
const ppUserSchema = new Schema({
  firstname:        {type: String, required: true},
  lastname:         {type: String, required: true},
  email:            {type: String, required: true},
  password:         {type: String, required: true},
  avatar:           {type: String},
  joindate:         {type: Date, default: Date.now}
});

/** export the ppUserSchema */
module.exports = ppMongoUser = mongoose.model('pp_collection_users', ppUserSchema);