/** import mongoose DB driver that already hosts the connection */
const mongoose = require('mongoose');

/** define the Schema variable out of the mongoose driver */
let Schema = mongoose.Schema;

/** define the Schema for the proxipiso User that will be put into the DB */
const ppCountrySchema = new Schema({
  shortcode:        {type: String, required: true},
  translation:      {
                      de:   {type: String, required: true},
                      en:   {type: String, required: true},
                      pl:   {type: String, required: true},
                      es:   {type: String, required: true}
                    }
});

/** export the ppUserSchema */
module.exports = ppMongoCountry = mongoose.model('pp_collection_countries', ppCountrySchema);