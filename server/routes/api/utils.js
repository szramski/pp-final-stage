const express = require('express');
const expressRouter = express.Router();


//var multer  = require('multer');

var fileParser = require('connect-multiparty')();

/** Import passport that we'll use for protecting private routes */
const passport = require('passport');

/** load MongoDB Schema that will also be used to
 * make operations on the DB
 */
const ppMongoCountry = require('../../models/Country');

/** Importing the Utils Controller to handle the routes */
const utilsController = require('../../controllers/utilscntrl');

expressRouter.get('/countries', (req, res) => {
  
  ppMongoCountry.find()
             .then( foundCountries => {
                res.json(foundCountries);
             })
             //.catch( error => console.log(error));
});//expressRouter.get for /api/utils/countries

expressRouter.put('/cloudinary/upload', 
                  passport.authenticate('jwt', {session: false}), 
                  fileParser, 
                  utilsController.uploadPictureToCloudinary 
                );

expressRouter.put('/cloudinary/uploadProfileImage', 
                  passport.authenticate('jwt', {session: false}), 
                  fileParser, 
                  utilsController.uploadProfilePictureToCloudinary 
                  );

expressRouter.delete('/cloudinary/delete', 
                      passport.authenticate('jwt', {session: false}), utilsController.deletePictureFromCloudinary );



module.exports = expressRouter;