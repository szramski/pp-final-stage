const express = require('express');
const expressRouter = express.Router();

const USER_ROUTE_TEST_MSG = 'Ey Alter, was gehd in users route?';

/** Importing the User Controller to handle the routes */
const userControler = require('../../controllers/usercntrl');

/** Import passport that we'll use for protecting private routes */
const passport = require('passport');

/** ================= ROUTES =================== */

/**=============================================
 * TEST ROUTE
 * =============================================
 * @route   GET /api/users/
 * @desc    Tests users route
 * @access  Public
 */
expressRouter.get('/', (req, res) => {
  res.send({msg: USER_ROUTE_TEST_MSG});
});//expressRouter.get for /api/users/

/**=============================================
 * USER SIGN UP
 * =============================================
 * @route   POST /api/users/signup
 * @desc    Sign up a user
 * @access  Public
 */
expressRouter.post('/signup', userControler.signUp)

/**=============================================
 * USER SIGN IN
 * =============================================
 * @route   POST /api/users/signin
 * @desc    Sign in a user
 * @access  Public
 */
expressRouter.post('/signin', userControler.signIn );

/**=============================================
 * USER UPDATE PROFILE
 * =============================================
 * @route   POST /api/users/update
 * @desc    Update a user profile
 * @access  Public
 */
expressRouter.post('/update',  
                    passport.authenticate('jwt', {session: false}),userControler.updateProfile 
                  );

/**=============================================
 * GET USER INFO
 * =============================================
 * @route   GET /api/users/getinfo
 * @desc    Get information about the user who created a flat
 * @access  Public
 */
expressRouter.get('/getinfo', passport.authenticate('jwt', {session: false}), userControler.getInfo );

module.exports = expressRouter;