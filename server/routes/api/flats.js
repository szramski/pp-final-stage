const express = require('express');
const expressRouter = express.Router();

/** Import passport that we'll use for protecting private routes */
const passport = require('passport');

/** Importing the Flat Controller to handle the routes */
const flatControler = require('../../controllers/flatcntrl');

/**=============================================
 * CREATE FLAT
 * =============================================
 * @route   GET /api/flats/create
 * @desc    create a flat
 * @access  Private
 */

expressRouter.post('/create', 
                    passport.authenticate('jwt', {session: false}), 
                    flatControler.createFlat
                  );


/**=============================================
 * EDIT FLAT
 * =============================================
 * @route   GET /api/flats/edit
 * @desc    edit an existing flat
 * @access  Private
 */

expressRouter.post('/edit', 
                    passport.authenticate('jwt', {session: false}), 
                    flatControler.editFlat
                  );

/**=============================================
 * GET FLAT LIST
 * =============================================
 * @route   GET /api/flats/all
 * @desc    provide flats list
 * @access  Private
 */

expressRouter.get('/all', 
passport.authenticate('jwt', {session: false}), 
                  flatControler.getAllFlats
                  );


/**=============================================
 * GET USER'S ANNOUNCEMENTS
 * =============================================
 * @route   GET /api/flats/my
 * @desc    provide the announcements that 
 *          the user has created
 * @access  Private
 */

expressRouter.get('/my', 
passport.authenticate('jwt', {session: false}), 
                  flatControler.getUsersFlats
                  );


/**=============================================
 * GET LIST OF LIKED FLATS
 * =============================================
 * @route   GET /api/flats/liked
 * @desc    provide flats list
 * @access  Private
 */

expressRouter.get('/liked', 
passport.authenticate('jwt', {session: false}), 
                  flatControler.getLikedFlats
                  );


/**=============================================
 * GET LIST OF BOOKMARKED FLATS
 * =============================================
 * @route   GET /api/flats/bookmarked
 * @desc    provide flats list
 * @access  Private
 */

expressRouter.get('/bookmarked', 
passport.authenticate('jwt', {session: false}), 
                  flatControler.getBookmarkedFlats
                  );


/**=============================================
 * GET FLAT OBJECT FOR EDIT
 * =============================================
 * @route   GET /api/flats/getForEdit
 * @desc    check whether the current user is
 *          the creatrir of the flat in question
 *          and if no, return an error, and if yes,
 *          return the flat data object
 * @access  Private
 */

expressRouter.get('/getForEdit', 
passport.authenticate('jwt', {session: false}), 
                  flatControler.getFlatForEdit
                  );



/**=============================================
 * GET LIST OF TRASHED FLATS
 * =============================================
 * @route   GET /api/flats/trashed
 * @desc    provide flats list
 * @access  Private
 */

expressRouter.get('/trashed', 
passport.authenticate('jwt', {session: false}), 
                  flatControler.getTrashedFlats
                  );

/**=============================================
 * ADD FLAT LIKE
 * =============================================
 * @route   POST /api/flats/like
 * @desc    add the userId to the flatLikes array
 * @access  Private
 */

expressRouter.post('/like', 
                    passport.authenticate('jwt', {session: false}), 
                    flatControler.likeFlat
                  );


/**=============================================
 * PUBLISH FLAT
 * =============================================
 * @route   POST /api/flats/publish
 * @desc    
 * @access  Private
 */

expressRouter.post('/publish', 
                    passport.authenticate('jwt', {session: false}), 
                    flatControler.publishFlat
                  );


/**=============================================
 * UNPUBLISH FLAT
 * =============================================
 * @route   POST /api/flats/unpublish
 * @desc    
 * @access  Private
 */

expressRouter.post('/unpublish', 
                    passport.authenticate('jwt', {session: false}), 
                    flatControler.unpublishFlat
                  );                  


/**=============================================
 * ADD FLAT VIEWER
 * =============================================
 * @route   POST /api/flats/viewed
 * @desc    add the userId to the flatViewed array
 * @access  Private
 */

expressRouter.post('/viewed', 
                    passport.authenticate('jwt', {session: false}), 
                    flatControler.viewedFlat
                  );

/**=============================================
 * REMOVE FLAT LIKE
 * =============================================
 * @route   POST /api/flats/unlike
 * @desc    remove the userId from the flatLikes array
 * @access  Private
 */

expressRouter.post('/unlike', 
                    passport.authenticate('jwt', {session: false}), 
                    flatControler.unlikeFlat
                  );                  


/**=============================================
 * ADD FLAT BOOKMARK
 * =============================================
 * @route   POST /api/flats/bookmark
 * @desc    add the userId to the flatBookmarks array
 * @access  Private
 */

expressRouter.post('/bookmark', 
                    passport.authenticate('jwt', {session: false}), 
                    flatControler.bookmarkFlat
                  );

/**=============================================
 * REMOVE FLAT BOOKMARK
 * =============================================
 * @route   POST /api/flats/unbookmark
 * @desc    remove the userId from the flatBookmarks array
 * @access  Private
 */

expressRouter.post('/unbookmark', 
                    passport.authenticate('jwt', {session: false}), 
                    flatControler.unbookmarkFlat
                  );     



/**=============================================
 * Trash a flat
 * =============================================
 * @route   POST /api/flats/trash
 * @desc    add the userId to the flatBookmarks array
 * @access  Private
 */

expressRouter.post('/trash', 
                    passport.authenticate('jwt', {session: false}), 
                    flatControler.trashFlat
                  );

/**=============================================
 * REMOVE FLAT BOOKMARK
 * =============================================
 * @route   POST /api/flats/untrash
 * @desc    remove the userId from the flatBookmarks array
 * @access  Private
 */

expressRouter.post('/untrash', 
                    passport.authenticate('jwt', {session: false}), 
                    flatControler.untrashFlat
                  );     


/**=============================================
 * ADD FLAT COMMENT
 * =============================================
 * @route   POST /api/flats/comment/add
 * @desc    add a commemt for an announcement
 * @access  Private
 */

expressRouter.post('/comment/add', 
                    passport.authenticate('jwt', {session: false}), 
                    flatControler.addComment
                  );

/**=============================================
 * DELETE FLAT COMMENT
 * =============================================
 * @route   DELETE /api/flats/comment/delete
 * @desc    delete a comment for an announcement
 * @access  Private
 */

expressRouter.delete('/comment/delete', 
                    passport.authenticate('jwt', {session: false}), 
                    flatControler.deleteComment
                  );
    
        
/**=============================================
 * DELETE FLAT !!!!!!!!!!!!!!!
 * =============================================
 * @route   GET /api/flats/delete
 * @desc    provide the announcements that 
 *          the user has created
 * @access  Private
 */

expressRouter.delete('/delete', 
passport.authenticate('jwt', {session: false}), 
                  flatControler.deleteFlat
                  );                  

module.exports = expressRouter;