/**
 * File: src/ReduxStore/configureStore.js
 * Purpose: Here we define a fct that will be exported and that configures
 * the global redux store.
 */

 /* importing the necessary fcts and libs */
 import { createStore, applyMiddleware, compose } from 'redux';
 import thunkMiddleware from 'redux-thunk';
 //import logger from 'redux-logger';
 
 /* 1. importing the root Reducer that combines together all the other reducers */
 import rootReducer from './reducers/rootReducer';
 
 /* 2. Define the function that we're going to use in index.js to
 create the global redux store */
 const configureStore = initialState => {
 
   /* 3. Here, we will add future middlewares */
   const middlewares = [thunkMiddleware, /*logger*/];
 
   /* 4. We define an enhancer for our store and that enhancer applies
   the middlewares to the store */
   const middlewareEnhancer = applyMiddleware(...middlewares);
 
   /* 5. The Store enhancer takes over the middleWare enhancer */
   const storeEnhancer = [middlewareEnhancer];
 
   /* 6. Because a store can have more than one enhancer, we need to 
   compose them. We use the redux devtools extension for chrome
   in dev environment and compose in prod env. */
   const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
   const composedEnhancer = composeEnhancers(...storeEnhancer);
  
   /* 7. Now we finally create the store by bringing all the stuff together */
   const theStore = createStore( rootReducer, initialState, composedEnhancer);
 
   /* Let's have the configureTheStore fct return theStore */
   return theStore;
 
 }//configureTheStore
 
 export default configureStore;