import axios from 'axios';

//Action: Get all flats
export const getFlatsFromDB = (viewType = '/home') => dispatch => {

  switch(viewType){

    case '/home':
      axios.get('/api/flats/all')
      .then(  result => dispatch({ type: 'GET_ALL_FLATS', payload: result.data })  )
      .catch( error  => console.log(error) );
      break

      case '/my':
      axios.get('/api/flats/my')
      .then(  result => dispatch({ type: 'GET_ALL_FLATS', payload: result.data })  )
      .catch( error  => console.log(error) );
      break

      case '/liked':
      axios.get('/api/flats/liked')
      .then(  result => dispatch({ type: 'GET_ALL_FLATS', payload: result.data })  )
      .catch( error  => console.log(error) );
      break

      case '/bookmarked':
      axios.get('/api/flats/bookmarked')
      .then(  result => dispatch({ type: 'GET_ALL_FLATS', payload: result.data })  )
      .catch( error  => console.log(error) );
      break

      case '/trashed':
      axios.get('/api/flats/trashed')
      .then(  result => dispatch({ type: 'GET_ALL_FLATS', payload: result.data })  )
      .catch( error  => console.log(error) );
      break      

      default:
      axios.get('/api/flats/all')
      .then(  result => dispatch({ type: 'GET_ALL_FLATS', payload: result.data })  )
      .catch( error  => console.log(error) );
      break

  };//switch
};//getFlatsFromDB

export const setDefaultFlat = () => dispatch => {
  dispatch({ type: 'SET_DEFAULT_FLAT' })
}

export const setFlatToEdit = flatToEdit => dispatch => {
  dispatch({ type: 'SET_FLAT_TO_EDIT', flatToEdit })
}

export const setCountry = country => dispatch => {
  dispatch({ type: 'SET_COUNTRY', country })
}

export const setLatLng = latlng => dispatch => {
  dispatch({ type: 'SET_LATLNG', latlng })
}

export const setAddressDetails = address_details => dispatch => {
  dispatch({ type: 'SET_ADDRESS_DETAILS', address_details })
}

export const setPublishStreetNumber = publishStreetNumber => dispatch => {
  dispatch({ type: 'SET_PUBLISH_STREETNUMBER', publishStreetNumber })
}

// ************* Page 2 *************

export const setTypeOfProperty = typeOfProperty => dispatch => {
  dispatch({ type: 'SET_TYPE_OF_PROPERTY', typeOfProperty })
}

export const setAvailableFromDate = availableFromDate => dispatch => {
  dispatch({ type: 'SET_AVAILABLE_FROM_DATE', availableFromDate })
}

export const setEpcRatingType = epcRatingType => dispatch => {
  dispatch({ type: 'SET_EPC_RATING_TYPE', epcRatingType })
}

export const setSolarOrientation = solarOrientation => dispatch => {
  dispatch({ type: 'SET_SOLAR_ORIENTATION', solarOrientation })
}

export const setFurnishingType = furnishingType => dispatch => {
  dispatch({ type: 'SET_FURNISHING_TYPE', furnishingType })
}

export const setNumberOfRooms = numberOfRooms => dispatch => {
  dispatch({ type: 'SET_NUMBER_OF_ROOMS', numberOfRooms })
}

export const setNumberOfBathRooms = numberOfBathRooms => dispatch => {
  dispatch({ type: 'SET_NUMBER_OF_BATHROOMS', numberOfBathRooms })
}

export const setSurfaceTotal = surfaceTotal => dispatch => {
  dispatch({ type: 'SET_SURFACE_TOTAL', surfaceTotal })
}

export const setSurfaceNet = surfaceNet => dispatch => {
  dispatch({ type: 'SET_SURFACE_NET', surfaceNet })
}

export const setFloor = floor => dispatch => {
  dispatch({ type: 'SET_FLOOR', floor })
}

export const setFloorTotal = floorTotal => dispatch => {
  dispatch({ type: 'SET_FLOOR_TOTAL', floorTotal })
}

export const setConstructionYearKnown = constructionYearKnown => dispatch => {
  dispatch({ type: 'SET_CONSTRUCTION_YEAR_KNOWN', constructionYearKnown })
}

export const setRenovationYearKnown = renovationYearKnown => dispatch => {
  dispatch({ type: 'SET_RENOVATION_YEAR_KNOWN', renovationYearKnown })
}

export const setConstructionYear = constructionYear => dispatch => {
  dispatch({ type: 'SET_CONSTRUCTION_YEAR', constructionYear })
}

export const setRenovationYear = renovationYear => dispatch => {
  dispatch({ type: 'SET_RENOVATION_YEAR', renovationYear })
}

// ************* Page 3 *************

export const setRent = rent => dispatch => {
  dispatch({ type: 'SET_RENT', rent })
}

export const setCurrency = currency => dispatch => {
  dispatch({ type: 'SET_CURRENCY', currency })
}

export const setPaymentPeriod = paymentPeriod => dispatch => {
  dispatch({ type: 'SET_PAYMENT_PERIOD', paymentPeriod })
}

export const setDeposit = deposit => dispatch => {
  dispatch({ type: 'SET_DEPOSIT', deposit })
}

export const setHasAdditionalCosts = hasAdditionalCosts => dispatch => {
  dispatch({ type: 'SET_HAS_ADDITIONAL_COSTS', hasAdditionalCosts })
}

export const setAdditionalCostsAmount = additionalCostsAmount => dispatch => {
  dispatch({ type: 'SET_ADDITIONAL_COSTS_AMOUNT', additionalCostsAmount })
}

export const setAdditionalCostsPaymentMethod = additionalCostsPaymentMethod => dispatch => {
  dispatch({ type: 'SET_ADDITIONAL_COSTS_PAYMENT_METHOD', additionalCostsPaymentMethod })
}

// ************* Page 4 *************

export const setHasAirConditioning = hasAirCon => dispatch => {
  dispatch({ type: 'SET_HAS_AIR_CONDITIONING', hasAirCon })
}

export const setHasBalcony = hasBalcony => dispatch => {
  dispatch({ type: 'SET_HAS_BALCONY', hasBalcony })
}

export const setHasBasement = hasBasement => dispatch => {
  dispatch({ type: 'SET_HAS_BASEMENT', hasBasement })
}

export const setHasBuiltInClosets = hasBuiltInClosets => dispatch => {
  dispatch({ type: 'SET_HAS_BUILT_IN_CLOSETS', hasBuiltInClosets })
}

export const setHasDishwasher = hasDishwasher => dispatch => {
  dispatch({ type: 'SET_HAS_DISHWASHER', hasDishwasher })
}

export const setHasElevator = hasElevator => dispatch => {
  dispatch({ type: 'SET_HAS_ELEVATOR', hasElevator })
}

export const setHasFridge = hasFridge => dispatch => {
  dispatch({ type: 'SET_HAS_FRIDGE', hasFridge })
}

export const setHasGarage = hasGarage => dispatch => {
  dispatch({ type: 'SET_HAS_GARAGE', hasGarage })
}

export const setHasGarden = hasGarden => dispatch => {
  dispatch({ type: 'SET_HAS_GARDEN', hasGarden })
}

export const setHasHeating = hasHeating => dispatch => {
  dispatch({ type: 'SET_HAS_HEATING', hasHeating })
}

export const setHasMicrowave = hasMicrowave => dispatch => {
  dispatch({ type: 'SET_HAS_MICROWAVE', hasMicrowave })
}

export const setHasParquetFlooring = hasParquetFlooring => dispatch => {
  dispatch({ type: 'SET_HAS_PARQUET_FLOORING', hasParquetFlooring })
}


export const setHasPlayground = hasPlayground => dispatch => {
  dispatch({ type: 'SET_HAS_PLAYGROUND', hasPlayground })
}

export const setHasPool = hasPool => dispatch => {
  dispatch({ type: 'SET_HAS_POOL', hasPool })
}

export const setHasReinforcedDoor = hasReinforcedDoor => dispatch => {
  dispatch({ type: 'SET_HAS_REINFORCED_DOOR', hasReinforcedDoor })
}

export const setHasStove = hasStove => dispatch => {
  dispatch({ type: 'SET_HAS_STOVE', hasStove })
}

export const setHasTV = hasTV => dispatch => {
  dispatch({ type: 'SET_HAS_TV', hasTV })
}

export const setHasWashingMachine = hasWashingMachine => dispatch => {
  dispatch({ type: 'SET_HAS_WASHING_MACHINE', hasWashingMachine })
}

export const setIsForPets = isForPets => dispatch => {
  dispatch({ type: 'SET_IS_FOR_PETS', isForPets })
}

export const setIsForSmokers= isForSmokers => dispatch => {
  dispatch({ type: 'SET_IS_FOR_SMOKERS', isForSmokers })
}

export const setIsForStudents= isForStudents => dispatch => {
  dispatch({ type: 'SET_IS_FOR_STUDENTS', isForStudents })
}

// ************* Page 5 *************

export const setHeadline = headline => dispatch => {
  dispatch({ type: 'SET_HEADLINE', headline })
}

export const setDescrShort= descr_short => dispatch => {
  dispatch({ type: 'SET_DESCR_SHORT', descr_short })
}

export const setDescrLong= descr_long => dispatch => {
  dispatch({ type: 'SET_DESCR_LONG', descr_long })
}

// ************* Page 6 *************

export const setFlatImages = flatImages => dispatch => {
  dispatch({ type: 'SET_FLAT_IMAGES', flatImages })
}
