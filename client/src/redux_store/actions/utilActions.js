import axios from 'axios';

export const setAuthToken = token => {

  if(token){
    //if it is passed to this fct, apply token to every request
    axios.defaults.headers.common['Authorization'] = token;  
  } else {
    //if no token is passed in, then delete the Auth header
    delete axios.defaults.headers.common['Authorization'];
  }
};//setAuthToken

export const getCountries = () => dispatch => {

  axios.get('api/utils/countries')
  .then(  result => dispatch({ type: 'GET_ALL_COUNTRIES', payload: result.data }) )
  //.catch( error  => console.log(error));  

}
