import axios from 'axios';
import jwt_decode from 'jwt-decode';
import { setAuthToken } from './utilActions';

//Action: Sign Up a User
export const signUpUser = (userData, history) => dispatch => {

  axios.post('/api/users/signup', userData)
  .then(  result => history.push('/login') )
  .catch( error  => dispatch({
    type: 'GET_ERRORS',
    payload: error.response.data
  }));

};//signInUser

export const setCurrentUser = decodedToken => {
  //dispatch the user to our store
  return {
    type: 'SET_CURRENT_USER',
    payload: decodedToken
  }

};//setCurrentUser;


export const setUserAvatar = avatar => {
  //dispatch the user to our store
  return {
    type: 'SET_USER_AVATAR',
    avatar
  }

};//setUserAvatar;

//Action: Sign In a User and get token
export const signInUser = userData => dispatch => {

  axios.post('/api/users/signin', userData)
  .then(  result => {
    //Destructure token out of the result data
    const { token } = result.data;

    //set token to localstorage
    //localStorage stores only strings but we do not have
    //to convert it to JSON because the token is just a string
    localStorage.setItem('jwtToken', token);

    //Set the token to the auth header:
    setAuthToken(token);

    //decode token to get user data
    const decodedToken = jwt_decode(token);

    //Set current user
    dispatch(setCurrentUser(decodedToken));
  })
  .catch( error  => dispatch({
    type: 'GET_ERRORS',
    payload: error.response.data
  }));

};//signInUser


//Action: Sign Out a User and destroy token
export const signOutUser = () => dispatch => {
  // Remove the token from localStorage
  localStorage.removeItem('jwtToken');

  // Remove auth header for future axios requests
  setAuthToken(false);

  //Set current user to an empty object (this will also set isAuthenticated to false)
  dispatch( setCurrentUser({}) );

  dispatch({type: 'DESTROY_REDUX_STATE'});
  
};//signOutUser