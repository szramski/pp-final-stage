import isEmpty from 'lodash.isempty';

const initialState = {
  isAuthenticated: false,
  user: {}
};

/** The default export of this reducer is a function that takes
 * in a state variable that will have the initialState as its default
 * and that also takes in an action. The action will be an object that
 * will contain a type property
 */
export default function(state = initialState, action){

  /** testing the action type */
  switch(action.type){

    case 'SET_CURRENT_USER': 
    /*we dispatch to the store the previous state (...state)
      and we add for the value of isAuthenticated the following value:
      if action.payload is not empty, it will be true
      else it will be false.
      To the use property we assign the value of the payload itself*/
      return {
        ...state,
        isAuthenticated: !isEmpty(action.payload),
        user: action.payload
      }

    case 'SET_USER_AVATAR':
    console.log("Set avatar: ", action.avatar)
      return {
        ...state, user: {
          ...state.user, avatar: action.avatar
        }
      }

    default:
      return state;
  }//switch

}//export default function