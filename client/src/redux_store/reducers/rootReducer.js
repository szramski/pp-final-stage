import { combineReducers } from 'redux';

import flatReducer from './flatReducer';
import authReducer from './authReducer';
import errorReducer from './errorReducer';
import flatCreatorReducer from './flatCreatorReducer';
import { reducer as reduxFormCreateFlat } from 'redux-form';

const appReducer = combineReducers({
  auth: authReducer,
  flat: flatReducer,
  flatToCreate: flatCreatorReducer,
  form: reduxFormCreateFlat,
  error: errorReducer
});

//See: https://stackoverflow.com/questions/35622588/how-to-reset-the-state-of-a-redux-store/35641992#35641992

const rootReducer = (state, action) => {

  if (action.type === 'DESTROY_REDUX_STATE') {
    state = undefined
  }

  return appReducer(state, action)
}

export default rootReducer;