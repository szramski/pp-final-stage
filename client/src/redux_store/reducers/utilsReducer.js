import { GET_ALL_COUNTRIES } from '../actions/action_types';

const initialState = {};

export default function(state = initialState, action){

  /** testing the action type */
  switch(action.type){

    case GET_ALL_COUNTRIES: return action.payload;
    
    default:
      return state;
  }//switch

}//export default function