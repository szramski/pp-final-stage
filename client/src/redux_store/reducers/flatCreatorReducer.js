import moment from 'moment';

const defaultFlat = {
  publishAnnouncement: false,
  typeOfProperty: 'n/a',
  furnishingType: 'n/a',
  epcRatingType: 'n/a',
  solarOrientation: 'n/a',
  availableFromDate: moment().format('YYYY-MM-DD'),
  createdOnDate: moment().format('YYYY-MM-DD'),
  publishedOnDate: moment().format('YYYY-MM-DD'),      
  country: 'n/a',
  address_details: {
      streetNumber: 0,
      streetName: '',
      city: '',
      postalcode: 0,
      region: '',
      country: 'n/a',
      country_short: '',
      formattedAddress: ''
  },
  latlng: {
      lat: 0,
      lng: 0
  },
  constructionYearKnown: false,
  hasAdditionalCosts: false,

  hasAirCon: false,
  hasBalcony: false,
  hasBasement: false,
  hasBuiltInClosets: false,
  hasDishwasher: false,
  hasElevator: false,
  hasFridge: false,
  hasGarage: false,
  hasGarden: false,
  hasHeating: false,
  hasMicrowave: false,
  hasParquetFlooring: false,
  hasPlayground: false,
  hasPool: false,
  hasReinforcedDoor: false,
  hasStove: false,
  hasTV: false,
  hasWashingMachine: false,
  isForPets: false,
  isForSmokers: false,
  isForStudents: false,
  
  publishStreetNumber: false,
  renovationYearKnown: false,
  numberOfRooms: 1,
  numberOfBathRooms: 1,
  surfaceTotal: 40,
  surfaceNet: 35,
  floor: 0,
  floorTotal: 4,
  rent: 400,
  currency: 'n/a',
  paymentPeriod: 'n/a',
  deposit: 0,
  additionalCostsAmount: 100,
  additionalCostsPaymentMethod: 'n/a',
  constructionYear: 2000,
  renovationYear: 2000,
  flatImages: [],
  flatLikes: [],
  flatBookmarks: [],
  flatTrashed: [],
  flatComments: [],
  flatViewed: [],
  headline: '',
  descr_short: '',
  descr_long: '',
};//defaultObject

const initialState = defaultFlat;

const flatCreatorReducer = ( state = initialState, action ) => {

  switch(action.type){

    case 'SET_DEFAULT_FLAT': 
      defaultFlat.flatImages = [];
      return defaultFlat;

    case 'SET_FLAT_TO_EDIT': 
      return action.flatToEdit;      

    case 'SET_COUNTRY': return { ...state, country: action.country }

    case 'SET_LATLNG': return { ...state, latlng: action.latlng }

    case 'SET_ADDRESS_DETAILS': 
      return { ...state, address_details: action.address_details }

    case 'SET_PUBLISH_STREETNUMBER': 
      return { ...state, publishStreetNumber: action.publishStreetNumber }

    case 'SET_TYPE_OF_PROPERTY':
      return { ...state, typeOfProperty: action.typeOfProperty }

    case 'SET_AVAILABLE_FROM_DATE':
      return { ...state, availableFromDate: action.availableFromDate }

    case 'SET_EPC_RATING_TYPE':
      return { ...state, epcRatingType: action.epcRatingType }

    case 'SET_SOLAR_ORIENTATION':
      return { ...state, solarOrientation: action.solarOrientation }

    case 'SET_FURNISHING_TYPE':
      return { ...state, furnishingType: action.furnishingType }

    case 'SET_NUMBER_OF_ROOMS':
      return { ...state, numberOfRooms: action.numberOfRooms }    

    case 'SET_NUMBER_OF_BATHROOMS':
      return { ...state, numberOfBathRooms: action.numberOfBathRooms }   

    case 'SET_SURFACE_TOTAL':
      return { ...state, surfaceTotal: action.surfaceTotal }  

    case 'SET_SURFACE_NET':
      return { ...state, surfaceNet: action.surfaceNet }   

    case 'SET_FLOOR':
      return { ...state, floor: action.floor }  

    case 'SET_FLOOR_TOTAL':
      return { ...state, floorTotal: action.floorTotal }   

    case 'SET_CONSTRUCTION_YEAR_KNOWN':
      return { ...state, constructionYearKnown: action.constructionYearKnown }   

    case 'SET_RENOVATION_YEAR_KNOWN':
      return { ...state, renovationYearKnown: action.renovationYearKnown }   
     
    case 'SET_CONSTRUCTION_YEAR':
      return { ...state, constructionYear: action.constructionYear }   

    case 'SET_RENOVATION_YEAR':
      return { ...state, renovationYear: action.renovationYear }         

    case 'SET_RENT': return { ...state, rent: action.rent }  
    
    case 'SET_CURRENCY': return { ...state, currency: action.currency }   

    case 'SET_PAYMENT_PERIOD': return { ...state, paymentPeriod: action.paymentPeriod }

    case 'SET_DEPOSIT': return { ...state, deposit: action.deposit }

    case 'SET_HAS_ADDITIONAL_COSTS': 
      return { ...state, hasAdditionalCosts: action.hasAdditionalCosts }

    case 'SET_ADDITIONAL_COSTS_AMOUNT': 
      return { ...state, additionalCostsAmount: action.additionalCostsAmount }

    case 'SET_ADDITIONAL_COSTS_PAYMENT_METHOD': 
      return { ...state, additionalCostsPaymentMethod: action.additionalCostsPaymentMethod }

    case 'SET_HAS_AIR_CONDITIONING':  
      return { ...state, hasAirCon: action.hasAirCon }

    case 'SET_HAS_BALCONY':
      return { ...state, hasBalcony: action.hasBalcony }

    case 'SET_HAS_BASEMENT':
      return { ...state, hasBasement: action.hasBasement }
    
    case 'SET_HAS_BUILT_IN_CLOSETS':
      return { ...state, hasBuiltInClosets: action.hasBuiltInClosets }

    case 'SET_HAS_DISHWASHER':
      return { ...state, hasDishwasher: action.hasDishwasher }

    case 'SET_HAS_ELEVATOR':
      return { ...state, hasElevator: action.hasElevator }

    case 'SET_HAS_FRIDGE':
      return { ...state, hasFridge: action.hasFridge }

    case 'SET_HAS_GARAGE':
      return { ...state, hasGarage: action.hasGarage }

    case 'SET_HAS_GARDEN':
      return { ...state, hasGarden: action.hasGarden }

    case 'SET_HAS_HEATING':
      return { ...state, hasHeating: action.hasHeating }

    case 'SET_HAS_MICROWAVE':
      return { ...state, hasMicrowave: action.hasMicrowave }

    case 'SET_HAS_PARQUET_FLOORING':
      return { ...state, hasParquetFlooring: action.hasParquetFlooring }

    case 'SET_HAS_PLAYGROUND':
      return { ...state, hasPlayground: action.hasPlayground }

    case 'SET_HAS_POOL':
      return { ...state, hasPool: action.hasPool }

    case 'SET_HAS_REINFORCED_DOOR':
      return { ...state, hasReinforcedDoor: action.hasReinforcedDoor }

    case 'SET_HAS_STOVE':
      return { ...state, hasStove: action.hasStove }

    case 'SET_HAS_TV':
      return { ...state, hasTV: action.hasTV }

    case 'SET_HAS_WASHING_MACHINE':
      return { ...state, hasWashingMachine: action.hasWashingMachine }

    case 'SET_IS_FOR_PETS':
      return { ...state, isForPets: action.isForPets }

    case 'SET_IS_FOR_SMOKERS':
      return { ...state, isForSmokers: action.isForSmokers }

    case 'SET_IS_FOR_STUDENTS':
      return { ...state, isForStudents: action.isForStudents }

    case 'SET_HEADLINE':
      return { ...state, headline: action.headline }

    case 'SET_DESCR_SHORT':
      return { ...state, descr_short: action.descr_short }

    case 'SET_DESCR_LONG':
      return { ...state, descr_long: action.descr_long }

    case 'SET_FLAT_IMAGES':
  
      return { ...state, flatImages: action.flatImages }      
      
    default:
    return state;

  }//switch

}//flatCreatorReducer

export default flatCreatorReducer;