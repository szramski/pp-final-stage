const flatsInitialState = {
  flatsarray: undefined,
};

const flatReducer = ( state = flatsInitialState, action ) => {

  switch(action.type){

    case 'GET_ALL_FLATS': return {
      ...state, 
      flatsarray: action.payload
    };
  
    default:
    return state;

  }

}

export default flatReducer;