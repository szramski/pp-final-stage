import React from 'react';
import { render } from 'react-dom';

import { BrowserRouter } from 'react-router-dom';
import ClientRoutes from './components/ClientRoutes/ClientRoutes';

import { Provider } from 'react-redux';
import configureStore from './redux_store/configureStore'

import 'bootstrap/dist/css/bootstrap.min.css';
import 'roboto-fontface/css/roboto/roboto-fontface.css';
import './index.css';

import jwt_decode from 'jwt-decode';
import { setCurrentUser, signOutUser } from './redux_store/actions/authActions';
import { setAuthToken } from './redux_store/actions/utilActions';

const Store = configureStore();

/*************************/
/* ======= AUTH =======  */
/*************************/

/* We wanna check if there is a user token in the localstorage
and if there is and if it hasn't expired, we will store cor-
responding entries in the store */

/** Check if a user token exists in local storage */
if (localStorage.jwtToken){
  //set the token as Authorization value for each axios request!
  setAuthToken(localStorage.jwtToken);

  //decode token to get user data
  const decodedToken = jwt_decode(localStorage.jwtToken);

  //Set current user
  Store.dispatch( setCurrentUser(decodedToken) );  

  /** If the current time is bigger than the expiration
   * time of the token, then we log out the User
   */
  const currentTime = Date.now() /1000 ; //miliseconds

  if(decodedToken.exp < currentTime){

    Store.dispatch( signOutUser() );

    //redirect the user to the landing page for login
    window.location.href='/';
  }
}



render(
  <Provider store={Store}>
    <BrowserRouter>
      <ClientRoutes/>
    </BrowserRouter>
  </Provider>, 
document.getElementById('root'));