export default {

  HERO_SLOGAN: 'Make yourself at home.',
  HERO_SIGNUP: 'Register',
  HERO_SIGNIN: 'Login',

  LGN_HDR: 'Login',
  LGN_PLCHLDR_EMAIL: 'Email Address',
  LGN_PLCHLDR_PWD: 'Password',
  LGN_BTN_GOTO_SIGNUP: 'Need An Account?',
  LGN_BTN_DO_SIGNIN: 'Login',

  PRFL_HDR: 'Profile Update',
  PRFL_BTN_DO_UPDATE: 'Save changes',

  RGSTR_HDR: 'Registration',
  RGSTR_PLCHLDR_FN: 'First Name',
  RGSTR_PLCHLDR_LN: 'Last Name',
  RGSTR_PLCHLDR_EMAIL: 'Email',
  RGSTR_PLCHLDR_PWD: 'Password',
  RGSTR_PLCHLDR_CPWD: 'Confirm Password',
  RGSTR_BTN_GOTO_SIGNIN: 'Have an account?',
  RGSTR_BTN_DO_SIGNUP: 'Register',

  FLAT_DESCR_HEADLINE_PLCHLDR: 'Headline. Example: Sunny flat in Málaga',
  FLAT_DESCR_HEADLINE_HINT: 'Please enter a short slogan that describes your announcement.',
  FLAT_DESCR_SHORT_PLCHLDR: 'Short description of the flat.',
  FLAT_DESCR_SHORT_HINT: 'This text will be displayed below the pictures and always visible to the user.',
  FLAT_DESCR_LONG_PLCHLDR: 'Long description of the flat.',
  FLAT_DESCR_LONG_HINT: 'This text will be displayed if the user wishes to read it. Please provide further details about your real estate but please avoid to write facts. You can provide facts in the next tab.',

  CRT_FLT_LCT_PLCATOCMPL_PLCHLDR: 'E.g. Castellana 1, Madrid',
  CRT_FLT_LCT_PLCATOCMPL_HINT: 'For correct search results, please provide the address in the local language and include the street number and postal code. Example: Bahnhofstr 1, 33602 Bielefeld, Deutschland. Please make sure to have selected the corresponding country, first.',

}