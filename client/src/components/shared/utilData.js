export const sideBarItemsArray = [
  { faIcon: 'home',         text: 'Home',       linkTo: '/home' },
  { faIcon: 'envelope',     text: 'Messages',   linkTo: '/messages' },
  { faIcon: 'plus-square',  text: 'New Announcement',   linkTo: '/create' }, 
  { faIcon: 'bullhorn',     text: 'My Announcements',   linkTo: '/my' },     
  { faIcon: 'user',         text: 'Profile',    linkTo: '/profile' },
  { faIcon: 'star',         text: 'Bookmarked',  linkTo: '/bookmarked' },
  { faIcon: 'thumbs-up',    text: 'Liked',      linkTo: '/liked' },  
  { faIcon: 'trash',        text: 'Irrelevant',      linkTo: '/trashed' },
  { faIcon: 'cog',          text: 'Search Settings',   linkTo: '/settings' },
];

export const supportedCountries = [
  {
  translation: {
    de: "Deutschland",
    en: "Germany",
    pl: "Niemcy",
    es: "Alemania"
  },
    shortcode: "de"
  },


  {
  translation: {
    de: "Spanien",
    en: "Spain",
    pl: "Poland",
    es: "España"
  },
    shortcode: "es"
  },


  {
  translation: {
    de: "Polen",
    en: "Poland",
    pl: "Polska",
    es: "Polonia"
  },
    shortcode: "pl"
  },

  {
    translation: {
      de: "England",
      en: "United Kingdom",
      pl: "Anglia",
      es: "Inglaterra"
    },
      shortcode: "uk"
  }  
  
  ]