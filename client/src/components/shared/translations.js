module.exports = {

depositTypes: {
'en': [
    { value: 0, translation: 'No' },
    { value: 1, translation: '1 monthly rate' }, 
    { value: 2, translation: '2 monthly rates' },
    { value: 3, translation: '3 monthly rates' },
    { value: 4, translation: '4 monthly rates' },
    { value: 5, translation: '5 monthly rates' },
    { value: 6, translation: '6 monthly rates' },
    { value: 7, translation: 'More than 6 monthly rates' },
  ],

'de': [
    { value: 0, translation: 'Nein' },
    { value: 1, translation: '1 Monatsrate' },
    { value: 2, translation: '2 Monatsraten' },
    { value: 3, translation: '3 Monatsraten' },
    { value: 4, translation: '4 Monatsraten' },
    { value: 5, translation: '5 Monatsraten' },
    { value: 6, translation: '6 Monatsraten' },
    { value: 7, translation: 'Mehr als 6 Monatsraten' },
  ], 

'es': [
    { value: 0, translation: 'No' },
    { value: 1, translation: '1 mes' }, 
    { value: 2, translation: '2 meses' },
    { value: 3, translation: '3 meses' },
    { value: 4, translation: '4 meses' },
    { value: 5, translation: '5 meses' },
    { value: 6, translation: '6 meses' },
    { value: 7, translation: 'Más de 6 meses' },
  ],  

'pl': [
    { value: 0, translation: 'Nie' },
    { value: 1, translation: '1 miesiąc' },
    { value: 2, translation: '2 miesiące' },
    { value: 3, translation: '3 miesiące' },
    { value: 4, translation: '4 miesiące' },
    { value: 5, translation: '5 miesięcy' },
    { value: 6, translation: '6 miesięcy' },
    { value: 7, translation: 'Więcej niż 6 miesięcy' },
  ],   
  
}, //depositTypes
  
}//module.exports  









/*

  hasAdditionalCosts,  
    additionalCostsAmount,
    additionalCostsPaymentMethod
  
  publishStreetNumber,
  
    constructionYearKnown,
  renovationYearKnown,
    constructionYear,
  renovationYear,

  creatorUserID,
  
  floorTotal,

  epcRatingType,
  solarOrientation,
  furnishingType,
  
  */