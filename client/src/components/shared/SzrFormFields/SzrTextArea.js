import React from 'react';
import { FormGroup, Label} from "reactstrap";
import classnames from 'classnames';

const SzrTextArea = (
  {label, placeholder, hint, value, onChange, validationError}
) => {

  const showErrorStyles = !!validationError;

  return (
  <FormGroup className="p-1 border shadow-sm">
    { label && <Label className="pr-0 py-1 pl-2 m-0">{label}</Label> }
    <textarea
      placeholder={placeholder ? placeholder : ''}
      value={value}
      onChange={onChange}
      className={ classnames('form-control py-0 px-2 ', {'is-invalid': showErrorStyles })}
    />
    { hint && <small className="p-0 pl-2 m-0 mt-2 form-text text-muted">{hint}</small> }
    { validationError && <div className="invalid-feedback p-0 pl-2">{validationError}</div> }
  </FormGroup>
  )//return
}//FormField

export default SzrTextArea;
