import React from 'react';
import { FormGroup, Label} from "reactstrap";
import NumericInput from 'react-numeric-input';
import classnames from 'classnames';

const SzrNumField = (
  { label, hint, value, min, max, onChange, disabled, validationError}
) => {

  const showErrorStyles = !!validationError;

  return (
    <FormGroup className="p-1 border shadow-sm">
      { label && <Label className="p-0 py-1 pl-2 m-0">{label}</Label> }

      <NumericInput 
        min={min} 
        max={max} 
        value={value}
        onChange={onChange}
        className={ classnames('form-control py-0 px-2', {'is-invalid': showErrorStyles })}
        disabled={disabled}
        autoComplete="off"
        mobile
        style={{
          wrap: {
            width:'100%',
            height: '40px',
              //boxShadow: '0 0 1px 1px #fff inset, 1px 1px 5px -1px #000',
              
              borderRadius: '6px 3px 3px 6px',
              fontSize: 32
          },
          input: {
            width: '100%',
            height: '100%',
            paddingRight: '30px',
              borderRadius: '4px 2px 2px 4px',
              color: '#000',
              border: '1px solid #ccc',
              display: 'block',
              fontWeight: 100,
          },
          'input:focus' : {
              border: '1px inset #B0BEC5',
              cursor: 'none'
          },
          'btnDown.mobile': {
            width: '50px',
            background: '#B0BEC5',
              cursor: 'pointer'
          },
          'btnUp.mobile': {
            width: '50px',
            background: '#B0BEC5',                
              cursor: 'pointer'
          }
      }}/>
    { hint && <small className="p-0 pl-2 m-0 form-text text-muted">{hint}</small> }
    { 
      //There's a problem with the bootstrap invalid-feedback class so that I
      //must use a workaround...
      validationError && <div 
                          className="p-0 pl-2"
                          style={{
                            color: '#b71c1c',
                            marginTop: '.25rem',
                            fontSize: '80%'
                          }}
                          >{validationError}</div> 
    }
  </FormGroup>
  )//return
}// const SzrNumField

export default SzrNumField;
