import React from 'react';
import { FormGroup } from "reactstrap";
import Toggle from 'react-toggle'

import './SzrToggle.css';

const SzrToggle = (
  { name, label, hint, value, onChange, touched, validationError, reduxFormError}
) => {

  //const showErrorStyles = !!validationError || !!reduxFormError;
  return (
    <FormGroup className="p-0 my-1 pt-1 shadow-sm" style={{ border: '1px solid #d6dbdf'}} >
    <label className="p-0 m-0" style={{ display: 'table', cursor: 'pointer'}}>
      <Toggle
        name={name}
        checked={value}
        onChange={onChange} 
        icons={false}
        className="mr-3 ml-2 mt-1 mb-0"
      />
        { label && <span style={{display: 'table-cell', verticalAlign:'middle'}}>{label}</span> }
    </label>
    { hint && <small className="p-0 pl-2 m-0 form-text text-muted">{hint}</small> }
    { validationError && <div className="invalid-feedback p-0 pl-2">{validationError}</div> }
    { touched && reduxFormError && <div className="invalid-feedback p-0 pl-2">{reduxFormError}</div> }
  </FormGroup>
  )//return
}// const SzrToggle

export default SzrToggle;
