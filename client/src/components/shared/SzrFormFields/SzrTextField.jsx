import React from 'react';
import { FormGroup, Label} from "reactstrap";
import classnames from 'classnames';

const SzrTextField = (
  {type, name, label, placeholder, hint, value, onChange, validationError, }
) => {

  const showErrorStyles = !!validationError;

  return (
  <FormGroup className="p-1 border shadow-sm">
    { label && <Label className="pr-0 py-1 pl-2 m-0">{label}</Label> }
    <input
      type={type}
      name={name}
      placeholder={placeholder ? placeholder : ''}
      value={value}
      onChange={onChange}
      autoComplete="off"
      className={ classnames('form-control py-0 px-2', {'is-invalid': showErrorStyles })}
    />
    { hint && <small className="p-0 pl-2 m-0 mt-2 form-text text-muted">{hint}</small> }
    { validationError && <div className="invalid-feedback p-0 pl-2">{validationError}</div> }
  </FormGroup>
  )//return
}//FormField

export default SzrTextField 

/*
We assign the classnames function to the className property of the first input (firstname):
The first argument specifies classes that always are being displayed (default classes).
The second argument is an object that specifies a class that will only be applied when the condition applies, i.e. here
the 'is-invalid' class will be applied only if the validationErrors object contains the firstname property.

Below the input, we want to show a div with the bootstrap class of invalid-feedback
but only if there is an error (i.e. if validationErrors.firstname is not undefined)
*/