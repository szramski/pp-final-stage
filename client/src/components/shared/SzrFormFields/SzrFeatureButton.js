import React from 'react';
import { FormGroup} from "reactstrap";
import classnames from 'classnames';

export default ({label, toggleValue, showOutline}) => (
    <FormGroup className="">    
      <button 
        type="button" 
        className={ 
          classnames('btn btn-block', 
          {'btn-success': !showOutline } , {'btn-outline-secondary': showOutline }
        )} 
        onClick={toggleValue}
      >
        {label}
      </button>
    </FormGroup>
)