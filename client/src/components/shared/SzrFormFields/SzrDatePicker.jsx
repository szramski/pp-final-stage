import React from 'react';
import { FormGroup, Label } from "reactstrap";

import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'moment/locale/de';
import 'moment/locale/pl';
import 'moment/locale/es';

export default ({ defValue, onChange, label, hint}) => (
  <FormGroup className="p-1 border shadow-sm">
      { label && <Label className="pr-0 py-1 pl-2 m-0">{label}</Label> }
        <DatePicker 
          selected={moment(defValue)} //must be moment, not string
          onChange={onChange} 
          withPortal
          showWeekNumbers
          className="form-control py-0 px-2"
          locale="en"
        />
      { hint && <small className="p-0 pl-2 m-0 mt-2 form-text text-muted">{hint}</small> }
    </FormGroup>
)