import React from 'react';
import { FormGroup, Label} from "reactstrap";
import classnames from 'classnames';

const SzrSelect = (
  {options, defValue, onSelect, label, hint, validationError}
) => {

  const showErrorStyles = !!validationError;

  return (
  <FormGroup className="p-1 border shadow-sm">
    { label && <Label className="pr-0 py-1 pl-2 m-0">{label}</Label> }
    <select 
      defaultValue={defValue}
      onChange={onSelect}
      className={ classnames('form-control py-0 px-2 ', {'is-invalid': showErrorStyles })}
    >
    <option key="n/a" value="n/a">Click here to select an option.</option>
      {
        options.map( opt =>  <option key={opt.value} value={opt.value}>{opt.label}</option> )  
      }
    </select>
    { hint && <small className="p-0 pl-2 m-0 mt-2 form-text text-muted">{hint}</small> }
    { validationError && <div className="invalid-feedback p-0 pl-2">{validationError}</div> }
  </FormGroup>
  )//return
}//FormField

export default SzrSelect;
