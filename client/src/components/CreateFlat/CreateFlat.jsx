import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Container, Row, Col, Modal, ModalBody, ModalFooter, Button} from 'reactstrap';
import CreateFlatWizardForm from './CreateFlatWizardForm/CreateFlatWizardForm';
import './CreateFlat.css';
import axios from 'axios';
import moment from 'moment';

import { setDefaultFlat, setFlatToEdit } from '../../redux_store/actions/flatActions';

class CreateFlat extends Component {

  state = {
    showMdlFlatSaveOk: false,
    operation: 'creating', //'creating' or 'editing' - 
                          //needed to display the correct save button in CreateFlatPage6
    defaultFlatForResetFormWhenEditing: {},                  
  }
   
  componentDidMount(){

    if (this.props.match.path === '/edit/:id') {

      //Here, we handle the editing of a flat
    
      if(this.props.match.params.id.length !== 24) {

        /* The flat object ID in mongodb is a 24 character string
        If we get for example http://localhost:3000/edit/5b 
        instead of http://localhost:3000/edit/5b8b45a89770c195f8d8a26b
        then findById of mongoose is going to cause a crash.
        We catch this on client side by routing the user back to /home
        if the string length is not 24. But to be on the safe side, we will
        also do a check on server side.
        */
        this.props.history.push('/home');
        
      } else {

        axios.get('/api/flats/getForEdit', {params: { flatId: this.props.match.params.id }})
        .then(  result => {

          result.data.availableFromDate = moment(result.data.availableFromDate)
          .format('YYYY-MM-DD');
          result.data.createdOnDate = moment(result.data.createdOnDate)
                                      .format('YYYY-MM-DD');
          result.data.publishedOnDate = moment(result.data.publishedOnDate)
                                      .format('YYYY-MM-DD');          

          
          this.props.setFlatToEdit(result.data);

          this.setState({defaultFlatForResetFormWhenEditing: result.data})
          this.setState( {operation: 'editing'} );  

        })//axios.get then
        .catch( error  => console.log(error) );

      }// if(this.props.match.params.id.length !== 24) 
    }//if (this.props.match.path === '/edit/:id')
    else {
      
      //here we handle the case of pure flat creation

      this.setState( {operation: 'creating'} );
      this.props.setDefaultFlat();
    }
  }; //componentDidMount


  handleCreateFlat = () => {

    if (this.state.operation === 'creating') {

      axios.post('/api/flats/create', this.props.theFlat)
      .then( result => {
          this.props.setDefaultFlat();
          this.setState({showMdlFlatSaveOk: true});
      });//axios.post then

    } else {

      axios.post('/api/flats/edit', {
        flatId: this.props.match.params.id, 
        ...this.props.theFlat
      }).then( result => {

        if(result.data.status === 'OK'){
          this.props.setDefaultFlat();
          this.setState({showMdlFlatSaveOk: true});
        }

      });//axios.post then
    }

  }//handleCreateFlat


  resetForm = () => {

    //We fall back to the default flat that is defined in the reducer,
    //in case that we're not editing. If we're editing, then the resetForm
    //function falls back to the flat saved originally

    if (this.props.match.path === '/edit/:id') {
      this.props.setFlatToEdit(this.state.defaultFlatForResetFormWhenEditing);
    } else {
      this.props.setDefaultFlat();
    }
  }//resetForm

  render() {
   
    return (
      <Container fluid className="create-flat">
        <Row className="align-items-center h-100">
          <Col xs={12} sm={12} md={6} lg={6} className="mx-auto">

            <CreateFlatWizardForm 
                onSubmit={this.handleCreateFlat} 
                operation={this.state.operation}
                resetForm={this.resetForm}
              />

            <Modal 
              isOpen={this.state.showMdlFlatSaveOk} 
              style={{width: '75%', margin:'150px auto'}}
            >
              <ModalBody className="text-justify">
                Your announcement has been successfully  
                {
                  this.state.operation === 'creating' ? ' created' : ' updated'  
                }.                
                Redirecting you to the list of your announcements where 
                you can review and edit them.
              </ModalBody>
              <ModalFooter className="">
                <Button 
                  color="success mx-auto" 
                  onClick={ () => this.props.history.push('/my') } >
                  OK
                </Button>
              </ModalFooter>
            </Modal>   

          </Col>
        </Row>  
      </Container>
    )//return
  }; //render
};//class

const mapStateToProps = state => ({
  theFlat: state.flatToCreate
})

export default connect(mapStateToProps, {
  setDefaultFlat,
  setFlatToEdit
})(CreateFlat);

