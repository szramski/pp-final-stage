import React, { Component } from 'react';

import CreateFlatPage1 from './formPages/CreateFlatPage1';
import CreateFlatPage2 from './formPages/CreateFlatPage2';
import CreateFlatPage3 from './formPages/CreateFlatPage3';
import CreateFlatPage4 from './formPages/CreateFlatPage4';
import CreateFlatPage5 from './formPages/CreateFlatPage5';
import CreateFlatPage6 from './formPages/CreateFlatPage6';

export default class CreateFlatWizardForm extends Component {

  state = {  page: 1 };

  nextPage = () => this.setState({ page: this.state.page + 1 });    
  previousPage = () => this.setState({ page: this.state.page - 1 });

  resetFormAndCurrentPage = () => {
    this.setState({ page: 1 });
    this.props.resetForm();
  }
  
  render() {
    const { operation, onSubmit } = this.props;
    const { page } = this.state;

    switch(page){
      case 1:
        return <CreateFlatPage1 
                  nextPage={this.nextPage} 
                  resetForm={this.resetFormAndCurrentPage}
                  />

      case 2:
        return <CreateFlatPage2
                  previousPage={this.previousPage}
                  nextPage={this.nextPage}
                  resetForm={this.resetFormAndCurrentPage}
                />
      case 3:
        return <CreateFlatPage3
                  previousPage={this.previousPage}
                  nextPage={this.nextPage}
                  resetForm={this.resetFormAndCurrentPage}
                />
      case 4:
        return <CreateFlatPage4
                  previousPage={this.previousPage}
                  nextPage={this.nextPage}
                  resetForm={this.resetFormAndCurrentPage}
                />
      case 5:
        return <CreateFlatPage5
                  previousPage={this.previousPage}
                  nextPage={this.nextPage}
                  resetForm={this.resetFormAndCurrentPage}
                />
      case 6:
        return <CreateFlatPage6
                  previousPage={this.previousPage}
                  operation={operation}
                  onSubmit={onSubmit}
                  resetForm={this.resetFormAndCurrentPage}
                />
      default: 
        return <CreateFlatPage1 
        nextPage={this.nextPage} 
        resetForm={this.resetFormAndCurrentPage} 
        />
    }//switch
  };//render
}