import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Container, Row, Col, Card, CardHeader, CardBody, CardFooter} from 'reactstrap';
import classnames from 'classnames';
import isEmpty from 'lodash.isempty';

import SzrNumField from '../../../shared/SzrFormFields/SzrNumField';
import SzrSelect from '../../../shared/SzrFormFields/SzrSelect';
import SzrFeatureButton from '../../../shared/SzrFormFields/SzrFeatureButton';

import {

  setRent,
  setCurrency,
  setPaymentPeriod,
  setDeposit,
  setHasAdditionalCosts,
  setAdditionalCostsAmount,
  setAdditionalCostsPaymentMethod,

} from '../../../../redux_store/actions/flatActions';

const currencies = [
  { value: 'EUR', label: 'EUR' },
  { value: 'PLN', label: 'PLN' },
  { value: 'GBP', label: 'GBP' },
];  

const paymentMethod = [
  { value: 'month', label: 'month' },
  { value: 'week', label: 'week' },
  { value: 'year', label: 'year' },
]; 

const depositTypes = [
  { value: 0, label: 'No Deposit' },
  { value: 1, label: '1 monthly rate' }, //Monatsrate, mensualidad
  { value: 2, label: '2 monthly rates' },
  { value: 3, label: '3 monthly rates' },
  { value: 4, label: '4 monthly rates' },
  { value: 5, label: '5 monthly rates' },
  { value: 6, label: '6 monthly rates' },
  { value: 7, label: 'more than 6 monthly rates' },
];   

class CreateFlatPage3 extends Component {

  state = { valErrors: {} }

  validate = () => {

    let tmp = {};

    if(this.props.rdxCurrency === 'n/a') tmp.currency = 'Required';

    if(this.props.rdxPaymentPeriod === 'n/a') tmp.paymentPeriod = 'Required';

    if(this.props.rdxDeposit === 'n/a') tmp.deposit = 'Required';
    
    if(this.props.rdxHasAdditionalCosts 
      && (this.props.rdxAdditionalCostsPaymentPeriod === 'n/a') )
        tmp.additionalCostsPaymentMethod = 'Required';

    isEmpty(tmp) ? this.props.nextPage() : this.setState({valErrors: tmp});

  };//validate

  render(){

    const { 
      previousPage,
      resetForm,
      rdxRent,
      rdxCurrency,
      rdxPaymentPeriod,
      rdxDeposit,
      rdxHasAdditionalCosts,
      rdxAdditionalCostsAmount,
      rdxAdditionalCostsPaymentMethod,


      setRent,
      setCurrency,
      setPaymentPeriod,
      setDeposit,
      setHasAdditionalCosts,
      setAdditionalCostsAmount,
      setAdditionalCostsPaymentMethod,

    
    } = this.props;
  
  
    return (
        <Card className="mb-5 mt-3 shadow">
          <CardHeader className="my-0 py-1 szr-card-header">
            New Announcement: Payment methods
          </CardHeader>
          <CardBody className="">
            <Container>
             <Row>
                <Col xs={12} sm={12} md={6} xl={6} >
                  <SzrNumField 
                    disabled={false}
                    label="Rent"
                    min={1}
                    max={10000}   
                    onChange={ value => setRent(value) }   
                    value={rdxRent}
                />
                </Col>
                <Col xs={12} sm={12} md={6} xl={6} > 
                  <SzrSelect 
                    label="Currency"        
                    onSelect={ e => setCurrency(e.target.value)}
                    options={currencies}                    
                    validationError={this.state.valErrors.currency}
                    defValue={rdxCurrency}
                  />     
                </Col>    
              </Row>
              <Row>
                <Col xs={12} sm={12} md={12} xl={12} >
                  <SzrSelect 
                    label="Pay per..."        
                    onSelect={ e => setPaymentPeriod(e.target.value)}
                    options={paymentMethod}                    
                    validationError={this.state.valErrors.paymentPeriod}
                    defValue={rdxPaymentPeriod}
                  />                   
                </Col>
              </Row>
              <Row>
                <Col xs={12} sm={12} md={12} xl={12} >
                  <SzrSelect 
                    label="Deposit"        
                    onSelect={ e => setDeposit(e.target.value)}
                    options={depositTypes}                    
                    validationError={this.state.valErrors.deposit}
                    defValue={rdxDeposit}
                  />                       
                </Col>
              </Row>   
              <Row>
                <Col xs={12} sm={12} md={12} xl={12} className="mx-auto">
                  <SzrFeatureButton
                    label={`Additional costs:
                            ${ rdxHasAdditionalCosts ? 'Yes' : 'No'} `
                          } 
                    showOutline={!rdxHasAdditionalCosts}
                    toggleValue={() => setHasAdditionalCosts(!rdxHasAdditionalCosts) }
                  />
  
                </Col>
              </Row>  
              <Row>
                <Col 
                  xs={12} sm={12} md={6} xl={6} 
                  className={ 
                    classnames({'d-none': !rdxHasAdditionalCosts})
                  }
                >
                  <SzrNumField 
                    disabled={false}
                    label="Additional Costs"
                    min={1}
                    max={10000}   
                    onChange={ value => setAdditionalCostsAmount(value) }   
                    value={rdxAdditionalCostsAmount}
                  />
                </Col>      
                <Col 
                  xs={12} sm={12} md={6} xl={6} 
                  className={ 
                    classnames({'d-none': !rdxHasAdditionalCosts})
                  }
                >
                  <SzrSelect 
                    label="Additional Costs paid per..."        
                    onSelect={ e => setAdditionalCostsPaymentMethod(e.target.value)}
                    options={paymentMethod}                    
                    validationError={this.state.valErrors.additionalCostsPaymentMethod}
                    defValue={rdxAdditionalCostsPaymentMethod}
                  />      
                </Col>
              </Row> 
            </Container>
          </CardBody>
          <CardFooter className="m-0 py-2">
          <button type="button" className="btn btn-secondary float-left" onClick={previousPage}>
            Previous
          </button>
          <button 
              type="button"
              onClick={resetForm} 
              className="btn btn-outline-danger shadow-sm float-left ml-3"
              >
              Reset
          </button> 
          <button 
            type="submit" 
            className="btn btn-success float-right" 
            onClick={this.validate}
            >
              Next
            </button>        
          </CardFooter>                            
        </Card>
    );//return
  };//render
};//class



const mapStateToProps = state => ({
  rdxRent: state.flatToCreate.rent,
  rdxCurrency: state.flatToCreate.currency, 
  rdxPaymentPeriod: state.flatToCreate.paymentPeriod, 
  rdxDeposit: state.flatToCreate.deposit, 
  rdxHasAdditionalCosts: state.flatToCreate.hasAdditionalCosts,
  rdxAdditionalCostsAmount: state.flatToCreate.additionalCostsAmount,
  rdxAdditionalCostsPaymentPeriod: state.flatToCreate.additionalCostsPaymentMethod,

})

export default connect(mapStateToProps, {
  setRent,
  setCurrency,
  setPaymentPeriod,
  setDeposit,
  setHasAdditionalCosts,
  setAdditionalCostsAmount,
  setAdditionalCostsPaymentMethod,

})(CreateFlatPage3);
