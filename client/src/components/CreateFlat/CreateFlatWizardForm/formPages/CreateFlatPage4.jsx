import React, {Component} from 'react';
import {connect} from 'react-redux'; 
import {Container, Row, Col, Card, CardHeader, CardBody, CardFooter} from 'reactstrap';
import SzrFeatureButton from '../../../shared/SzrFormFields/SzrFeatureButton';
import isEmpty from 'lodash.isempty';

import {
  setHasAirConditioning,
  setHasBalcony,
  setHasBasement,
  setHasBuiltInClosets,
  setHasDishwasher,
  setHasElevator,
  setHasFridge,
  setHasGarage,
  setHasGarden,
  setHasHeating,
  setHasMicrowave,
  setHasPlayground,
  setHasPool,
  setHasReinforcedDoor,
  setHasStove,
  setHasTV,
  setHasWashingMachine,
  setHasParquetFlooring,
  setIsForPets,
  setIsForSmokers,
  setIsForStudents
} from '../../../../redux_store/actions/flatActions';


class CreateFlatPage4 extends Component{

  state = { valErrors:  {} }

  validate = () => {
    let tmp = {};
    /*
      This page shows simple feature buttons hence we don't do validation
      checking here. But maybe in the future, there will be further fields
      on this page that require validation check. 
      As for now, we simply go to the next page. 
    */
    isEmpty(tmp) ? this.props.nextPage() : this.setState({valErrors: tmp});
  };//validate


  render(){

    const { 
      previousPage, 
      resetForm,

      rdxHasAirConditioning,
      rdxHasBalcony,
      rdxHasBasement,
      rdxHasBuiltInClosets,
      rdxHasDishwasher,
      rdxHasElevator,
      rdxHasFridge,
      rdxHasGarage,
      rdxHasGarden,
      rdxHasHeating,
      rdxHasMicrowave,
      rdxHasPlayground,
      rdxHasPool,
      rdxHasReinforcedDoor,
      rdxHasStove,
      rdxHasTV,
      rdxHasWashingMachine,
      rdxHasParquetFlooring,
      rdxIsForPets,
      rdxIsForSmokers,
      rdxIsForStudents,   
    
      setHasAirConditioning,
      setHasBalcony,
      setHasBasement,
      setHasBuiltInClosets,
      setHasDishwasher,
      setHasElevator,
      setHasFridge,
      setHasGarage,
      setHasGarden,
      setHasHeating,
      setHasMicrowave,
      setHasPlayground,
      setHasPool,
      setHasReinforcedDoor,
      setHasStove,
      setHasTV,
      setHasWashingMachine,
      setHasParquetFlooring,
      setIsForPets,
      setIsForSmokers,
      setIsForStudents,          
    } = this.props;

    return (
      <Card className="mb-5 mt-3 shadow">
        <CardHeader className="my-0 py-1 szr-card-header">
          New Announcement: Features
        </CardHeader>
        <CardBody className="">
          <Container>
          <Row className="mt-2">
              <Col xs={12} sm={12} md={12} xl={12}>
                <h5>Tenant Preference</h5>
              </Col>
            </Row>
            <Row className="border-bottom">
            <Col xs={12} sm={12} md={12} xl={4} >
              <SzrFeatureButton
                label="Students Accepted" 
                showOutline={!rdxIsForStudents}
                toggleValue={() => setIsForStudents(!rdxIsForStudents) }
              />
            </Col>
            <Col xs={12} sm={12} md={12} xl={4} >
              <SzrFeatureButton
                label="Pets Accepted" 
                showOutline={!rdxIsForPets}
                toggleValue={() => setIsForPets(!rdxIsForPets) }
              />
            </Col>
            <Col xs={12} sm={12} md={12} xl={4} >
              <SzrFeatureButton
                label="Smokers Accepted" 
                showOutline={!rdxIsForSmokers}
                toggleValue={() => setIsForSmokers(!rdxIsForSmokers) }
              />
            </Col>
          </Row>
          <Row className="mt-2">
              <Col xs={12} sm={12} md={12} xl={12}>
                <h5>Property Features</h5>
              </Col>
          </Row>
          <Row>
            <Col xs={12} sm={12} md={12} xl={4} >
              <SzrFeatureButton
                label="Balcony" 
                showOutline={!rdxHasBalcony}
                toggleValue={() => setHasBalcony(!rdxHasBalcony) }
              />
            </Col>
            <Col xs={12} sm={12} md={12} xl={4} >
              <SzrFeatureButton
                label="Air Conditioning" 
                showOutline={!rdxHasAirConditioning}
                toggleValue={() => setHasAirConditioning(!rdxHasAirConditioning) }
              />
            </Col>
            <Col xs={12} sm={12} md={12} xl={4} >
              <SzrFeatureButton
                label="Heating" 
                showOutline={!rdxHasHeating}
                toggleValue={() => setHasHeating(!rdxHasHeating) }
              />
            </Col>
          </Row>
          <Row>
              <Col xs={12} sm={12} md={12} xl={4} >
                <SzrFeatureButton
                  label="Built-in Closets" 
                  showOutline={!rdxHasBuiltInClosets}
                  toggleValue={() => setHasBuiltInClosets(!rdxHasBuiltInClosets) }
                />
              </Col>
              <Col xs={12} sm={12} md={12} xl={4} >
                <SzrFeatureButton
                  label="Reinforced Door" 
                  showOutline={!rdxHasReinforcedDoor}
                  toggleValue={() => setHasReinforcedDoor(!rdxHasReinforcedDoor) }
                />               
              </Col>  
              <Col xs={12} sm={12} md={12} xl={4} >
                <SzrFeatureButton
                  label="Parquet Flooring" 
                  showOutline={!rdxHasParquetFlooring}
                  toggleValue={() => setHasParquetFlooring(!rdxHasParquetFlooring) }
                />              
              </Col>                  
            </Row>
            <Row>
              <Col xs={12} sm={12} md={12} xl={4} >
                <SzrFeatureButton
                  label="Fridge" 
                  showOutline={!rdxHasFridge}
                  toggleValue={() => setHasFridge(!rdxHasFridge) }
                />
              </Col>
              <Col xs={12} sm={12} md={12} xl={4} >
                <SzrFeatureButton
                  label="Stove" 
                  showOutline={!rdxHasStove}
                  toggleValue={() => setHasStove(!rdxHasStove) }
                />               
              </Col>  
              <Col xs={12} sm={12} md={12} xl={4} >
                <SzrFeatureButton
                  label="Microwave" 
                  showOutline={!rdxHasMicrowave}
                  toggleValue={() => setHasMicrowave(!rdxHasMicrowave) }
                />              
              </Col>                  
            </Row>
            <Row className="border-bottom">
            <Col xs={12} sm={12} md={12} xl={4} >
                <SzrFeatureButton
                  label="Washing machine" 
                  showOutline={!rdxHasWashingMachine}
                  toggleValue={() => setHasWashingMachine(!rdxHasWashingMachine) }
                />
              </Col>
              <Col xs={12} sm={12} md={12} xl={4} >
                <SzrFeatureButton
                  label="Dishwasher" 
                  showOutline={!rdxHasDishwasher}
                  toggleValue={() => setHasDishwasher(!rdxHasDishwasher) }
                />               
              </Col>  
              <Col xs={12} sm={12} md={12} xl={4} >
                <SzrFeatureButton
                  label="TV" 
                  showOutline={!rdxHasTV}
                  toggleValue={() => setHasTV(!rdxHasTV) }
                />              
              </Col>        
            </Row>
            <Row className="mt-2">
              <Col xs={12} sm={12} md={12} xl={12}>
                <h5>Other Features</h5>
              </Col>
            </Row>
            <Row>
              <Col xs={12} sm={12} md={12} xl={4} >
                <SzrFeatureButton
                  label="Garage" 
                  showOutline={!rdxHasGarage}
                  toggleValue={() => setHasGarage(!rdxHasGarage) }
                />
              </Col>
              <Col xs={12} sm={12} md={12} xl={4} >
                <SzrFeatureButton
                  label="Basement" 
                  showOutline={!rdxHasBasement}
                  toggleValue={() => setHasBasement(!rdxHasBasement) }
                />               
              </Col>  
              <Col xs={12} sm={12} md={12} xl={4} >
                <SzrFeatureButton
                  label="Elevator" 
                  showOutline={!rdxHasElevator}
                  toggleValue={() => setHasElevator(!rdxHasElevator) }
                />              
              </Col>                  
            </Row>
            <Row>
              <Col xs={12} sm={12} md={12} xl={4} >
                <SzrFeatureButton
                  label="Pool" 
                  showOutline={!rdxHasPool}
                  toggleValue={() => setHasPool(!rdxHasPool) }
                />
              </Col>
              <Col xs={12} sm={12} md={12} xl={4} >
                <SzrFeatureButton
                  label="Garden" 
                  showOutline={!rdxHasGarden}
                  toggleValue={() => setHasGarden(!rdxHasGarden) }
                />               
              </Col>  
              <Col xs={12} sm={12} md={12} xl={4} >
                <SzrFeatureButton
                  label="Playground" 
                  showOutline={!rdxHasPlayground}
                  toggleValue={() => setHasPlayground(!rdxHasPlayground) }
                />              
              </Col>                  
            </Row>
          </Container>
        </CardBody>
        <CardFooter className="m-0 py-2">
        <button type="button" className="btn btn-secondary float-left" onClick={previousPage}>
          Previous
        </button>
        <button 
          type="button"
          onClick={resetForm} 
          className="btn btn-outline-danger shadow-sm float-left  ml-3"
          >
          Reset
        </button> 
        <button 
          type="submit" 
          className="btn btn-success float-right" 
          onClick={this.validate}>
            Next
          </button>        
        </CardFooter>                            
      </Card>
    );//return
  };//render
};


const mapStateToProps = state => ({
  rdxHasAirConditioning: state.flatToCreate.hasAirCon ,
  rdxHasBalcony: state.flatToCreate.hasBalcony ,
  rdxHasBasement: state.flatToCreate.hasBasement ,
  rdxHasBuiltInClosets: state.flatToCreate.hasBuiltInClosets ,
  rdxHasDishwasher: state.flatToCreate.hasDishwasher ,
  rdxHasElevator: state.flatToCreate.hasElevator ,
  rdxHasFridge: state.flatToCreate.hasFridge ,
  rdxHasGarage: state.flatToCreate.hasGarage ,
  rdxHasGarden: state.flatToCreate.hasGarden ,
  rdxHasHeating: state.flatToCreate.hasHeating ,
  rdxHasMicrowave: state.flatToCreate.hasMicrowave ,
  rdxHasPlayground: state.flatToCreate.hasPlayground ,
  rdxHasPool: state.flatToCreate.hasPool ,
  rdxHasReinforcedDoor: state.flatToCreate.hasReinforcedDoor ,
  rdxHasStove: state.flatToCreate.hasStove ,
  rdxHasTV: state.flatToCreate.hasTV ,
  rdxHasWashingMachine: state.flatToCreate.hasWashingMachine ,
  rdxHasParquetFlooring: state.flatToCreate.hasParquetFlooring , 
  rdxIsForPets: state.flatToCreate.isForPets ,
  rdxIsForSmokers: state.flatToCreate.isForSmokers,
  rdxIsForStudents: state.flatToCreate.isForStudents ,   
  
});

export default connect(mapStateToProps,{
  setHasAirConditioning,
  setHasBalcony,
  setHasBasement,
  setHasBuiltInClosets,
  setHasDishwasher,
  setHasElevator,
  setHasFridge,
  setHasGarage,
  setHasGarden,
  setHasHeating,
  setHasMicrowave,
  setHasPlayground,
  setHasPool,
  setHasReinforcedDoor,
  setHasStove,
  setHasTV,
  setHasWashingMachine,
  setHasParquetFlooring,
  setIsForPets,
  setIsForSmokers,
  setIsForStudents,
})(CreateFlatPage4);