import React from 'react';
import {Card, CardHeader, CardBody, CardFooter} from 'reactstrap';

import { connect } from 'react-redux';
import { 
  setCountry,
  setLatLng,
  setAddressDetails,
  setPublishStreetNumber
      } from '../../../../redux_store/actions/flatActions';

import { getLatLng, geocodeByAddress } from "react-places-autocomplete";
import { getLocationDetails } from "./FieldRenderer/getLocationDetails";
 
import SzrSelect from '../../../shared/SzrFormFields/SzrSelect';
import SzrFeatureButton from '../../../shared/SzrFormFields/SzrFeatureButton';
import RenderPlaceAutoCompField from './FieldRenderer/RenderPlaceAutoCompField';
import RenderMap from './FieldRenderer/RenderMap';

import {supportedCountries} from '../../../shared/utilData';
import textDB from '../../../shared/textDB.js';

class CreateFlatPage1 extends React.Component {
  
  handleAddressSelect = (selectedAdress) => {

    geocodeByAddress(selectedAdress)
      .then(results => getLatLng(results[0]))
      .then(latlng => this.props.setLatLng(latlng))
      .catch(/*error => console.log(error)*/);

    geocodeByAddress(selectedAdress)
      .then(results => this.props.setAddressDetails(  getLocationDetails(results[0]) ))
      .catch(/*error => console.log(error)*/);
  }; //handleAddressSelect

  render(){

    const { 
      nextPage, 
      resetForm,
      rdxCountry,
      rdxLatlng,
      rdxFormattedAddress,
      rdxPublishStreetNumber
    } = this.props;

    return (
        <Card className="mb-5 mt-3 shadow">
          <CardHeader className="my-0 py-1 szr-card-header">
            New Announcement: Location
          </CardHeader>
          <CardBody>

            <SzrSelect 
              label="Country"        
              onSelect={ event => this.props.setCountry(event.target.value) }
              options={
                supportedCountries.map( c => ({value: c.shortcode, label: c.translation.en}) )
              }                    
              validationError={undefined} 
              defValue={rdxCountry}
            />

          {
            rdxCountry !== 'n/a' 
            ? <RenderPlaceAutoCompField 
                label="Address Lookup" 
                countryCode={rdxCountry} 
                onAddressSelect={this.handleAddressSelect}
                hint={textDB.CRT_FLT_LCT_PLCATOCMPL_HINT}
                />
            : <p>Please select the country first.</p>
          }
   
          {
            (rdxLatlng.lat !== 0 && rdxLatlng.lng !== 0) && (
              <div>
                <Card 
                  className="border shadow-sm mb-3" 
                  style={
                    {
                      backgroundColor: '#f1f1f1',
                      borderRadius: '0px'
                    }
                  }
                >
                  <CardHeader className="pl-3">
                    {rdxFormattedAddress}
                  </CardHeader>
                  <CardBody className="p-0">
                    <RenderMap 
                      coords={rdxLatlng} 
                      containerElement={<div style={{ height: `400px` }} />}
                      mapElement={<div style={{ height: `400px` }} />}
                      formattedAddress={rdxFormattedAddress}
                    />
                  </CardBody>
                </Card>

                <SzrFeatureButton
                  label={`Publish street number: 
                          ${ rdxPublishStreetNumber ? 'Yes' : 'No'} `
                        } 
                  showOutline={!rdxPublishStreetNumber}
                  toggleValue={() => this.props.setPublishStreetNumber(!rdxPublishStreetNumber) }
                />
              </div>
                )
          }
          </CardBody>
          <CardFooter className="m-0 py-2">
            {
              (rdxLatlng.lat !== 0 && rdxLatlng.lng !== 0)
              ? ( 
              <div>
                <button 
                  type="button"
                  onClick={nextPage} 
                  className="btn btn-success shadow-sm float-right"
                  >
                    Next
                </button>
                <button 
                  type="button"
                  onClick={resetForm} 
                  className="btn btn-outline-danger shadow-sm float-left"
                  >
                  Reset
                </button> 
              </div>)   
              : <button 
                  type="button" 
                  className="btn btn-success disabled shadow-sm float-right">
                  Provide location to proceed...
                </button>
            }
          </CardFooter>                            
        </Card>
    );//return
  }//render
};

const mapStateToProps = state => ({
  rdxCountry: state.flatToCreate.country,
  rdxLatlng: state.flatToCreate.latlng,
  rdxFormattedAddress: state.flatToCreate.address_details.formattedAddress,
  rdxPublishStreetNumber: state.flatToCreate.publishStreetNumber
});

export default connect(mapStateToProps, {
  setCountry,
  setLatLng,
  setAddressDetails,
  setPublishStreetNumber
})(CreateFlatPage1);