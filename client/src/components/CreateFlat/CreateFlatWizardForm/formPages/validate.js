const validate = values => {
  const errors = {};

  if (!values.headline) {
    errors.headline = 'Required';
  }  

  if (!values.descr_short) {
    errors.descr_short = 'Required.';
  }

  if (!values.country || values.country === 'n/a') {
    errors.country = 'Required.';
  }  

  if (!values.typeOfProperty || values.typeOfProperty === 'n/a') {
    errors.typeOfProperty = 'Required.';
  }

  if (!values.epcRatingType || values.epcRatingType === 'n/a') {
    errors.epcRatingType = 'Required.';
  }  

  if (!values.furnishingType || values.furnishingType === 'n/a') {
    errors.furnishingType = 'Required.';
  }  

  if (values.floor > values.floorTotal) {
    errors.floor = 'Floor number should be smaller than the total number of floors';
  }  

  if (values.renovationYearKnown && (values.renovationYear < values.constructionYear)) {
    errors.renovationYear = 'Renovation should take place after contruction';
  }  

  if (values.surfaceTotal < values.surfaceNet) {
    errors.surfaceTotal = 'Total surface should be larger than net surface';
  }  


  if (!values.currency || values.currency === 'n/a') {
    errors.currency = 'Required.';
  }  

  if (!values.paymentPeriod || values.paymentPeriod === 'n/a') {
    errors.paymentPeriod = 'Required.';
  }  
  
  if (!values.deposit || values.deposit === 'n/a') {
    errors.deposit = 'Required.';
  }  

  //console.log("additionalCostsPaymentMethod", values.additionalCostsPaymentMethod)
  //console.log("deposit", values.deposit)

  if (values.hasAdditionalCosts && 
    (!values.additionalCostsPaymentMethod || values.additionalCostsPaymentMethod === 'n/a')) {
    errors.additionalCostsPaymentMethod = 'Required';
  }  
  
  console.log("fucking errors in formPages/validate.js", errors)
  return errors;
};

export default validate;
