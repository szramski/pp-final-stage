import React, {Component} from 'react';
import {Card, CardHeader, CardBody, CardFooter} from 'reactstrap';
import {connect} from 'react-redux'; 
import isEmpty from 'lodash.isempty';

import SzrTextField from '../../../shared/SzrFormFields/SzrTextField';
import SzrTextArea from '../../../shared/SzrFormFields/SzrTextArea';

import {
  setHeadline,
  setDescrShort,
  setDescrLong,
} from '../../../../redux_store/actions/flatActions';

class CreateFlatPage5 extends Component {

  state = { valErrors:  {} }

  validate = () => {
    let tmp = {};

    if(this.props.rdxHeadline.trim() === '') tmp.headline = 'Required';

    if(this.props.rdxDescrShort.trim() === '') tmp.descr_short = 'Required';
    
    isEmpty(tmp) ? this.props.nextPage() : this.setState({valErrors: tmp});
  };//validate

  render(){

    const {  
      previousPage,
      resetForm,
      rdxHeadline,
      rdxDescrShort,
      rdxDescrLong,       

      setHeadline,
      setDescrShort,
      setDescrLong,    
    } = this.props;

    return (
        <Card className="mb-5 mt-3 shadow">
          <CardHeader className="my-0 py-1 szr-card-header">
            New Announcement: Description
          </CardHeader>
          <CardBody className="">
  
            <SzrTextField 
              hint="Example: Sunny flat in Málaga next to the beach."
              label="Headline"
              onChange={ e => setHeadline(e.target.value) }
              type="text" 
              validationError={this.state.valErrors.headline}
              value={rdxHeadline}
            />

            <SzrTextArea 
              hint="This text will be always visible to the user."
              label="Short description"
              onChange={ e => setDescrShort(e.target.value) }
              type="text" 
              validationError={this.state.valErrors.descr_short}
              value={rdxDescrShort}
            />

            <SzrTextArea 
              hint="This text will be displayed if the user wishes to read it. Please provide further details about your real estate but please try to avoid writing facts like for example the 
              number of rooms or the rent because you've already provided that info."
              label="Detailed description"
              onChange={ e => setDescrLong(e.target.value) }
              type="text" 
              validationError={undefined}
              value={rdxDescrLong}
            />

          </CardBody>
          <CardFooter className="m-0 py-2">
            <button type="button" className="btn btn-secondary float-left" 
              onClick={previousPage}>
                Previous
            </button>
            <button 
              type="button"
              onClick={resetForm} 
              className="btn btn-outline-danger shadow-sm float-left  ml-3"
              >
              Reset
            </button>     
            <button 
              type="submit" 
              className="btn btn-success shadow-sm float-right"
              onClick={this.validate}>
                Next
            </button>
          </CardFooter>                            
        </Card>
    );//return
  };//render
};

const mapStateToProps = state => ({
  rdxHeadline: state.flatToCreate.headline,
  rdxDescrShort: state.flatToCreate.descr_short,
  rdxDescrLong: state.flatToCreate.descr_long,
});

export default connect(mapStateToProps,{
  setHeadline,
  setDescrShort,
  setDescrLong,
})(CreateFlatPage5);
