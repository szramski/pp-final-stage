import React from 'react';
import { connect } from 'react-redux';
import {Card, CardHeader, CardBody, CardFooter} from 'reactstrap';
import ReactLoading from 'react-loading';

import Dropzone from 'react-dropzone';
import axios from 'axios';

import RenderThumbnail from './FieldRenderer/RenderThumbnail';

import { setFlatImages } from '../../../../redux_store/actions/flatActions'; 

class CreateFlatPage6 extends React.Component {

  state = {
    uploading: false,
    fileBeingUploaded: '',
    publicIdToDelete: '',
  }

  deletePictureFromCloudinary = async public_id => {
    
    this.setState({publicIdToDelete: public_id})
  
    let result = await axios.delete('/api/utils/cloudinary/delete', {params: {public_id}});
      
    let newImages = this.props.rdxFlatImages.filter( item => item.public_id !== result.data.deleted);
     
      this.props.setFlatImages(newImages); 

  };//deletePictureFromCloudinary
  

  handleFileUpload = async (files, rdxFlatImages) => {

    this.setState({
      uploading: true, 
      fileBeingUploaded: files[0].name
    });

    let mrData = new FormData();
    mrData.append('newFlatImage', files[0]);

    let res = await axios.put('/api/utils/cloudinary/upload', mrData);
  
    rdxFlatImages.push(res.data);
    this.setState({images: rdxFlatImages})
    this.setState({uploading: false});
    this.props.setFlatImages(rdxFlatImages); 
  }; //handleFileUpload

  render(){

    const { 
      onSubmit, 
      previousPage,
      resetForm, 
      rdxFlatImages,
    } = this.props;
    
    const allowedCountOfUploadedImages = 10;


    return (
        <Card className="mb-5 mt-3 shadow">
          <CardHeader className="my-0 py-1 szr-card-header">
            New Announcement: Picture upload
          </CardHeader>
          <CardBody className="p-0">

          {rdxFlatImages.length < allowedCountOfUploadedImages 
          && <Dropzone
              accept="image/jpeg, image/png"
              className="p-5 border-bottom shadow-sm text-center align-items-center w-100"
              multiple={false}
              onDrop={(files) => this.handleFileUpload(files, rdxFlatImages)}
              style={{backgroundColor: '#ccc', cursor: 'pointer'}}
              disabled={this.state.uploading}
            >            
          {
            this.state.uploading 
            ? <h5 className="m-0">
                Uploading and processing:<br/>{this.state.fileBeingUploaded}
                <ReactLoading 
                  type='spin' 
                  color='white' 
                  height={45} 
                  width={45} 
                  className="mx-auto mt-5 d-block" 
                /> 
              </h5>
            : <h5 className="m-0">
                    Drop an image here or click to select one.
                    <br/>
                    {allowedCountOfUploadedImages - rdxFlatImages.length} of {allowedCountOfUploadedImages} images remaining.
              </h5> 
          }
          </Dropzone>
          }          

          {
            rdxFlatImages 
              && <div className="card-deck mx-auto w-100"> 
                  <RenderThumbnail
                    images={rdxFlatImages}
                    publicIdToDelete={this.state.publicIdToDelete} 
                    deletePictureFromCloudinary={this.deletePictureFromCloudinary} 
                  />
                </div>
          }  
          </CardBody>
          <CardFooter className="m-0 py-2">
            <button 
              type="button" 
              className="btn btn-secondary float-left" 
              onClick={previousPage}>
                Previous
              </button>
              <button 
                type="button"
                onClick={resetForm} 
                className="btn btn-outline-danger shadow-sm float-left ml-3"
                >
                Reset
              </button> 
            { 
              this.props.operation === 'creating' 
                &&  <button 
                      type="button" 
                      className="btn btn-success shadow-sm float-right"
                      onClick={onSubmit}
                    >
                      Create
                    </button>  
            }

            { 
              this.props.operation === 'editing' 
                &&  <button 
                      type="submit" 
                      className="btn btn-success shadow-sm float-right"
                      onClick={onSubmit} 
                    >
                      Update
                    </button>  
            }

          </CardFooter>                            
        </Card>
    ); 
  }
};

const mapStateToProps = state => ({
  rdxFlatImages: state.flatToCreate.flatImages,
})

export default connect(mapStateToProps, {
  setFlatImages
})(CreateFlatPage6);