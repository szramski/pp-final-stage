import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Container, Row, Col, Card, CardHeader, CardBody, CardFooter} from 'reactstrap';
import moment from 'moment';
import classnames from 'classnames';
import isEmpty from 'lodash.isempty';

import SzrDatePicker from '../../../shared/SzrFormFields/SzrDatePicker';
import 'react-datepicker/dist/react-datepicker.css';
import SzrNumField from '../../../shared/SzrFormFields/SzrNumField';
import SzrSelect from '../../../shared/SzrFormFields/SzrSelect';
import SzrFeatureButton from '../../../shared/SzrFormFields/SzrFeatureButton';

import {

  setTypeOfProperty,
  setAvailableFromDate,
  setEpcRatingType,
  setSolarOrientation,
  setFurnishingType,
  setNumberOfRooms,
  setNumberOfBathRooms,
  setSurfaceTotal,
  setSurfaceNet,
  setFloor,
  setFloorTotal,  
  setConstructionYearKnown,
  setRenovationYearKnown,
  setConstructionYear,
  setRenovationYear,

} from '../../../../redux_store/actions/flatActions';

const typesOfProperty = [
  { value: 'Flat', label: 'Flat' },
  { value: 'Room', label: 'Room' },
  { value: 'Room in a shared flat', label: 'Room in a shared flat' },
  { value: 'Attic flat', label: 'Attic flat' },
  { value: 'Studio appartment', label: 'Studio Appartment' },
  { value: 'Ground floor', label: 'Ground Floor' },//planta Baja, Erdgeschoss
  { value: 'House', label: 'House' },
  { value: 'Maisonette', label: 'Maisonette' }, //Duplex
];    

const furnishingTypes = [
  { value: 'Unfurnished', label: 'Unfurnished' },
  { value: 'Rooms and kitchen', label: 'Furnished: Rooms & Kitchen' },
  { value: 'Rooms only', label: 'Furnished: Rooms Only' },
  { value: 'Kitchen only', label: 'Furnished: Kitchen Only' },
];   

const solarOrientationTypes = [
  { value: 'north', label: 'north' },
  { value: 'east', label: 'east' },
  { value: 'west', label: 'west' },
  { value: 'south', label: 'south' },
];   


const epcRatingTypes = [
  { value: 'A', label: 'A' },
  { value: 'B', label: 'B' },
  { value: 'C', label: 'C' },
  { value: 'D', label: 'D' },
  { value: 'E', label: 'E' },
  { value: 'F', label: 'F' },
  { value: 'G', label: 'G' },
  { value: 'Currently Being Obtained', label: 'Currently Being Obtained' },
  { value: 'EPC Not Required', label: 'EPC not required' },
]  

class CreateFlatPage2 extends Component {

  //setting an empty object of validation errors in the component state
  state = { valErrors: {} }

  //The validate function populates properties on the valErrors obj in the
  //state in case that there are any errors.
  validate = () => {

    let tmp = {};

    if(this.props.rdxTypeOfProperty === 'n/a') tmp.typeOfProperty = 'Required';

    if(this.props.rdxEpcRatingType  === 'n/a') tmp.epcRatingType  = 'Required';  

    if(this.props.rdxFurnishingType === 'n/a') tmp.furnishingType = 'Required';
    
    if(this.props.rdxSurfaceTotal < this.props.rdxSurfaceNet) 
      tmp.surfaceTotal = 'Should be bigger than net surface.';

    if(this.props.rdxFloor > this.props.rdxFloorTotal) 
      tmp.floor = 'Should be smaller than total floor count.'

    if(this.props.rdxRenovationYearKnown 
        && (this.props.rdxRenovationYear < this.props.rdxConstructionYear)
      )
          tmp.renovationYear = 'Should be bigger than contruction year.'

    isEmpty(tmp) ? this.props.nextPage() : this.setState({valErrors: tmp});
  };//validate

render(){

  const { 
    previousPage,
    resetForm,
    rdxTypeOfProperty,
    rdxAvailableFromDate,
    rdxEpcRatingType,
    rdxSolarOrientation,
    rdxFurnishingType,
    rdxNumberOfRooms,
    rdxNumberOfBathRooms,
    rdxSurfaceTotal,
    rdxSurfaceNet,
    rdxFloor,
    rdxFloorTotal,
    rdxConstructionYearKnown,
    rdxRenovationYearKnown,
    rdxConstructionYear,
    rdxRenovationYear,
  
    setTypeOfProperty,
    setAvailableFromDate,
    setEpcRatingType,
    setSolarOrientation,
    setFurnishingType,
    setNumberOfRooms,
    setNumberOfBathRooms,
    setSurfaceTotal,
    setSurfaceNet,
    setFloor,
    setFloorTotal,  
    setConstructionYearKnown,
    setRenovationYearKnown,  
    setConstructionYear,
    setRenovationYear,    
    } = this.props;

  return (
    <Card className="mb-5 mt-3 shadow">
      <CardHeader className="my-0 py-1 szr-card-header">
        New Announcement: Property details
      </CardHeader>
      <CardBody className="">
        <Container>
          <Row>
            <Col xs={12} sm={12} md={12} xl={12} >
              <SzrSelect 
                label="Property Type"        
                onSelect={ e => setTypeOfProperty(e.target.value)}
                options={typesOfProperty}                    
                validationError={this.state.valErrors.typeOfProperty} 
                defValue={rdxTypeOfProperty}
              />                
            </Col>   
          </Row>   
          <Row>
            <Col xs={12} sm={12} md={12} xl={12} >
            <SzrDatePicker 
                label="Available From"
                onChange={ date => setAvailableFromDate(moment(date).format('YYYY-MM-DD'))}
                defValue={rdxAvailableFromDate}
              />
            </Col>
          </Row>                 
          <Row>
            <Col xs={12} sm={12} md={6} xl={6} >
              <SzrSelect 
                label="EPC rating"        
                onSelect={ e => setEpcRatingType(e.target.value)}
                options={epcRatingTypes}                    
                validationError={this.state.valErrors.epcRatingType}
                defValue={rdxEpcRatingType}
              />                
            </Col>   
            <Col xs={12} sm={12} md={6} xl={6} >
              <SzrSelect 
                label="Solar orientation"        
                onSelect={ e => setSolarOrientation(e.target.value)}
                options={solarOrientationTypes}                    
                validationError={undefined} 
                defValue={rdxSolarOrientation}
              />                
            </Col>                 
          </Row> 
          <Row>
            <Col xs={12} sm={12} md={12} xl={12} >
            <SzrSelect 
                label="Furnishing"        
                onSelect={ e => setFurnishingType(e.target.value)}
                options={furnishingTypes}                    
                validationError={this.state.valErrors.furnishingType}
                defValue={rdxFurnishingType}
              />                  
            </Col>
          </Row>
          <Row>
            <Col xs={12} sm={12} md={6} xl={6} >
              <SzrNumField 
                disabled={false}
                label="Number of bedrooms"
                min={1}
                max={20}   
                onChange={ value => setNumberOfRooms(value) }   
                value={rdxNumberOfRooms}
            />
            </Col>
            <Col xs={12} sm={12} md={6} xl={6} >
              <SzrNumField 
                disabled={false}
                label="Number of bathrooms"
                min={1}
                max={10}   
                onChange={ value => setNumberOfBathRooms(value) }   
                value={rdxNumberOfBathRooms}
              />
            </Col>              
          </Row>
          <Row>
            <Col xs={12} sm={12} md={6} xl={6} >
              <SzrNumField 
                disabled={false}
                label="Total surface (&#13217;)"
                min={1}
                max={1000}   
                onChange={ value => setSurfaceTotal(value) }   
                value={rdxSurfaceTotal}
                validationError={this.state.valErrors.surfaceTotal}
                />
            </Col>
            <Col xs={12} sm={12} md={6} xl={6} >
              <SzrNumField 
                disabled={false}
                label="Habitable net surface (&#13217;)"
                min={1}
                max={1000}   
                onChange={ value => setSurfaceNet(value) }   
                value={rdxSurfaceNet}
                />
            </Col>              
          </Row>
          <Row>
            <Col xs={12} sm={12} md={6} xl={6}>
              <SzrNumField 
                disabled={false}
                label="Floor"
                min={0}
                max={100}   
                onChange={ value => setFloor(value) }   
                value={rdxFloor}
                validationError={this.state.valErrors.floor}
              />
            </Col>
            <Col xs={12} sm={12} md={6} xl={6}>
            <SzrNumField 
                disabled={false}
                label="Total number of floors"
                min={0}
                max={100}   
                onChange={ value => setFloorTotal(value) }   
                value={rdxFloorTotal}
              />
            </Col>              
          </Row>  
          <Row>
            <Col xs={12} sm={12} md={6} xl={6} lg={6} className="mx-auto">
            <SzrFeatureButton
                label={`Construction year known: 
                        ${ rdxConstructionYearKnown ? 'Yes' : 'No'} `
                      } 
                showOutline={!rdxConstructionYearKnown}
                toggleValue={() => setConstructionYearKnown(!rdxConstructionYearKnown) }
              />
            </Col>
            <Col xs={12} sm={12} md={6} xl={6} lg={6} className="mx-auto">
            <SzrFeatureButton
                label={`Renovation year known: 
                        ${ rdxRenovationYearKnown ? 'Yes' : 'No'} `
                      } 
                showOutline={!rdxRenovationYearKnown}
                toggleValue={() => setRenovationYearKnown(!rdxRenovationYearKnown) }
              />                
            </Col>
          </Row>
          <Row>
          <Col xs={12} sm={12} md={6} xl={6}
              className={ 
                classnames({
                  'visible': rdxConstructionYearKnown, 
                  'invisible': !rdxConstructionYearKnown
          })}>
            <SzrNumField 
                disabled={false}
                label="Year of construction"
                min={1800}
                max={5000}   
                onChange={ value => setConstructionYear(value) }   
                value={rdxConstructionYear}
            />
          </Col>              
          <Col xs={12} sm={12} md={6} xl={6}
              className={ 
                classnames({
                  'visible': rdxRenovationYearKnown, 
                  'invisible': !rdxRenovationYearKnown
          })}>
            <SzrNumField 
                disabled={false}
                label="Most recent renovation"
                min={1800}
                max={5000}   
                onChange={ value => setRenovationYear(value) }   
                value={rdxRenovationYear}
                validationError={this.state.valErrors.renovationYear}
            />
        </Col>
          </Row>
        </Container>
      </CardBody>
      <CardFooter className="m-0 py-2">
      <button type="button" 
        className="btn btn-secondary float-left" 
        onClick={previousPage}
      >
        Previous
      </button>
      <button 
        type="button"
        onClick={resetForm} 
        className="btn btn-outline-danger shadow-sm float-left ml-3"
        >
        Reset
      </button>       
      <button 
        type="button"
        onClick={this.validate} 
        className="btn btn-success float-right">
          Next
        </button>        
      </CardFooter>                            
    </Card>
  );//return
}; //render
};//class

const mapStateToProps = state => ({
  rdxTypeOfProperty: state.flatToCreate.typeOfProperty,
  rdxAvailableFromDate: state.flatToCreate.availableFromDate,
  rdxEpcRatingType: state.flatToCreate.epcRatingType,
  rdxSolarOrientation: state.flatToCreate.solarOrientation,
  rdxFurnishingType: state.flatToCreate.furnishingType,
  rdxNumberOfRooms: state.flatToCreate.numberOfRooms,
  rdxNumberOfBathRooms: state.flatToCreate.numberOfBathRooms,
  rdxSurfaceTotal: state.flatToCreate.surfaceTotal,
  rdxSurfaceNet: state.flatToCreate.surfaceNet,
  rdxFloor: state.flatToCreate.floor,
  rdxFloorTotal: state.flatToCreate.floorTotal,
  rdxConstructionYearKnown: state.flatToCreate.constructionYearKnown,
  rdxRenovationYearKnown: state.flatToCreate.renovationYearKnown,
  rdxConstructionYear: state.flatToCreate.constructionYear,
  rdxRenovationYear: state.flatToCreate.renovationYear,
})

export default connect(mapStateToProps, {
  setTypeOfProperty,
  setAvailableFromDate,
  setEpcRatingType,
  setSolarOrientation,
  setFurnishingType,
  setNumberOfRooms,
  setNumberOfBathRooms,
  setSurfaceTotal,
  setSurfaceNet,
  setFloor,
  setFloorTotal,
  setConstructionYearKnown,
  setRenovationYearKnown,
  setConstructionYear,
  setRenovationYear,

})(CreateFlatPage2);