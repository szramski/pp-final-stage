import React from 'react';
import { FormGroup, Label} from "reactstrap";
import PlacesAutocomplete from 'react-places-autocomplete';


import './RenderPlaceAutoCompField.css';

class RenderPlaceAutoCompField extends React.Component {

  state = {
    address_details: {
      streetNumber: "",
      streetName: "",
      city: "",
      postalcode: "",
      region: "",
      country: "",
      country_short: "",
      formattedAddress: '',
    },
    temporaryAddress: '',
    address_latlng: { 
      lat: 0, //Default
      lng: 0
    }
    //address_latlng: { lat: 0, lng: 0 }
  };

  onChange = temporaryAddress => this.setState({ temporaryAddress });


  handleCoordinates = address_latlng => {
    this.setState({ address_latlng });
  };

  render(){

    const { label, hint, countryCode } = this.props;


    const inputProps = {
      value: this.state.temporaryAddress,
      placeholder:  'Enter search term here.',
      onChange: this.onChange
    };
  
    const cssClasses = {
      root: "form-group w-100",
      input: "form-control form-control w-100 ",
      autocompleteContainer: "szr-autocomplete-container",
      autocompleteItem: "szr-autocomplete-item",
      autocompleteItemActive: "szr-autocomplete-item-active"
    };
    
      return (
        <FormGroup className="p-1 border shadow-sm">
        { label && <Label className="pr-0 py-1 pl-2 m-0">{label}</Label> }
          <PlacesAutocomplete 
            inputProps={inputProps} 
            classNames={cssClasses} 
            options={{
              componentRestrictions:{ country: [ countryCode ]},
            }}
            onChange={ this.onChange }
            onSelect={ () => this.props.onAddressSelect(this.state.temporaryAddress) }
            />

        { hint && <small className="p-0 pl-2 m-0 mt-2 form-text text-muted">{hint}</small> }
      </FormGroup>
      )//return

  }//render




  
  
};

export default RenderPlaceAutoCompField;
