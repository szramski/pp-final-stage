import React from 'react';

import {Col, Button, Card, CardImg, CardBody} from 'reactstrap';

const RenderThumbnail = ({images, publicIdToDelete, deletePictureFromCloudinary}) => {

    return images.map( (image, id ) => 
    <Col key={id} xs={12} sm={12} md={12} xl={6} lg={6}>
    <Card className="border shadow-sm m-2">        
        <CardImg 
          top 
          width="100%" 
          src={image.thumb_url} 
          alt="Card image cap"           
        />
        <CardBody>
        <Button 
            type="button"
            className="btn btn-danger btn-block btn-sm my-2"
            onClick={() => deletePictureFromCloudinary(image.public_id)}
          >
            { image.public_id === publicIdToDelete ? 'Deleting...' : 'Delete'  }
        </Button>          
        </CardBody>
      </Card>
    </Col>
    );//map
      
}//RenderThumbnails

export default RenderThumbnail;