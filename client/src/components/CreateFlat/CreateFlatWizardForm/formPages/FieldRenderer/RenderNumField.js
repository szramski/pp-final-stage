import React from 'react';

/* import Form Field Component */
import SzrNumField from '../../../../shared/SzrFormFields/SzrNumField';

const RenderNumField = ({ input, label, placeholder, min, max,value, disabled, hint, meta: { error } }) => {

    //console.log("renderNumField Error", error);

    return (
      <SzrNumField 
      name={label}
      label={label}
      min={min}
      max={max}      
      placeholder={placeholder}
      hint={hint}
      value={value}
      disabled={disabled}
      {...input}          
      reduxFormError={ 
        /* the error var of Redux Form contains always the error condition but 
        only if the field was touched, we want to show the error and error styling, 
        hence we do: */
        error}
      />
    )//return
};

export default RenderNumField;
