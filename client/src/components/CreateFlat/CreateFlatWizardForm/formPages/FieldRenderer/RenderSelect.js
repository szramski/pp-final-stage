import React from 'react';

/* import Form Field Component */
import SzrSelect from '../../../../shared/SzrFormFields/SzrSelect';



const RenderSelect = ({ options, input, label, hint, disabled, meta: { touched, error } }) => {

    // for debugging: console.log("renderField Error", touched && error);

    return (
      <SzrSelect 
      options={options}
      name={label}
      label={label}
      hint={hint}
      disabled={disabled}
      {...input}          
      reduxFormError={ 
        /* the error var of Redux Form contains always the error condition but 
        only if the field was touched, we want to show the error and error styling, 
        hence we do: */
        touched && error}
      touched={touched}  
      />
    )//return
};

export default RenderSelect;
