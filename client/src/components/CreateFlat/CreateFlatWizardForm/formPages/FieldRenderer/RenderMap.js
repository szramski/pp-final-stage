import React from 'react';
import { withGoogleMap, GoogleMap, Marker } from 'react-google-maps';

const RenderMap = ({coords, formattedAddress }) => (

  <GoogleMap
    defaultZoom={18}
    center={{ lat: coords.lat, lng: coords.lng }}>
    <Marker position={{ lat: coords.lat, lng: coords.lng }} />
  </GoogleMap> 
)

export default withGoogleMap(RenderMap);