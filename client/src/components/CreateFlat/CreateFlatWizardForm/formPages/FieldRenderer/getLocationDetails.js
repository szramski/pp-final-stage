import {countries} from 'countries-list';


export const getLocationDetails = data => {
  let addressDetailsArr = data.address_components;

  //console.log("DATA ", data );

  let addressDetails = {};

  try {
    addressDetails.streetNumber = addressDetailsArr.filter(
      element => element.types[0] === "street_number"
    )[0].long_name;
  } catch (error) {
    if (error) addressDetails.streetNumber = "n/a";
  }


  try {
    addressDetails.streetName = addressDetailsArr.filter(
      element => element.types[0] === "route"
    )[0].short_name;
  } catch (error) {
    if (error) addressDetails.streetName = "n/a";
  }

  try {
    addressDetails.streetName = addressDetailsArr.filter(
      element => element.types[0] === "route"
    )[0].long_name;
  } catch (error) {
    if (error) addressDetails.streetName = "n/a";
  }

  try {
    addressDetails.city = addressDetailsArr.filter(
      element => element.types[0] === "locality"
    )[0].long_name;
  } catch (error) {
    if (error) {


      try {
        addressDetails.city = addressDetailsArr.filter(
          element => element.types[0] === "administrative_area_level_4"
        )[0].long_name;
      } catch (error) {
        if (error) addressDetails.city = "n/a";
      }
    


    }
  }

  try {
    addressDetails.postalcode = addressDetailsArr.filter(
      element => element.types[0] === "postal_code"
    )[0].long_name;
  } catch (error) {
    if (error) addressDetails.postalcode = "n/a";
  }

  try {
    addressDetails.region = addressDetailsArr.filter(
      element => element.types[0] === "administrative_area_level_1"
    )[0].long_name;
  } catch (error) {
    if (error) addressDetails.region = "n/a";
  }

  try {
    addressDetails.country = addressDetailsArr.filter(
      element => element.types[0] === "country"
    )[0].long_name;
  } catch (error) {
    if (error) addressDetails.country = "n/a";
  }

  try {
    addressDetails.country_short = addressDetailsArr.filter(
      element => element.types[0] === "country"
    )[0].short_name;
  } catch (error) {
    if (error) addressDetails.country_short = "n/a";
  }  

  addressDetails.formattedAddress = data.formatted_address;

  addressDetails.country = countries[addressDetails.country_short].native;

  return addressDetails;
};
