import React from 'react';

/* import Form Field Component */
import SzrTextField from '../../../../shared/SzrFormFields/SzrTextField';

const RenderTextField = ({ input, label, placeholder, type, hint, meta: { touched, error } }) => {

    // for debugging: console.log("renderField Error", touched && error);

    return (
      <SzrTextField 
      type={type} 
      name={label}
      label={label}
      placeholder={placeholder}
      hint={hint}
      {...input}          
      reduxFormError={ 
        /* the error var of Redux Form contains always the error condition but 
        only if the field was touched, we want to show the error and error styling, 
        hence we do: */
        touched && error}
      touched={touched}  
      />
    )//return
};

export default RenderTextField;
