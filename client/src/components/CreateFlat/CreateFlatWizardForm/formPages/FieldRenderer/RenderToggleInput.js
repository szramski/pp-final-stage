import React from 'react';

/* import Form Field Component */
import SzrToggle from '../../../../shared/SzrFormFields/SzrToggle';

const RenderToggleInput = ({ input, label, hint, meta: { touched, error } }) => {

    // for debugging: console.log("renderField Error", touched && error);

    return (
      <SzrToggle 
      name={label}
      label={label}
      hint={hint}
      {...input}          
      reduxFormError={ 
        /* the error var of Redux Form contains always the error condition but 
        only if the field was touched, we want to show the error and error styling, 
        hence we do: */
        touched && error}
      touched={touched}  
      />
    )//return
};

export default RenderToggleInput;
