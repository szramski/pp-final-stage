import React, { Component } from 'react';
import { connect } from 'react-redux';
import Header from '../../h_f_s/Header/Header';
import { Route, Redirect } from 'react-router-dom';


class PrivateRoute extends Component {

  state={showSideBar: false};

  toggleSideBar = () => {
    this.setState({showSideBar: !this.state.showSideBar});
  }

  render(){

    const {isAuthenticated, showLiked, component: Component, ...rest} = this.props;

    return (
      <div>
        <Header
        enableSideBar={isAuthenticated}
        showSideBar={this.state.showSideBar}
        toggleSideBar={this.toggleSideBar}
        />  
        <Route {...rest} 
        component={ props => isAuthenticated ?  <Component {...props} 
                                                showLiked={showLiked} /> 
                                              : <Redirect to="/" />} 
        />
      </div>      
    )
  }


};

const mapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated
})

export default connect(mapStateToProps)(PrivateRoute);
