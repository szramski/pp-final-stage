/**
 * Component: ClientRoutes
 * Parent Component: Provider / BrowserRouter in index.js
 * Goal: To catch and handle routes that are provided by the user in the browser.
 * 
 */

import React, {Component} from 'react';
import { Switch } from 'react-router-dom';

/** importing main components */
import Home       from '../Home/Home';
import Register   from '../l_r_l/Register/Register';
import Login      from '../l_r_l/Login/Login';
import CreateFlat from '../CreateFlat/CreateFlat';
import Landing    from '../l_r_l/Landing/Landing';
import PublicRoute from './PublicRoute/PublicRoute';
import PrivateRoute from './PrivateRoute/PrivateRoute';
import Profile from '../Profile/Profile';
import SearchSettings from '../SearchSettings/SearchSettings';
import UserMessages from '../UserMessages/UserMessages';

class ClientRoutes extends Component {

  render(){

    return (  
      <Switch>
        <PublicRoute path="/"             component={ Landing } exact />
        <PublicRoute path="/register"     component={ Register } />
        <PublicRoute path="/login"        component={ Login } />
        <PrivateRoute path="/home"        component={ Home } />
        <PrivateRoute path="/liked"       component={ Home } />
        <PrivateRoute path="/bookmarked"  component={ Home } />
        <PrivateRoute path="/trashed"     component={ Home } />                
        <PrivateRoute path="/profile"     component={ Profile } />
        <PrivateRoute path="/create"      component={ CreateFlat } /> 
        <PrivateRoute path="/edit/:id"    component={ CreateFlat }/>
        <PrivateRoute path="/settings"    component={ SearchSettings }/>
        <PrivateRoute path="/messages"    component={ UserMessages }/>
        <PrivateRoute                     component={ Home } /> 
      </Switch>
    )//return
  }//render
};//class

export default ClientRoutes;

