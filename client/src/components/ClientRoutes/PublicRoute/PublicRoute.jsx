import React from 'react';
import Header from '../../h_f_s/Header/Header';
import { connect } from 'react-redux';
import { Route, Redirect } from 'react-router-dom';

const PublicRoute = ({isAuthenticated, component: Component, ...otherProps}) => (
      <div>
        <Header
        enableSideBar={false}
        showSideBar={false}
        toggleSideBar={ undefined }
        />  
        <Route {...otherProps} 
        component={ props => isAuthenticated ? <Redirect to="/home" /> : <Component {...props} /> }
        />
      </div>      
);
  
const mapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated
})

export default connect(mapStateToProps)(PublicRoute);
