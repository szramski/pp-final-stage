import React, { Component } from "react";
import { Container, Row, Col } from "reactstrap";
import {Link} from 'react-router-dom';
import "./Landing.css";

/* import text strings */
import textDB from '../../shared/textDB'

export default class Landing extends Component {

  render() {
    return (
      <Container fluid className="hero">
        <Row className="align-items-center text-center h-100">
          <Col md="12" className="">
            <div className="szr-welcome text-white">
              {textDB.HERO_SLOGAN}              
            </div>
            <Link to="/register" className="btn btn-lg btn btn-outline-light mr-3">
              {textDB.HERO_SIGNUP}
            </Link> 
            <Link to="/login" className="btn btn-lg btn btn-outline-light">
              {textDB.HERO_SIGNIN}
            </Link>
          </Col>
        </Row>
      </Container>
    );
  }
}
