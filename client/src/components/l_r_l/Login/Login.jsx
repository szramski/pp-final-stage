import React, { Component } from "react";
import { Link, withRouter } from 'react-router-dom';
import {
  Container,
  Row,
  Col,
  Card,
  CardHeader,
  CardFooter,
  CardBody,
  Button,
  Form,
} from "reactstrap";

/* import Form Field Component */
import SzrTextField from '../../shared/SzrFormFields/SzrTextField';

/* import text strings */
import textDB from '../../shared/textDB';

/*import the function that is going to connect our component to the store*/
import { connect }  from 'react-redux';

/* import the signInUser action */
import { signInUser } from '../../../redux_store/actions/authActions';

/* import the styles for this component */
import "./Login.css";

const mapGlobalStateToProps = globalState =>{
  return {
    auth: globalState.auth,
    validationErrors: globalState.error
  }
}

class Login extends Component {

  /* Initialize an empty object in the component state */
  state = {
    email: '',
    password: '',
    validationErrors: {}
  };

  /**************************************************************/
  /**************************************************************/

  componentWillReceiveProps(nextProps){

  if(nextProps.auth.isAuthenticated){

    /** This if branch gets entered after the user has successfully
     * logged in. The successful login causes that the auth reducer
     * gets updated and isAuthenticated gets set to true. This re-renders
     * this Login component and we arrive exactly in this branch.
     * We check if nextProps.auth.isAuthenticated is true and if it is
     * we forward the user to /home.
     */
    this.props.history.push('/home');
  }

  if(nextProps.validationErrors){
    this.setState( {validationErrors: nextProps.validationErrors} );
  }
  }//componentWillReceiveProps

  /**************************************************************/
  /**************************************************************/

  handleInputChange = (event) => {
    /** This fct gets invoked on each change of any input.
     * event.target.name contains the name of the input that
     * is being changed and event.target.value contains the 
     * value of the input. This helps us to understand which
     * property of the object needs to be written:
    */
    this.setState({ [event.target.name] : event.target.value });
  }

  /**************************************************************/
  /**************************************************************/
  handleFormSubmit = (event) => {
    /** This fct gets called when the submit btn is pressed.
     * First, we prevent the default behavior of the form
     * i.e. no submitting = no page reload!
     */
    event.preventDefault();

    /** Next, we define our new user login by taking the values out
     * of the component state
     */
    const login = {
      email: this.state.email,
      password: this.state.password,
    }

    /* Calling the Redux action to log in the user */
    this.props.signInUser(login);

  }//onSubmit

  /**************************************************************/
  /**************************************************************/
  render() {

    /* desrturcture the validationErros from the state */
    const { validationErrors } = this.state

    return (
      <Container fluid className="login">
        <Row className="align-items-center text-center h-100">
          <Col md="3" className="mx-auto">

          <Form onSubmit={ this.handleFormSubmit }>
            <Card className="my-0 py-0 shadow">
            
              <CardHeader className="my-0 py-1 szr-card-header">
                {textDB.LGN_HDR}
              </CardHeader>
              <CardBody>

                <SzrTextField 
                  type="email"
                  name="email"
                  placeholder={textDB.LGN_PLCHLDR_EMAIL}
                  value={this.state.email}
                  onChange={this.handleInputChange}              
                  validationError={validationErrors.email}
                />

                <SzrTextField 
                  type="password"
                  name="password"
                  placeholder={textDB.LGN_PLCHLDR_PWD}
                  value={this.state.password}
                  onChange={this.handleInputChange}              
                  validationError={validationErrors.password}
                />                                  
              </CardBody>
              <CardFooter>
                <Link to="/register" 
                  className="btn btn-outline-dark btn-block mb-2">
                   {textDB.LGN_BTN_GOTO_SIGNUP}
                </Link>
                <Button className="btn btn-outline-success btn-block" 
                  type="submit">
                    {textDB.LGN_BTN_DO_SIGNIN}
                </Button>
              </CardFooter>
            </Card>
            </Form>

          </Col>
        </Row>
      </Container>
    ) //return
  } //render
}//class

export default connect(mapGlobalStateToProps, {signInUser})(withRouter(Login));
