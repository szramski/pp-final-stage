import React, { Component } from "react";
import { Link, withRouter } from 'react-router-dom';
import {
  Container,
  Row,
  Col,
  Card,
  CardHeader,
  CardFooter,
  CardBody,
  Button,
  Form,
} from "reactstrap";

/* import Form Field Component */
import SzrTextField from '../../shared/SzrFormFields/SzrTextField';

/* import text strings */
import textDB from '../../shared/textDB';

/*import the function that is going to connect our component to the store*/
import { connect }  from 'react-redux';

/* import the signInUser action */
import {signUpUser } from '../../../redux_store/actions/authActions';

/* import the styles for this component */
import "./Register.css"

class Register extends Component {

  /** Initialize an empty user object in the component state */
  state = {
    firstname: '',
    lastname: '',
    email: '',
    password: '',
    password2: '',
    validationErrors: {}
  }

  /* If the user is alredy logged in, we don't want him to be able to call /signin   */
  componentDidMount(){
    if(this.props.auth.isAuthenticated){
      this.props.history.push('/flatlist')
    }
  }  

  componentWillReceiveProps(nextProps){
    if(nextProps.validationErrors){
      this.setState({validationErrors: nextProps.validationErrors})
    }//if
  }

  handleInputChange = event => {
    this.setState({ [event.target.name] : event.target.value })
  }

  onSubmit = (event) => {
    event.preventDefault()

    /** We define our new user by taking the values out
     * of the component state
     */
    const newUser = {
      firstname: this.state.firstname,
      lastname: this.state.lastname,
      email: this.state.email,
      password: this.state.password,
      password2: this.state.password2,
    }

    /** Dispatch the signInUser action to the Redux Store
     * and pass to it the history prop so that we can
     * redirect the user to login page after successful
     * registration from within the signUpUser Action.
     * Without withRouter this wouldn't be possible.
     */
    this.props.signUpUser(newUser, this.props.history)
  }

  render() {

    /** pull out the validationErrors out of the state object by destructuring it */
    const {validationErrors} = this.state

    return (
      <Container fluid className="register">
        <Row className="align-items-center text-center h-100">
          <Col md="3" className="mx-auto">
          <Form onSubmit={this.onSubmit}>
            <Card className="my-0 py-0 shadow">
              <CardHeader className="my-0 py-1 szr-card-header">
                {textDB.RGSTR_HDR}
              </CardHeader>
              <CardBody>

                <SzrTextField 
                  type="text"
                  name="firstname"
                  placeholder={textDB.RGSTR_PLCHLDR_FN}
                  value={this.state.firstname}
                  onChange={this.handleInputChange}              
                  validationError={validationErrors.firstname}
                />
                <SzrTextField 
                  type="text"
                  name="lastname"
                  placeholder={textDB.RGSTR_PLCHLDR_LN}
                  value={this.state.lastname}
                  onChange={this.handleInputChange}              
                  validationError={validationErrors.lastname}
                />
                <SzrTextField 
                  type="email"
                  name="email"
                  placeholder={textDB.RGSTR_PLCHLDR_EMAIL}
                  value={this.state.email}
                  onChange={this.handleInputChange}              
                  validationError={validationErrors.email}
                />
                <SzrTextField 
                  type="password"
                  name="password"
                  placeholder={textDB.RGSTR_PLCHLDR_PWD}
                  value={this.state.password}
                  onChange={this.handleInputChange}              
                  validationError={validationErrors.password}
                />
                <SzrTextField 
                  type="password"
                  name="password2"
                  placeholder={textDB.RGSTR_PLCHLDR_CPWD}
                  value={this.state.password2}
                  onChange={this.handleInputChange}              
                  validationError={validationErrors.password2}
                />
              </CardBody>
              <CardFooter>
                <Link to="/login" 
                  className="btn btn-outline-dark mr-5 btn-block mb-2">
                    {textDB.RGSTR_BTN_GOTO_SIGNIN}
                </Link>
                <Button className="btn btn-outline-success btn-block" 
                  type="submit">
                    {textDB.RGSTR_BTN_DO_SIGNUP}
                </Button>
              </CardFooter>
            </Card>
            </Form>
          </Col>
        </Row>
      </Container>
    ) //return
  } //render
} //class

const mapStateToProps = state =>{
  return {
    auth: state.auth,
    validationErrors: state.error
  }
}

export default connect(mapStateToProps, { signUpUser } )(withRouter(Register))
