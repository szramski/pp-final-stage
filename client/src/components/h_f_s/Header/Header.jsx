import React, { Component } from 'react';
import { Container, Row, Col } from 'reactstrap';
import FontAwesome from 'react-fontawesome';
import SideBar from '../Sidebar/Sidebar';
import './Header.css';

export default class Header extends Component {

  render() {

    const {enableSideBar} = this.props;
 
    return (

      <Container fluid className="header fixed-top mb-5">
        { enableSideBar && <SideBar {...this.props}/> }
        <Row>
          <Col xs={12} sm={12} md={12} lg={12} className="p-0 text-center">
          { enableSideBar && <FontAwesome 
                            name="bars" 
                            onClick={this.props.toggleSideBar} 
                            className="float-left ml-3"
                            />  
          }
            <span id="szr-app-title">Proxipiso</span>
          </Col>          
        </Row>
      </Container>      

    )//return

  }//render
}//class