import React from 'react';
import { Container, Row, Col } from 'reactstrap';
import FontAwesome from 'react-fontawesome';

export default (props) => (
  <a onClick={
    e => {
      e.preventDefault();
      props.toggleSideBar();
    }
  }>
    <Container>
      <Row>
        <Col xs={2} sm={2} md={2} lg={2}>
          <FontAwesome name="window-close"/>
        </Col>
        <Col xs={10} sm={10} md={10} lg={10}>
          Close Menu
        </Col>        
      </Row>
    </Container>    
  </a>
) 

