import React from 'react';
import { Container, Row, Col } from 'reactstrap';
import { NavLink } from 'react-router-dom';
import FontAwesome from 'react-fontawesome';
import './SideBarItem.css';

export default ({faIcon, text, linkTo, toggleSideBar}) => (
  <NavLink exact to={linkTo} activeClassName="szr-active-navlink" onClick={toggleSideBar}>
    <Container>
      <Row>
        <Col xs={2} sm={2} md={2} lg={2}>
          <FontAwesome name={faIcon} />
        </Col>
        <Col xs={10} sm={10} md={10} lg={10}>
          {text}
        </Col>        
      </Row>
    </Container>
  </NavLink>
) 