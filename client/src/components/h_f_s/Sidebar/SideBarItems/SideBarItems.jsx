import React from 'react';
import SideBarItem from './SideBarItem/SideBarItem';
//import CloseMenuLink from './CloseMenuLink/CloseMenuLink';
import LogOutLink from './LogOutLink/LogOutLink';
import './SideBarItems.css';

import {sideBarItemsArray} from '../../../shared/utilData';

const SideBarItems = (props) => (
  <div className="szr-side-nav-item-list">
    {
      sideBarItemsArray.map( (item, i) => <SideBarItem 
                                      key={i} 
                                      faIcon={item.faIcon} 
                                      text={item.text}
                                      linkTo={item.linkTo}
                                      toggleSideBar={props.toggleSideBar}
                                      /> )
    }
    <LogOutLink toggleSideBar={props.toggleSideBar} />
    {/* <CloseMenuLink toggleSideBar={props.toggleSideBar} /> */}
  </div>
);//SideBarItems

export default SideBarItems;