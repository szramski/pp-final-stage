import React from 'react';
import { connect } from 'react-redux';
import { signOutUser } from '../../../../../redux_store/actions/authActions';
import { Container, Row, Col } from 'reactstrap';
import FontAwesome from 'react-fontawesome';

const mapStateToProps = state => {
  return { auth: state.auth }
};

const LogOutLink = props => (
  <a onClick={
    e => {
      e.preventDefault();
      props.signOutUser();
      // props.history.push('/');      
      props.toggleSideBar();
    }
  }>
    <Container>
      <Row>
        <Col xs={2} sm={2} md={2} lg={2}>
          <FontAwesome name="sign-out alt"/>
        </Col>
        <Col xs={10} sm={10} md={10} lg={10}>
          Logout
        </Col>        
      </Row>
    </Container>    
  </a>
) 

export default connect(mapStateToProps, {signOutUser})(LogOutLink);
