import React from 'react';
import SideNav from 'react-simple-sidenav';
import SideBarItems from './SideBarItems/SideBarItems';

export default (props) => {
  return (
    <div>
      <SideNav 
        showNav={props.showSideBar}
        onHideNav={props.toggleSideBar}
        navStyle={{
          backgroundImage: 'linear-gradient(rgba(0, 77, 64, 0.7),rgba(0, 77, 64, 0.7)), url(/assets/dsgn/bg/plant.jpg)',
          backgroundSize: 'cover',
          backgroundPosition: 'center',
          borderRight: '1px solid white',
          maxWidth: '300px',
        }}
        >
          <SideBarItems toggleSideBar={props.toggleSideBar} />
        </SideNav>     
      
    </div>
  )
}
