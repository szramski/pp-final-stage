import React, { Component } from 'react';

//import axios from 'axios';

import {
  Container,
  Row,
  Col,
  Card,
  CardHeader,
  CardFooter,
  CardBody,
  Button,
  Form,
  Modal,
  ModalBody,
  ModalFooter, 
} from "reactstrap";

/* import Form Field Component */
//import SzrTextField from '../shared/SzrFormFields/SzrTextField';

import './SearchSettings.css';

import textDB from '../shared/textDB';

class SearchSettings extends Component {

  state = {
    showMdlSettingsSaved: false,
  }

  componentDidMount() {
    //get search settings
  };//componentDidMount

  handleInputChange = event => {
    this.setState({ [event.target.name] : event.target.value })
  }

  onSubmit = event => {
    event.preventDefault();
  //save search settings
  };//onSubmit


  render() {
  
    return (
      <Container fluid className="settings">
        <Row className="align-items-center text-center h-100">
          <Col xs={8} sm={8} md={8} xl={8} lg={8} className="mx-auto">
          <Form onSubmit={this.onSubmit}>
            <Card className="my-0 py-0 shadow">
              <CardHeader className="my-0 py-1 szr-card-header">
                Search settings
              </CardHeader>
              <CardBody>
                <Container fluid>
                  <Row className="text-center"> 
                    <Col xs={12} sm={12} md={12} xl={12} lg={12}>
                      <h1>To be continued...</h1>
                    </Col>
                  </Row>                                  
                </Container>
              </CardBody>
              <CardFooter>
                <Button className="btn btn-outline-success btn-block" 
                  type="submit">
                    {textDB.PRFL_BTN_DO_UPDATE}
                </Button>
              </CardFooter>
            </Card>
            </Form>
          </Col>
        </Row>
        <Modal 
              isOpen={this.state.showMdlSettingsSaved} 
              style={{width: '75%', margin:'150px auto'}}
            >
              <ModalBody className="text-justify">
                The search settings have been successfully updated.
              </ModalBody>
              <ModalFooter className="">
                <Button 
                  color="success mx-auto" 
                  onClick={ () => this.setState({showMdlProfileUpdateOk: false}) } >
                  OK
                </Button>
              </ModalFooter>
            </Modal>   
      </Container>
    )
  }
}

export default SearchSettings;
