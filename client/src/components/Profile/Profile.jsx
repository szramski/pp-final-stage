import React, { Component } from 'react';
import { connect } from 'react-redux';

import Dropzone from 'react-dropzone';
import axios from 'axios';

import {
  Container,
  Row,
  Col,
  Card,
  CardHeader,
  CardFooter,
  CardBody,
  Button,
  Modal,
  ModalBody,
  ModalFooter, 
} from "reactstrap";

import { setUserAvatar } from '../../redux_store/actions/authActions';

/* import Form Field Component */
import SzrTextField from '../shared/SzrFormFields/SzrTextField';

import './Profile.css';

import textDB from '../shared/textDB';

class Profile extends Component {

  /** Initialize an empty user object in the component state */
  state = {
    firstname: '',
    lastname: '',
    email: '',
    avatar: '',
    password: '',
    password2: '',
    validationErrors: {},
    showMdlProfileUpdateOk: false,
    uploadingProfileImage: false,
  }

  componentDidMount() {
    axios.get('/api/users/getinfo')
    .then( result => {
      this.setState({firstname: result.data.firstname});
      this.setState({lastname: result.data.lastname});
      this.setState({email: result.data.email});
      this.setState({avatar: result.data.avatar});
    })
  };//componentDidMount

  handleInputChange = event => {
    this.setState({ [event.target.name] : event.target.value })
  }

  componentWillReceiveProps(nextProps){
    if(nextProps.validationErrors){
      this.setState({validationErrors: nextProps.validationErrors})
    }//if
  }

  handleProfileUpdate = () => {

    const updatedUser = {
      firstname: this.state.firstname,
      lastname: this.state.lastname,
      email: this.state.email,
      password: this.state.password,
      password2: this.state.password2,
    }

    axios.post('/api/users/update', updatedUser)
    .then(  result => {
        this.setState({showMdlProfileUpdateOk: true});
        this.setState({firstname: result.data.firstname});
        this.setState({lastname: result.data.lastname});
        this.setState({email: result.data.email});
        this.setState({avatar: result.data.avatar});
        this.setState({ validationErrors: {} });
    })
    .catch( error  => this.setState({ validationErrors: error.response.data} ));
    
  };//handleProfileUpdate


  handleProfileImageUpdate = async images => {

    this.setState({uploadingProfileImage: true});

    let mrData = new FormData();
    mrData.append('newProfileImage', images[0]);

    let res = await axios.put('/api/utils/cloudinary/uploadProfileImage', mrData);

    this.setState({avatar: res.data.url});
    this.setState({uploadingProfileImage: false});
    this.props.setUserAvatar(res.data.url);
  }

  render() {
  
    const {validationErrors} = this.state;

return (
  <Container fluid className="profile py-3">
    <Row className="h-100">
      <Col xs={11} sm={12} md={6} xl={6} lg={6} className="mx-auto">
        <Card className="my-0 py-0 shadow">
          <CardHeader className="my-0 py-1 szr-card-header text-center">
            {textDB.PRFL_HDR}
          </CardHeader>
          <CardBody className="p-0">
            <Container className="">
              <Row>
                <Col 
                xs={12} sm={12} md={6} xl={6} lg={6}
                className="mt-1">
                  
                  <SzrTextField 
                          type="text"
                          name="firstname"
                          label="First Name"
                          placeholder={textDB.RGSTR_PLCHLDR_FN}
                          value={this.state.firstname}
                          onChange={this.handleInputChange}              
                          validationError={validationErrors.firstname}
                      />   

                        <SzrTextField 
                          type="text"
                          name="lastname"
                          label="Last Name"
                          placeholder={textDB.RGSTR_PLCHLDR_LN}
                          value={this.state.lastname}
                          onChange={this.handleInputChange}              
                          validationError={validationErrors.lastname}
                      />       

                        <SzrTextField 
                          type="email"
                          name="email"
                          label="Email"
                          placeholder={textDB.RGSTR_PLCHLDR_EMAIL}
                          value={this.state.email}
                          onChange={this.handleInputChange}              
                          validationError={validationErrors.email}
                        />                                            
                </Col>
                <Col 
                xs={12} sm={12} md={6} xl={6} lg={6}
                className="pt-md-1 px-0 pb-4">
                    <Dropzone
                          accept="image/jpeg, image/png"
                          className="text-center align-items-center w-100"
                          multiple={false}
                          onDrop={ files  => this.handleProfileImageUpdate(files)}
                          style={{backgroundColor: '#fff', cursor: 'pointer'}}
                          disabled={this.state.uploading}
                        >   
                          <img 
                            className="shadow-sm mt-5"
                            src={this.state.avatar} 
                            width="150px"
                            alt={`${this.state.firstname} ${this.state.lastname}`} 
                          />
                      </Dropzone>
                      <p className="text-center my-2">
                        {
                          this.state.uploadingProfileImage 
                          ? 'Uploading...'
                          : 'Click on the image to change it.'
                        }  
                      </p>
                </Col>                
              </Row>
              <Row>
              <Col 
                xs={12} sm={12} md={6} xl={6} lg={6}
                className="">
                  <SzrTextField 
                    label="New password"
                    type="password"
                    name="password"
                    placeholder={textDB.RGSTR_PLCHLDR_PWD}
                    value={this.state.password}
                    onChange={this.handleInputChange}              
                    validationError={validationErrors.password}
                  />     
              </Col>   
              <Col 
                xs={12} sm={12} md={6} xl={6} lg={6}
                className="">
                  <SzrTextField 
                    label="Confirm new password"
                    type="password"
                    name="password2"
                    placeholder={textDB.RGSTR_PLCHLDR_CPWD}
                    value={this.state.password2}
                    onChange={this.handleInputChange}              
                    validationError={validationErrors.password2}
                  />
              </Col>                                 
              </Row>
              <Row>
                <Col xs={12} sm={12} md={12} xl={12} lg={12} 
                className="my-1 text-center">
                  Hint: Leave the password fields empty if you don't wish
                  to update your password.
                </Col>
              </Row>              
            </Container>
          </CardBody> 
          <CardFooter>
            <Button className="btn btn-outline-success btn-block" 
              type="button"
              onClick={this.handleProfileUpdate}>
                {textDB.PRFL_BTN_DO_UPDATE}
            </Button>
          </CardFooter>          
        </Card>
      </Col>
    </Row>
    <Modal 
        isOpen={this.state.showMdlProfileUpdateOk} 
        style={{width: '75%', margin:'150px auto'}}
    >
      <ModalBody className="text-justify">
        Your profile has been successfully updated.
        <br/>Please log out of the session and log in back again
        so that the changes become effective.
      </ModalBody>
      <ModalFooter className="">
        <Button 
          color="success mx-auto" 
          onClick={ () => this.setState({showMdlProfileUpdateOk: false}) } >
          OK
        </Button>
      </ModalFooter>
    </Modal>   
  </Container>
    )//return
  }; //render
}//class

export default connect(undefined, {
  setUserAvatar
})(Profile);
