import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { getFlatsFromDB } from '../../../../redux_store/actions/flatActions';
import { Tooltip } from 'reactstrap';

import axios from 'axios';
import FontAwesome from 'react-fontawesome';

class TrashButton extends Component {

  state= { tooltipOpen: false };

  toggleToolTip = () => this.setState({tooltipOpen: !this.state.tooltipOpen})

  trashFlat = flatId => {
    axios.post('/api/flats/trash', {flatId})
    .then( res => this.props.getFlatsFromDB(this.props.location.pathname) );
  };

  untrashFlat = flatId => {
    axios.post('/api/flats/untrash', {flatId})
    .then( res => this.props.getFlatsFromDB(this.props.location.pathname) );
  };

  render() {
    
    const { flat, userId } = this.props;
    const flatId = flat._id;
    
    /*  check if the id of the user who's currently logged in
        exists in the flatBookmarks array and if yes, save true or
        false in userBookmarkedFlat variable and return the like or
        unlike button accordingly */
 
    let userTrashedFlat = flat.flatTrashed.map( trashed => trashed._id.toString()).indexOf(userId) !== -1;

    return userTrashedFlat 
              ? <button 
                  type="button" 
                  id={`TrashToolTipTarget${flatId}`} 
                  className="btn btn-sm btn-warning mr-2"
                  onClick={ () => this.untrashFlat(flatId) }>
                    <FontAwesome name="trash" className=""/>
                    <Tooltip placement="bottom" 
                      isOpen={this.state.tooltipOpen}      
                      target={`TrashToolTipTarget${flatId}`}  
                      toggle={this.toggleToolTip}>
                        Unmark irrelevant
                    </Tooltip>                  
                </button>
              : <button 
                    type="button" 
                    id={`TrashToolTipTarget${flatId}`} 
                    className="btn btn-sm btn-outline-secondary mr-2"
                    onClick={ () => this.trashFlat(flatId) }>
                      <FontAwesome name="trash" className=""/>
                      <Tooltip placement="bottom" 
                        isOpen={this.state.tooltipOpen}      
                        target={`TrashToolTipTarget${flatId}`}  
                        toggle={this.toggleToolTip}>
                          Mark as irrelevant
                    </Tooltip>                        
                </button>              
  }//render
}//class

const mapStateToProps = state => ({
  userId: state.auth.user.id
})

export default connect(mapStateToProps, { getFlatsFromDB })(withRouter(TrashButton));

