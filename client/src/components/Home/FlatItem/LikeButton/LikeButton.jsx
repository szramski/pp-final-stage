import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { getFlatsFromDB } from '../../../../redux_store/actions/flatActions';
import axios from 'axios';
import FontAwesome from 'react-fontawesome';
import { Tooltip } from 'reactstrap';

class LikeButton extends Component {

  state = { tooltipOpen: false };

  toggleToolTip = () => this.setState({tooltipOpen: !this.state.tooltipOpen})  

  likeFlat = flatId => {
    axios.post('/api/flats/like', {flatId})
    .then( res => this.props.getFlatsFromDB(this.props.location.pathname) );
  };

  unlikeFlat = flatId => {
    axios.post('/api/flats/unlike', {flatId})
    .then( res => this.props.getFlatsFromDB(this.props.location.pathname) );
  };

  render() {
    
    const { flat, userId } = this.props;
    const flatId = flat._id;
    
    /*  check if the id of the user who's currently logged in
        exists in the flatLikes array and if yes, save true or
        false in userLikesFlat variable and return the like or
        unlike button accordingly */
 
    let userLikesFlat = flat.flatLikes.map( like => like._id.toString()).indexOf(userId) !== -1;

    return userLikesFlat 
              ? <button 
                  type="button" 
                  id={`LikeToolTipTarget${flatId}`} 
                  className="btn btn-sm btn-danger mr-2"
                  onClick={ () => this.unlikeFlat(flatId) }>
                    <FontAwesome name="heart" className=""/>
                    <Tooltip placement="bottom" 
                      isOpen={this.state.tooltipOpen}      
                      target={`LikeToolTipTarget${flatId}`} 
                      toggle={this.toggleToolTip}>
                        Remove Like
                    </Tooltip>                          
                </button>
              : <button 
                    type="button" 
                    id={`LikeToolTipTarget${flatId}`}
                    className="btn btn-sm btn-outline-secondary mr-2"
                    onClick={ () => this.likeFlat(flatId) }>
                      <FontAwesome name="heart" className=""/>
                      <Tooltip placement="bottom" 
                        isOpen={this.state.tooltipOpen}      
                        target={`LikeToolTipTarget${flatId}`} 
                        toggle={this.toggleToolTip}>
                          Like
                    </Tooltip>                           
                </button>              
  }//render
}//class

const mapStateToProps = state => ({
  userId: state.auth.user.id
})

export default connect(mapStateToProps, { getFlatsFromDB })(withRouter(LikeButton));

