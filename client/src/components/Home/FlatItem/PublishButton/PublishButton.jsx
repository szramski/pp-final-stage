import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { getFlatsFromDB } from '../../../../redux_store/actions/flatActions';
import { Tooltip } from 'reactstrap';

import axios from 'axios';
import FontAwesome from 'react-fontawesome';

class PublishButton extends Component {

  state= { tooltipOpen: false };

  toggleToolTip = () => this.setState({tooltipOpen: !this.state.tooltipOpen})

  publishFlat = flatId => {
    axios.post('/api/flats/publish', {flatId})
    .then( res => this.props.getFlatsFromDB(this.props.location.pathname) );
  };

  unpublishFlat = flatId => {
   axios.post('/api/flats/unpublish', {flatId})
   .then( res => this.props.getFlatsFromDB(this.props.location.pathname) );
  };

  render() {
    
    const { flatId, publishAnnouncement } = this.props;
      
    return publishAnnouncement 
              ? <button 
                  type="button" 
                  id={`PublishToolTipTarget${flatId}`}
                  className="btn btn-sm btn-success mr-2"
                  onClick={ () => this.unpublishFlat(flatId) }>
                    <FontAwesome name="globe" className=""/>
                    <Tooltip placement="bottom" 
                      isOpen={this.state.tooltipOpen}      
                      target={`PublishToolTipTarget${flatId}`}
                      toggle={this.toggleToolTip}>
                        Unpublish Announcement
                    </Tooltip>                        
                </button>              
              : <button 
                  type="button" 
                  id={`PublishToolTipTarget${flatId}`}
                  className="btn btn-sm btn-outline-secondary mr-2"
                  onClick={ () => this.publishFlat(flatId) }>
                    <FontAwesome name="globe" className=""/>
                    <Tooltip placement="bottom" 
                      isOpen={this.state.tooltipOpen}      
                      target={`PublishToolTipTarget${flatId}`}
                      toggle={this.toggleToolTip}>
                        Publish Announcement
                    </Tooltip>                  
                </button>
  }//render
}//class

const mapStateToProps = state => ({
  userId: state.auth.user.id
})

export default connect(mapStateToProps, { getFlatsFromDB })(withRouter(PublishButton));

