import React from 'react';
import {Container, Row, Col} from 'reactstrap';
import Moment from 'react-moment';
import FontAwesome from 'react-fontawesome';

Moment.globalLocale = 'de';

const FurtherFacts = ({
  currency,
  address_details,
  constructionYearKnown,
  hasAdditionalCosts,
  hasAirCon,
  hasBalcony,
  hasBasement,
  hasBuiltInClosets,
  hasDishwasher,
  hasFridge,
  hasGarage,
  hasGarden,
  hasHeating,
  hasMicrowave,
  hasParquetFlooring,
  hasPlayground,
  hasPool,
  hasReinforcedDoor,
  hasStove,
  hasTV,
  hasWashingMachine,
  isForPets,
  isForSmokers,
  isForStudents,
  publishStreetNumber,
  renovationYearKnown,
  floorTotal,
  additionalCostsAmount,
  constructionYear,
  renovationYear,
  epcRatingType,
  solarOrientation,
  furnishingType,
  additionalCostsPaymentMethod
}) => {
  
  const { streetNumber, streetName, postalcode, region, city, country } = address_details;

  const paintYesOrNo = value => value ? 
                                <span className="text-success px-1">
                                  <FontAwesome name="check" />
                                </span> 
                                :
                                <span className="text-danger px-1">
                                <FontAwesome name="times" />
                                </span>                               
  
  return (
    <Container fluid>

      <Row className="text-left p-0 mt-0 mb-3 border-bottom">
        <Col xs={12} sm={12} md={12} lg={12} xl={12} className="p-0">
          <h4 className="m-0 mb-1 p-0">General Info</h4>
        </Col>
      </Row>

      {
        hasAdditionalCosts &&      
          <Row className="text-left p-0 my-2">
            <Col xs={12} sm={12} md={12} lg={12} xl={12} className="p-0">
              Additional Costs:{' '}
              { currency === 'GBP' && <span dangerouslySetInnerHTML={{__html: '&#163;'}}></span>}
              { additionalCostsAmount }
              { currency === 'PLN' && <span dangerouslySetInnerHTML={{__html: '&#122;&#322;'}}></span>  }
              { currency === 'EUR' && <span dangerouslySetInnerHTML={{__html: '&#8364;'}}></span>  }
              {' '}per { additionalCostsPaymentMethod }
            </Col>   
          </Row>
      }
          <Row className="text-left p-0 my-2">
            <Col xs={12} sm={12} md={4} lg={4} xl={4} className="p-0">
              Construction Year: { constructionYearKnown ? constructionYear : 'n/a' }
            </Col>   
            <Col xs={12} sm={12} md={4} lg={4} xl={4} className="p-0">
              Solar orientation: { solarOrientation }
            </Col>  
            <Col xs={12} sm={12} md={4} lg={4} xl={4} className="p-0">
              EPC Rating: { epcRatingType }
            </Col>   
            <Col xs={12} sm={12} md={4} lg={4} xl={4} className="p-0">
              Most recent renovation: { renovationYearKnown ? renovationYear : 'n/a' }        
            </Col>   
            <Col xs={12} sm={12} md={4} lg={4} xl={4} className="p-0">
              Total number of floors: { floorTotal }
            </Col>    
            <Col xs={12} sm={12} md={4} lg={4} xl={4} className="p-0">
              Furnishing: { furnishingType }
            </Col>                                         
          </Row>

      <Row className="text-left p-0 my-3 border-bottom">
        <Col xs={12} sm={12} md={12} lg={12} xl={12} className="p-0">
          <h4 className="m-0">Tenant Preference</h4>
        </Col>
      </Row>



      <Row className="text-left p-0 my-2">
        <Col xs={12} sm={12} md={3} lg={3} xl={3} className="p-0">
          Somkers allowed: { paintYesOrNo(isForSmokers) }
        </Col>   
        <Col xs={12} sm={12} md={3} lg={3} xl={3} className="p-0">
          Pets allowed: { paintYesOrNo(isForPets) }
        </Col>   
        <Col xs={12} sm={12} md={3} lg={3} xl={3} className="p-0">
          Students allowed: { paintYesOrNo(isForStudents) }
        </Col>   
        <Col xs={12} sm={12} md={3} lg={3} xl={3} className="p-0">
          
        </Col>                                    
      </Row> 

      <Row className="text-left p-0 my-3 border-bottom">
        <Col xs={12} sm={12} md={12} lg={12} xl={12} className="p-0">
          <h4 className="m-0">Features</h4>
        </Col>
      </Row>

      <Row className="text-left p-0 my-2">
        <Col xs={12} sm={12} md={3} lg={3} xl={3} className="p-0">
          Balcony: { paintYesOrNo(hasBalcony) }
        </Col>   
        <Col xs={12} sm={12} md={3} lg={3} xl={3} className="p-0">
          Air Con: { paintYesOrNo(hasAirCon) }
        </Col>   
        <Col xs={12} sm={12} md={3} lg={3} xl={3} className="p-0">
          Heating: { paintYesOrNo(hasHeating) }
        </Col>   
        <Col xs={12} sm={12} md={3} lg={3} xl={3} className="p-0">
          Built-In Closets: { paintYesOrNo(hasBuiltInClosets) }
        </Col>                                    
      </Row>          

      <Row className="text-left p-0 my-2">
        <Col xs={12} sm={12} md={3} lg={3} xl={3} className="p-0">
          Reinforced Door: { paintYesOrNo(hasReinforcedDoor) }
        </Col>   
        <Col xs={12} sm={12} md={3} lg={3} xl={3} className="p-0">
          Parquet Flooring: { paintYesOrNo(hasParquetFlooring) }
        </Col>   
        <Col xs={12} sm={12} md={3} lg={3} xl={3} className="p-0">
          Fridge: { paintYesOrNo(hasFridge) }
        </Col>   
        <Col xs={12} sm={12} md={3} lg={3} xl={3} className="p-0">
          Stove: { paintYesOrNo(hasStove) }
        </Col>                                    
      </Row>   

      <Row className="text-left p-0 my-2">
        <Col xs={12} sm={12} md={3} lg={3} xl={3} className="p-0">
          Microwave: { paintYesOrNo(hasMicrowave) }
        </Col>   
        <Col xs={12} sm={12} md={3} lg={3} xl={3} className="p-0">
          Washing Machine: { paintYesOrNo(hasWashingMachine) }
        </Col>   
        <Col xs={12} sm={12} md={3} lg={3} xl={3} className="p-0">
          Dishwasher: { paintYesOrNo(hasDishwasher) }
        </Col>   
        <Col xs={12} sm={12} md={3} lg={3} xl={3} className="p-0">
          TV: { paintYesOrNo(hasTV) }
        </Col>                                    
      </Row>      


      <Row className="text-left p-0 my-3 border-bottom">
        <Col xs={12} sm={12} md={12} lg={12} xl={12} className="p-0">
          <h4 className="m-0">Other Features</h4>
        </Col>
      </Row>


      <Row className="text-left p-0 my-2">
        <Col xs={12} sm={12} md={3} lg={3} xl={3} className="p-0">
          Garage: { paintYesOrNo(hasGarage) }
        </Col>   
        <Col xs={12} sm={12} md={3} lg={3} xl={3} className="p-0">
          Basement { paintYesOrNo(hasBasement) }
        </Col>   
        <Col xs={12} sm={12} md={3} lg={3} xl={3} className="p-0">
          Pool: { paintYesOrNo(hasPool) }
        </Col>   
        <Col xs={12} sm={12} md={3} lg={3} xl={3} className="p-0">
          
        </Col>                                    
      </Row> 

      <Row className="text-left p-0 my-2">
        <Col xs={12} sm={12} md={3} lg={3} xl={3} className="p-0">
          Garden: { paintYesOrNo(hasGarden) }
        </Col>   
        <Col xs={12} sm={12} md={3} lg={3} xl={3} className="p-0">
          Playground: { paintYesOrNo(hasPlayground) }
        </Col>   
        <Col xs={12} sm={12} md={3} lg={3} xl={3} className="p-0">
         
        </Col>   
        <Col xs={12} sm={12} md={3} lg={3} xl={3} className="p-0">
          
        </Col>                                    
      </Row>

      <Row className="text-left p-0 my-3 border-bottom">
        <Col xs={12} sm={12} md={12} lg={12} xl={12} className="p-0">
          <h4 className="m-0">Situation</h4>
        </Col>
      </Row>
    
      <Row className="text-left p-0 my-2">
        <Col xs={12} sm={12} md={3} lg={3} xl={3} className="p-0">
          Country: { country }
        </Col>   
        <Col xs={12} sm={12} md={3} lg={3} xl={3} className="p-0">
          Region: { region }
        </Col>   
        <Col xs={12} sm={12} md={3} lg={3} xl={3} className="p-0">
         City: { city }
        </Col>   
        
        { publishStreetNumber 
            ? 
            <Col xs={12} sm={12} md={3} lg={3} xl={3} className="p-0">
              Postal code: {postalcode}
            </Col>  
            : null 
          }
 
        </Row>

        <Row className="text-left p-0 my-2">
        <Col xs={12} sm={12} md={3} lg={3} xl={3} className="p-0">
          Street: { streetName }
        </Col>  
        
          { publishStreetNumber 
            ? 
            <Col xs={12} sm={12} md={3} lg={3} xl={3} className="p-0">
              Street Number: {streetNumber}
            </Col>  
            : null 
          }
                                                         
      </Row>
      

    </Container>
  )
}

export default FurtherFacts;