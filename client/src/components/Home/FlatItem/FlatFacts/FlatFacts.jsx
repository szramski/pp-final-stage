import React from 'react';
import {Container, Row, Col} from 'reactstrap';
import Moment from 'react-moment';
import translationDB from './../../../shared/translations';

Moment.globalLocale = 'de';

const FlatFacts = ({
  currency,
  availableFromDate,
  numberOfRooms,
  numberOfBathRooms,
  rent,
  paymentPeriod,
  surfaceTotal,
  surfaceNet,
  address_details,
  floor,
  floorTotal,
  hasElevator,
  typeOfProperty,
  deposit,
  flatLikes,
  flatViewed
}) => {
  
const { city } = address_details;
  
//find a depsit type and return it's translation in a given language:
const getDepositType = (lang, value) => translationDB
                                        .depositTypes[lang]
                                        .find( item => item.value === value)
                                        .translation;   

  return (
    <Container fluid>
      <Row className="border-bottom text-center">
        <Col xs={12} sm={12} md={12} lg={12} xl={12}>
          <div className="h1 m-0">
          { currency === 'GBP' && <span dangerouslySetInnerHTML={{__html: '&#163;'}}></span>}
          {rent}
          { currency === 'PLN' && <span dangerouslySetInnerHTML={{__html: '&#122;&#322;'}}></span>  }
          { currency === 'EUR' && <span dangerouslySetInnerHTML={{__html: '&#8364;'}}></span>  }
          <small>{` per ${paymentPeriod}`}</small>
          </div>
        </Col>      
      </Row>          
      <Row className="py-2 text-center">
        <Col xs={6} sm={6} md={6} lg={6} xl={6}>
          {`${flatLikes.length} ${flatLikes.length === 1 ? 'Like' : 'Likes'}`}
        </Col>  
        <Col xs={6} sm={6} md={6} lg={6} xl={6}>
          {`${flatViewed.length} ${flatViewed.length === 1 ? 'View' : 'Views'}` }
        </Col>                           
      </Row> 
      <Row className="py-2 text-left">
      <Col xs={12} sm={12} md={6} lg={6} xl={6}>
          Available from: <Moment format="DD MMMM YYYY">{availableFromDate}</Moment>
        </Col>         
        <Col xs={12} sm={12} md={6} lg={6} xl={6}>
          Deposit: {getDepositType('en',deposit)}
        </Col>      
      </Row>    

      <Row className="py-2 text-left">
        <Col xs={6} sm={6} md={3} lg={3} xl={3}>
          City: {city}
        </Col>  
        <Col xs={6} sm={6} md={3} lg={3} xl={3}>
          Type: {typeOfProperty}
        </Col>  
        <Col xs={6} sm={6} md={3} lg={3} xl={3}>
          Surface: {surfaceTotal} &#x33A1;
        </Col>    
        <Col xs={6} sm={6} md={3} lg={3} xl={3}>
          Usable: {surfaceNet} &#x33A1;
        </Col>                                 
      </Row>  

      <Row className="py-2 text-left">
      <Col xs={6} sm={6} md={3} lg={3} xl={3}>
          Bedrooms: {numberOfRooms}
        </Col>       
        <Col xs={6} sm={6} md={3} lg={3} xl={3}>
          Bathrooms: {numberOfBathRooms}
        </Col>  
        <Col xs={6} sm={6} md={3} lg={3} xl={3}>
          Floor: {floor} of {floorTotal}
        </Col>    
        <Col xs={6} sm={6} md={3} lg={3} xl={3}>
          Elevator: {hasElevator ? 'Yes' : 'No'}
        </Col>                                  
      </Row> 
    </Container>
  )
}

export default FlatFacts;