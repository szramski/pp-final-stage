import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { getFlatsFromDB } from '../../../../redux_store/actions/flatActions';
import {Container, Row, Col} from 'reactstrap';
import UserAvatar from 'react-user-avatar';
import FontAwesome from 'react-fontawesome';
import moment from 'moment';
import axios from 'axios';

import FlatCommentItem from './FlatCommentItem/FlatCommentItem';

import './FlatComments.css';

/* import Form Field Component */
import SzrTextArea from '../../../shared/SzrFormFields/SzrTextArea';


class FlatComments extends Component {

  state = {
    new_comment: '',
  } 


  handleInputChange = e => this.setState({new_comment: e.target.value})

/**********************/

  handleCommentDelete = commentId => {
    axios.delete('/api/flats/comment/delete', 
      {params: {commentId, flatId: this.props.flatid}} 
    ).then( result => this.props.getFlatsFromDB(this.props.location.pathname) );    
  }

  handleCommentAdd = async ( flatId, userId, comment ) => {

    if (this.state.new_comment.trim() === '') return; 

    let commentToAdd = {
      flatId,
      userId,
      createdAt: moment().unix(),
      comment
    };

    axios.post('/api/flats/comment/add', commentToAdd )
    .then( result => {
      this.setState({new_comment: ''}); //empty the input
      this.props.getFlatsFromDB(this.props.location.pathname);
    } );
    
  };//handleCommentAdd

/**********************/

  render() {

    const {id: curr_user_id, 
          firstname: curr_user_firstname, 
          lastname: curr_user_lastname, 
          avatar: curr_user_avatar} = this.props.curr_user; 

    return (
      <Container fluid className="szr-comments-box">
        <Row className="">
          <Col xs={2} sm={2} md={2} lg={2} xl={1} className="p-0 pt-2 pl-xs-1 pl-sm-1 pl-md-1">
            <UserAvatar 
            size="48" 
            name={`${curr_user_firstname} ${curr_user_lastname}`} 
            src={curr_user_avatar} 
            style={{
              marginLeft: '3px'
            }}
            />
          </Col>
          <Col xs={8} sm={8} md={8} lg={8} xl={10} className="p-0">
          <SzrTextArea 
            name="new_comment"
            placeholder={
              `Click here to add a comment as ${curr_user_firstname} ${curr_user_lastname}...`
            }
            value={this.state.new_comment}
            onChange={this.handleInputChange}
          />   
          </Col>
          <Col xs={2} sm={2} md={2} lg={2} xl={1} className="p-0">
            <button 
              type="button" 
              className="btn btn-info btn-lg"
              onClick={
                () => this.handleCommentAdd( 
                  this.props.flatid,
                  curr_user_id, 
                  this.state.new_comment.trim()
                )}
              >
              <FontAwesome name="comment"/>
            </button>
          </Col>                    
        </Row>
        {
          this.props.flatComments.map( comment => <FlatCommentItem 
                                                    key={comment._id} 
                                                    comment={comment} 
                                                    curr_user_id={curr_user_id}
                                                    handleCommentDelete={this.handleCommentDelete}
                                                    /> )
        }
      </Container>
   )
  }
}

const mapStateToProps = state => ({
  curr_user: state.auth.user
})

export default connect(mapStateToProps, { getFlatsFromDB })(withRouter(FlatComments));