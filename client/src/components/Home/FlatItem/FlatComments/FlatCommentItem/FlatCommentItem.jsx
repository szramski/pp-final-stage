import React from 'react';
import {Row, Col} from 'reactstrap';
import UserAvatar from 'react-user-avatar';
import moment from 'moment';
import Moment from 'react-moment';
import FontAwesome from 'react-fontawesome';

export default ({comment, curr_user_id, handleCommentDelete}) => {
  return (
    <Row className="my-3 ml-1">
    <Col xs={2} sm={2} md={2} lg={1} xl={1} className="p-0">
     <UserAvatar 
        size="48" 
        name={`${comment.user.firstname} ${comment.user.lastname}`} 
        src={comment.user.avatar} 
        style={{
          marginLeft: '5px'
        }}
      />
    </Col>
    <Col xs={9} sm={9} md={9} lg={9} xl={9} className="p-0 px-1 text-left">
      <div className="m-0 mb-2">
        <h5 className="">
          {`${comment.user.firstname} ${comment.user.lastname}`}
        </h5>
        <em> 
        {' '}on <Moment format="DD.MM.YYYY HH:mm">{moment.unix(comment.createdAt)}</Moment>
        </em>
        {
          (comment.user._id === curr_user_id) 
              && <button
                    type="button"
                    onClick={() => handleCommentDelete(comment._id)}
                    className="btn btn-sm btn-outline-danger ml-2 py-0 px-2"
                  >
                      <FontAwesome name="times"/>
                  </button> 
        }
        </div>
        <div className="m-0 pr-3">{comment.comment}</div>
      </Col>    
    </Row>
  )//return
}
