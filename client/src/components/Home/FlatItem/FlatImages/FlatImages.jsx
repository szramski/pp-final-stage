import React, { Component } from 'react';
import Gallery from 'react-photo-gallery';
import Lightbox from 'react-images';
import './FlatImages.css'

class FlatImages extends Component {

  state = { currentImage: 0 };

  openLightbox = (event, obj) => {
    this.setState({
      currentImage: obj.index,
      lightboxIsOpen: true,
    });
  }
  closeLightbox = () => {
    this.setState({
      currentImage: 0,
      lightboxIsOpen: false,
    });
  }
  gotoPrevious = () => {
    this.setState({
      currentImage: this.state.currentImage - 1,
    });
  }
  gotoNext = () => {
    this.setState({
      currentImage: this.state.currentImage + 1,
    });
  }


  render() {

    const { images } = this.props;

    let photo_set = images.map( image => ({ 
      src: image.url,
      width: 2,
      height: 1
      }) 
  );

  return images.length !== 0 
  ? <div> 
      <Gallery 
        photos={photo_set} 
        onClick={this.openLightbox} 
        margin={1}
        direction={"column"}
        columns={ 
          /* 
            For better pic alignment, we define that if the PHOTO_SET contains
            just one pic, then we display just one column of photos which will
            result in the pic taking the full width of the div.
            
            In case that there are more than 1 pics available, we check if
            we've got an even amount of pics and if we have, then we show
            two columns. Otherwise we show three columns. 
            Result: For three pics, we have three little cols. For five pics,
            we have one row of three pics and one of two pics.
          */
        photo_set.length > 1 ? ( photo_set.length % 2 === 0 ? 2 : 3 ) : 1
        
        }                
      />
      <Lightbox images={photo_set}
        onClose={this.closeLightbox}
        onClickPrev={this.gotoPrevious}
        onClickNext={this.gotoNext}
        currentImage={this.state.currentImage}
        isOpen={this.state.lightboxIsOpen}
      />
    </div>
  : 
    <div className="szr-no-image">
      <span className="text-white display-4">
        No image available
      </span>
    </div> 

  };//render
}//class

export default FlatImages;
