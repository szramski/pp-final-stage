import React, { Component } from 'react';
import UserAvatar from 'react-user-avatar';
import FontAwesome from 'react-fontawesome';
import { Container, Row, Col, Button, Modal, ModalBody, ModalFooter } from 'reactstrap';

import './FileCreatorInfo.css';


class FlatCreatorInfo extends Component {

  state = {
    showMdlMsgBox: false,
  }

  toggleShowMdlMsgBox = () => this.setState({showMdlMsgBox: !this.state.showMdlMsgBox})

  render(){

    const {id, firstname, lastname, avatar} = this.props.creator;

    return (
      <Container 
      className="text-truncate"
      style={{
        height: '64px',
      }}>
        <Row>
          <Col xs={3} sm={3} md={2} lg={2} xl={2} className="mx-auto">
            <UserAvatar 
              size="64" 
              name={`${firstname} ${lastname}`} 
              src={avatar} 
            />   
          </Col>
          <Col xs={9} sm={9} md={10} lg={10} xl={10}>
            <p className="szr-file-creator m-0">
            by {firstname} {lastname}
            </p>
            <p className="m-0">
              <button 
                type="button" 
                className="btn btn-sm btn-info"
                onClick={ this.toggleShowMdlMsgBox }>
                <FontAwesome name="envelope" className="mr-2"/>Send message
              </button>
              <Modal 
              isOpen={this.state.showMdlMsgBox} 
              style={{width: '75%', margin:'150px auto'}}
            >
              <ModalBody className="text-justify">
                Message to {firstname} {lastname} {id}
              </ModalBody>
              <ModalFooter className="">
                <Button 
                  color="success mx-auto" 
                  onClick={ this.toggleShowMdlMsgBox } >
                  OK
                </Button>
              </ModalFooter>
            </Modal>   

            </p>         
          </Col>          
        </Row>
      </Container>
    )//return
  }//render
}

export default FlatCreatorInfo;
