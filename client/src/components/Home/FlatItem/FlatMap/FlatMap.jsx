import React from 'react';
import { withGoogleMap, GoogleMap, Marker } from "react-google-maps"

const FlatMap = ({latlng, isMarkerShown}) => (
  <GoogleMap
    mapTypeId="hybrid"  
    defaultZoom={17}
    center={{ lat: latlng.lat, lng: latlng.lng }}>
    { 
      isMarkerShown && <Marker position={{ lat: latlng.lat, lng: latlng.lng }} /> 
    }
  </GoogleMap>
)

export default withGoogleMap(FlatMap)