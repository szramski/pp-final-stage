import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import axios from 'axios';
import {Container, Row, Col} from 'reactstrap';
import { 
  Button,
  Card, 
  Collapse, 
  CardText, 
  CardBody, 
  CardHeader, 
  CardFooter,
  Modal,
  ModalBody,
  ModalFooter, 
 } from 'reactstrap';
import FontAwesome from 'react-fontawesome';
import classnames from 'classnames';
import { Tooltip } from 'reactstrap';

import  { getFlatsFromDB } from '../../../redux_store/actions/flatActions';

import FlatImages from './FlatImages/FlatImages';
import FlatFacts from './FlatFacts/FlatFacts';
import FurtherFacts from './FurtherFacts/FurtherFacts';
import FlatMap from './FlatMap/FlatMap';
import FlatCreatorInfo from './FlatCreatorInfo/FlatCreatorInfo';
import FlatComments from './FlatComments/FlatComments';


import LikeButton from './LikeButton/LikeButton';
import BookmarkButton from './BookmarkButton/BookmarkButton';
import TrashButton from './TrashButton/TrashButton';
import EditButton from './EditButton/EditButton';
import PublishButton from './PublishButton/PublishButton';

import Moment from 'react-moment';

import './FlatItem.css';

class FlatItem extends Component {

  state = {
    showMdlDeleteFlat: false,
    showDetails: false,
    showComments: false,
    tooltipDetailsOpen: false,
    tooltipCommentsOpen: false,
    tooltipEditOpen: false,
    tooltipDeleteOpen: false
  };
  
  toggleDetailsToolTip = () => this.setState({tooltipDetailsOpen: !this.state.tooltipDetailsOpen});

  toggleCommentsToolTip = () => this.setState({tooltipCommentsOpen: !this.state.tooltipCommentsOpen});

  toggleDeleteToolTip = () => this.setState({tooltipDeleteOpen: !this.state.tooltipDeleteOpen});

  toggleMdlDeleteFlat = () => this.setState({showMdlDeleteFlat: !this.state.showMdlDeleteFlat});

  delteFlat = () => {
    this.toggleMdlDeleteFlat();
    axios.delete('/api/flats/delete', {params: { flatId: this.props.flat._id}})
    .then(result => {
      if(result.data.status === 'ok'){
        this.props.getFlatsFromDB(this.props.location.pathname);
      }//if
    });//then
  };//delteFlat

  render() {

    const { flat } = this.props;

    return (
      <Card className="mb-4 szr-card shadow-sm">
      <CardHeader 
        className="p-2 shadow-sm szr-flat-item-header">
        <span className="m-0">
          {flat.headline}
        </span>
      </CardHeader>
      <CardBody className="p-0">
        <Container fluid className="">
          <Row className="text-justify h-100">
            <Col xs={12} sm={12} md={12} lg={12} className="p-0">
              <FlatImages images={this.props.flat.flatImages}/>
            </Col>
          </Row>
          <Row>
          <Col xs={12} sm={12} md={12} lg={12} className="mt-2 mt-xl-2">
              <FlatFacts {...this.props.flat} />
            </Col>            
          </Row>
        </Container>     
        <CardText className=" px-3 pb-0 mt-lg-3 text-justify" style={{whiteSpace: 'pre-line'}}>
          {flat.descr_short}
        </CardText>
        <Collapse isOpen={this.state.showDetails}>
          <CardText className=" px-3 pt-0 text-justify" style={{whiteSpace: 'pre-line'}}>
            {flat.descr_long}
          </CardText>
            <Container fluid>
              <Row className="text-justify h-100 p-0">
                <Col xs={12} sm={12} md={12} lg={12} className="">
                  <FurtherFacts {...this.props.flat} />
                </Col>
              </Row>
              <Row>
                <Col xs={12} sm={12} md={12} lg={12} className="p-0 m-0">
                  <FlatMap 
                    isMarkerShown={this.props.flat.publishStreetNumber}
                    latlng={this.props.flat.latlng}
                    containerElement={
                      <div 
                        style={{ height: `480px` }} 
                        className="border shadow-sm"
                      />}
                    mapElement={<div style={{ height: `100%` }} />}
                  />
                </Col>
              </Row>
              <Row className="text-left p-0 border-bottom">
                <Col xs={12} sm={12} md={12} lg={12} xl={12} className="p-0">
                  <h6 className="m-0 ml-2 my-3">
                    Published on 
                    {` `}<Moment format="DD MMMM YYYY">
                        {this.props.flat.publishedOnDate}
                      </Moment>{` `}                   
                  </h6>
                </Col>
              </Row>              
              <Row className="text-left">
                <Col xs={12} sm={12} md={12} lg={12} className="my-3">
                  <FlatCreatorInfo creator={this.props.flat.creator}/>
                </Col>
              </Row>              
            </Container>
        </Collapse>
        <Collapse isOpen={this.state.showComments} className="">
          <FlatComments 
            flatid={this.props.flat._id} 
            flatComments={this.props.flat.flatComments} 
          /> 
        </Collapse>      
      </CardBody>
      <CardFooter className="">

     {/******************
      Details Button
       *****************/}                 

        <button 
          type="button"
          id={`DetailsToolTipTarget${flat._id}`}
          className={classnames('btn btn-sm mr-2', {
            'btn-secondary': !this.state.showDetails,
            'btn-outline-secondary': this.state.showDetails,
          })} 
          onClick={ () => {

            if(!this.state.showDetails){
              /*the following will take place when the user opens
                the flat details. There is no need that the same
                happens when the user closes the flat details */

              /*Call /api/flats/viewed to add the userId (from 
                the auth header) to the flatViewed array of the flat
                identified by flatId. If the user already is in that
                array, this call will be simply ignored. */

              axios.post('/api/flats/viewed', { flatId: flat._id })
              .then(result => this.props.getFlatsFromDB(this.props.location.pathname) )

            }; //if

            this.setState( {showDetails: !this.state.showDetails} )
          }}>
            <FontAwesome name="info-circle"/>
          </button>
          <Tooltip placement="bottom" 
                      isOpen={this.state.tooltipDetailsOpen}      
                      target={`DetailsToolTipTarget${flat._id}`}
                      toggle={this.toggleDetailsToolTip}>
                        {this.state.showDetails ? 'Hide' : 'Show'} Details
          </Tooltip>              

     {/******************
      Like Button
      *****************/}   
          <LikeButton flat={this.props.flat} />

     {/******************
      Comments Button
      *****************/}   

          <button 
          type="button"
          id={`CommentsToolTipTarget${flat._id}`}  
          className={classnames('btn btn-sm mr-2', {
            'btn-info': !this.state.showComments,
            'btn-outline-secondary': this.state.showComments,
          })} 
          onClick={ () => this.setState( {showComments: !this.state.showComments} )}>
            <FontAwesome name="comment"/>
          </button>
          <Tooltip placement="bottom" 
                      isOpen={this.state.tooltipCommentsOpen}      
                      target={`CommentsToolTipTarget${flat._id}`} 
                      toggle={this.toggleCommentsToolTip}>
                        {this.state.showComments ? 'Hide' : 'Show'} Comments
          </Tooltip>              

     {/******************
      Bookmark Button
      *****************/}   

          <BookmarkButton flat={this.props.flat} />  
          
     {/******************
      Trash Button
      *****************/}   

          <TrashButton flat={this.props.flat} />

     {/******************
      Edit Button,  only on the "My Announcement Page"
      *****************/}   

    {
      this.props.location.pathname === '/my' 
      && <EditButton 
        creatorId={this.props.flat.creator._id} 
        flatId={this.props.flat._id} 
        /> 
    }

     {/******************
      Publish Button,  only on the "My Announcement Page"
      *****************/}   

    {
      this.props.location.pathname === '/my' 
      && <PublishButton 
        flatId={this.props.flat._id}        
        publishAnnouncement={this.props.flat.publishAnnouncement} 
        /> 
    }
        
     {/******************
      Remove Button, only on the "My Announcement Page"
      *****************/}   

        { 
          this.props.location.pathname === '/my' 
          &&           
            <button 
              type="button"
              id={`DeleteToolTipTarget${flat._id}`}  
              className="btn btn-sm szr-delete-btn"              
              onClick={ this.toggleMdlDeleteFlat }>
              <FontAwesome name="exclamation-triangle"/>
              <Tooltip placement="bottom" 
                        isOpen={this.state.tooltipDeleteOpen}      
                        target={`DeleteToolTipTarget${flat._id}`} 
                        toggle={this.toggleDeleteToolTip}>
                          Delete announcement permanently!
            </Tooltip>  
            <Modal 
              isOpen={this.state.showMdlDeleteFlat} 
              style={{width: '75%', margin:'150px auto'}}
            >
              <ModalBody className="text-justify">
                Please be aware that you're going to delete
                permanently this announcement as well as all
                the comments and likes related to it.
              </ModalBody>
              <ModalFooter className="">              
                <Button 
                  color="danger mx-auto" 
                  onClick={ this.delteFlat }>
                  <FontAwesome name="exclamation-triangle mr-2"/>
                    Delete
                    <FontAwesome name="exclamation-triangle ml-2"/>
                </Button>
                <Button 
                  color="success mx-auto" 
                  onClick={ this.toggleMdlDeleteFlat }>
                  Cancel
                </Button>                
              </ModalFooter>
            </Modal> 
            </button>
        }


      </CardFooter>
    </Card>
    )
  }
}

export default connect(null, { getFlatsFromDB })(withRouter(FlatItem));

