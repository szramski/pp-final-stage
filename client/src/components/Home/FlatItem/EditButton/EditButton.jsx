import React, {Component} from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Tooltip } from 'reactstrap';
import FontAwesome from 'react-fontawesome';
import './EditButton.css'

class EditButton extends Component {

  state = {
    tooltipEditOpen: false
  }

  toggleEditToolTip = () => this.setState({tooltipEditOpen: !this.state.tooltipEditOpen})

  render(){

    const {creatorId, flatId, curr_userId} = this.props;

    return (curr_userId === creatorId) && (
      <Link 
        id={`EditToolTipTarget${flatId}`}
        className="btn btn-sm szr-edit-btn mr-2"
        to={`/edit/${flatId}`}
        >
          <FontAwesome name="edit"/>
          <Tooltip placement="bottom" 
                    isOpen={this.state.tooltipEditOpen}      
                    target={`EditToolTipTarget${flatId}`} 
                    toggle={this.toggleEditToolTip}>
                      Edit Announcement
        </Tooltip>
      </Link>
   )//return
  };//render
};//class

const mapStateToProps = state => ({
  curr_userId: state.auth.user.id
})

export default connect(mapStateToProps)(EditButton)
