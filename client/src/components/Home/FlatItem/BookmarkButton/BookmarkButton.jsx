import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { getFlatsFromDB } from '../../../../redux_store/actions/flatActions';
import { Tooltip } from 'reactstrap';

import axios from 'axios';
import FontAwesome from 'react-fontawesome';

class BookmarkButton extends Component {

  state= { tooltipOpen: false };

  toggleToolTip = () => this.setState({tooltipOpen: !this.state.tooltipOpen})

  bookmarkFlat = flatId => {
    axios.post('/api/flats/bookmark', {flatId})
    .then( res => this.props.getFlatsFromDB(this.props.location.pathname) );
  };

  unbookmarkFlat = flatId => {
    axios.post('/api/flats/unbookmark', {flatId})
    .then( res => this.props.getFlatsFromDB(this.props.location.pathname) );
  };

  render() {
    
    const { flat, userId } = this.props;
    const flatId = flat._id;
    
    /*  check if the id of the user who's currently logged in
        exists in the flatBookmarks array and if yes, save true or
        false in userBookmarkedFlat variable and return the like or
        unlike button accordingly */
 
    let userBookmarkedFlat = flat.flatBookmarks.map( bookmark => bookmark._id.toString()).indexOf(userId) !== -1;

    return userBookmarkedFlat 
              ? <button 
                  type="button" 
                  id={`BookmarkToolTipTarget${flatId}`}
                  className="btn btn-sm btn-primary mr-2"
                  onClick={ () => this.unbookmarkFlat(flatId) }>
                    <FontAwesome name="bookmark" className=""/>
                    <Tooltip placement="bottom" 
                      isOpen={this.state.tooltipOpen}      
                      target={`BookmarkToolTipTarget${flatId}`}
                      toggle={this.toggleToolTip}>
                        Remove Bookmark
                    </Tooltip>                  
                </button>
              : <button 
                    type="button" 
                    id={`BookmarkToolTipTarget${flatId}`}
                    className="btn btn-sm btn-outline-secondary mr-2"
                    onClick={ () => this.bookmarkFlat(flatId) }>
                      <FontAwesome name="bookmark" className=""/>
                      <Tooltip placement="bottom" 
                        isOpen={this.state.tooltipOpen}      
                        target={`BookmarkToolTipTarget${flatId}`}
                        toggle={this.toggleToolTip}>
                          Add Bookmark
                    </Tooltip>                        
                </button>              
  }//render
}//class

const mapStateToProps = state => ({
  userId: state.auth.user.id
})

export default connect(mapStateToProps, { getFlatsFromDB })(withRouter(BookmarkButton));

