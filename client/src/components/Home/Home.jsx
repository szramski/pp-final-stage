import React, { Component } from 'react';
import { connect } from 'react-redux';
import ReactLoading from 'react-loading';
import {Container, Row, Col} from 'reactstrap';
import FlatItem from './FlatItem/FlatItem';
import {Link} from 'react-router-dom';

import  { getFlatsFromDB } from '../../redux_store/actions/flatActions';

import './Home.css';

class Home extends Component {

  componentDidMount(){
    this.props.getFlatsFromDB(this.props.location.pathname);
  };

  render() {

    const {flatsarray, loading} = this.props;

    return (
      <Container fluid className="home">
        <Row className="h-100">
          <Col xs={12} sm={12} md={8} lg={8} xl={6} className="text-center mx-auto">
            {
              this.props.location.pathname === '/liked' && 
                <p className="my-4 bg-white szr-home-title">Liked Announcements</p>
            }
            {
              this.props.location.pathname === '/my' && 
                <p className="my-4 bg-white szr-home-title">My Announcements</p>
            }            
            {
                this.props.location.pathname === '/bookmarked' && 
                <p className="my-4 bg-white szr-home-title">Bookmarked Announcements</p>
            }
            {  
                this.props.location.pathname === '/trashed' && 
                <p className="my-4 bg-white szr-home-title">Irrelevant Announcements</p>
            }
            {  
                this.props.location.pathname === '/home' && 
                <p className="my-4 bg-white szr-home-title">All Announcements</p>
            }
            {
              (flatsarray === undefined || loading)
              ? <ReactLoading 
                  type='spin' 
                  color='white' 
                  height={75} 
                  width={75} 
                  className="mx-auto mt-5 d-block" 
                /> 
              : (
                flatsarray.length > 0 
                ? flatsarray.map( flat => <FlatItem key={flat._id} flat={flat} /> )
                : //if there are no matching announcements:
                  <h5>
                    { (this.props.location.pathname === '/home' || loading)  && (
                      <div className="">  
                        <p className="mt-5 bg-white szr-home-subtitle d-block">
                          There's nothing here.
                        </p>
                        <Link 
                          to="/settings" 
                          className="btn btn-lg btn-block btn-light mt-3">
                          Check the search settings.
                        </Link> 
                        <Link 
                          to="/create" 
                          className="btn btn-lg btn-block btn-light mt-3">
                          Create a new announcement.
                        </Link> 
                        <Link 
                          to="/trashed" 
                          className="btn btn-lg btn-block btn-light mt-3">
                          View irrelevant announcements.
                        </Link>                                                   
                      </div>
                      ) 
                    }
                    {
                      this.props.location.pathname === '/my' 
                      && (
                      <div>  
                        <p className="bg-white szr-home-subtitle">
                          You haven't created any announcements by now.
                        </p>
                        <Link to="/create" className="btn btn-lg btn-light mt-3">
                          Ok, let me create one.
                        </Link> 
                      </div>
                      ) 
                    }                      
                    {
                      this.props.location.pathname === '/liked' 
                      && (
                      <div>  
                        <p className="bg-white szr-home-subtitle">
                          You haven't liked anything so far.
                        </p>
                        <Link to="/home" className="btn btn-lg btn-light mt-3">
                          Ok, show me all the announcements.
                        </Link> 
                      </div>
                      ) 
                    }  
                    {
                      this.props.location.pathname === '/bookmarked' 
                      && (
                      <div>  
                        <p className="bg-white szr-home-subtitle">
                          No bookmarks available.
                        </p>
                        <Link to="/home" className="btn btn-lg btn-light mt-3">
                          Ok, show me all the announcements.
                        </Link> 
                      </div>
                      ) 
                    }  
                    {
                      this.props.location.pathname === '/trashed' 
                      && (
                      <div>  
                        <p className="bg-white szr-home-subtitle">
                          No announcements have been marked as irrelevant.
                        </p>
                        <Link to="/home" className="btn btn-lg btn-light mt-3">
                          Ok, show me all the announcements.
                        </Link> 
                      </div>
                      ) 
                    }  
                  </h5>
              )
            }         
          </Col>
        </Row>
      </Container>

    )
  }
}


const mapStateToProps = state => ({
  flatsarray: state.flat.flatsarray
})


export default connect(mapStateToProps, { getFlatsFromDB })(Home);
